using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PruebaAgent : MonoBehaviour
{
    [SerializeField]
    private Transform m_Target;
    private NavMeshAgent m_Agent;
    // Start is called before the first frame update
    void Start()
    {
        m_Agent = GetComponent<NavMeshAgent>();
        m_Agent.updateRotation = false;
        m_Agent.updateUpAxis = false;
    }

    // Update is called once per frame
    void Update()
    {
        m_Agent.SetDestination(m_Target.position);
    }
    
}
