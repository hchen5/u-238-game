using EnemiesNS;
using SaveDataNS;
using ScriptableObjectsNS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager m_Instance;
    public static GameManager Instance => m_Instance;
    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
    public void ChangeDieScene()
    {
        SceneManager.LoadScene("DieScene");
        StartCoroutine(a());
    }
    IEnumerator a()
    {
        yield return new WaitForSeconds(1f) ;
        SaveGameManager.Instance.LoadData(SaveGameManager.Instance.saveFileNamePublic);
    }
}
