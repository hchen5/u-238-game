using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Tilemaps;
using static UnityEngine.GraphicsBuffer;

public class CameraInGame : MonoBehaviour
{
    [SerializeField] 
    private Transform _Target;
    [SerializeField]
    private float _SmothSpeed = 2f;
    public enum CameraStates { Plataform, Iddle, Continue }
    [SerializeField]
    private CameraStates _State;
    public CameraStates State => _State;
    [SerializeField]
    private Vector3 _CameraIddlePosition = Vector3.zero;
    [SerializeField]
    private float _CameraVelocityInContinue;
    [SerializeField]
    private Vector2 _CameraDirectionInContinue;
    [SerializeField]
    private float _CapsuleCircumference;
    public delegate void CamaeraDelegate();
    public event CamaeraDelegate onCameraRaisePosition;
    public static CameraInGame instance;

    [Range(-20, 20)]
    public float minModX, maxModX, minModY, maxModY;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {
        transform.position = _Target.transform.position;
    }
    private void FixedUpdate()
    {
        switch (_State)
        {
            case CameraStates.Plataform:
                MoveCameraWithPlayer();
                break;
            case CameraStates.Continue:
                CameraContinue();
                break;
            case CameraStates.Iddle:
                CameraIddle(); 
                break;
        }
    }
    private void MoveCameraWithPlayer()
    {
        Vector2 posBottom = new Vector2(transform.position.x, transform.position.y);
        Vector2 posUp = new Vector2(transform.position.x, transform.position.y);
        Vector2 posLeft = new Vector2(transform.position.x, transform.position.y);
        Vector2 posRight = new Vector2(transform.position.x, transform.position.y);

        RaycastHit2D rhitD = Physics2D.Raycast(posBottom, -Vector2.up, Mathf.Infinity, LayerMask.GetMask("Limiters"));
        RaycastHit2D rhitU = Physics2D.Raycast(posUp, Vector2.up, Mathf.Infinity, LayerMask.GetMask("Limiters"));
        RaycastHit2D rhitR = Physics2D.Raycast(posLeft, Vector2.right, Mathf.Infinity, LayerMask.GetMask("Limiters"));
        RaycastHit2D rhitL = Physics2D.Raycast(posRight, -Vector2.right, Mathf.Infinity, LayerMask.GetMask("Limiters"));

        float minPosY;
        float maxPosY;
        float minPosX;
        float maxPosX;

        if (rhitD.collider == null)
            minPosY = float.MinValue;
        else
            minPosY = rhitD.point.y;
        if (rhitU.collider == null)
            maxPosY = float.MaxValue;
        else
            maxPosY = rhitU.point.y;
        if (rhitR.collider == null)
            maxPosX = float.MaxValue;
        else
            maxPosX = rhitR.point.x;
        if (rhitL.collider == null)
            minPosX = float.MinValue;
        else
            minPosX = rhitL.point.x;

        minPosY -= minModY;
        minPosX -= minModX;
        maxPosX -= maxModX;
        maxPosY -= maxModY;

        Vector3 clampPos = new Vector3(
            Mathf.Clamp(_Target.position.x, minPosX, maxPosX),
            Mathf.Clamp(_Target.position.y, minPosY, maxPosY),
            Mathf.Clamp(_Target.position.z, -10f, -10f)
        );

        Vector3 smoothPos = Vector3.Lerp(transform.position, clampPos, _SmothSpeed * Time.deltaTime);
        transform.position = new Vector3(smoothPos.x, smoothPos.y, smoothPos.z);
    }
    private void CameraContinue()
    {
        Vector3 Position = transform.position + new Vector3(_CameraDirectionInContinue.normalized.x, _CameraDirectionInContinue.normalized.y, -10) * _CameraVelocityInContinue;
        transform.position = Position;
    }
    private void CameraIddle()
    {
        if (transform.position != _CameraIddlePosition)
            transform.position = _CameraIddlePosition;
    }
    public void GoToPosition(Vector2 Position, float Velocity)
    {
        StopAllCoroutines();
        ChangeStateContinue((Position - (Vector2)transform.position).normalized, Velocity);
        StartCoroutine(ComprovePosition(Position));
    }
    private IEnumerator ComprovePosition(Vector2 Pos)
    {
        bool PosMajorX = transform.position.x < Pos.x;
        bool PosMajorY = transform.position.y < Pos.y;
        while (true)
        {
            bool Xdone = false;
            bool Ydone = false;

            if (PosMajorX)
                if (transform.position.x >= Pos.x)
                    Xdone = true;
            if (!PosMajorX)
                if (transform.position.x <= Pos.x)
                    Xdone = true;
            if (PosMajorY)
                if (transform.position.y >= Pos.y)
                    Ydone = true;
            if (!PosMajorY)
                if (transform.position.y <= Pos.y)
                    Ydone = true;

            if (Xdone && Ydone)
                break;
            yield return new WaitForSeconds(0.05f);
        }
        ChangeStateIddle(transform.position);
        onCameraRaisePosition?.Invoke();
    }
    public void ChangeStateContinue(Vector2 Direction, float Velocity)
    {
        StopAllCoroutines();
        _CameraDirectionInContinue = Direction;
        _CameraVelocityInContinue = Velocity;
        _State = CameraStates.Continue;
    }
    public void ChangeStateIddle(Vector2 Position)
    {
        StopAllCoroutines();
        transform.position = Position;
        _CameraIddlePosition = Position;
        _State = CameraStates.Iddle;
    }
    public void ChangeStatePlataform()
    {
        StopAllCoroutines();
        _State = CameraStates.Plataform;
    }
}
