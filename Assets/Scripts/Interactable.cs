using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    [SerializeField]
    protected bool _SetIddle = false;
    public bool SetIddle => _SetIddle;
    [SerializeField]
    GameObject _ExclamationPrefab;
    GameObject go;
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            InstantExclamation();
            collision.GetComponent<PlayerBehaviour>().OnInteractable += OnInteract;
        }
    }
    protected virtual void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            DestroyExclamation();
            collision.GetComponent<PlayerBehaviour>().OnInteractable -= OnInteract;
        }
    }
    protected virtual void OnInteract() { }
    protected void InstantExclamation(bool isDoor = false)
    {
        //go = Instantiate(_ExclamationPrefab);
        //go.transform.position = new Vector2(transform.position.x, transform.position.y + (GetComponent<Collider2D>().bounds.max.y - GetComponent<Collider2D>().bounds.center.y));
        /*if (isDoor) 
        {
            go.transform.position = new Vector2(transform.position.x, transform.position.y);
        }*/
        if (GetComponent<UnlockSkill>() != null)
            GetComponent<Renderer>().material.SetFloat("_Width",1);
        else
            GetComponent<Renderer>().material.SetFloat("_Width", 0.95F);
    }
    protected void DestroyExclamation()
    {
        GetComponent<Renderer>().material.SetFloat("_Width", 0);
        //Destroy(go);
    }
}
