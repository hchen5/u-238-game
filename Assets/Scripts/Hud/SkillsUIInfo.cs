using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UINS
{
    public class SkillsUIInfo : MonoBehaviour
    {
        [SerializeField]
        private SkillsSO _SkillsSO;
        [SerializeField]
        private GameEventSkillSo _GameEventTalismanClicked;
        public SkillsSO SkillsSO { get => _SkillsSO; }
        public void SetTalismanSO(SkillsSO t)
        {
            _SkillsSO = t;
            transform.GetChild(0).GetComponent<Image>().sprite = _SkillsSO._SkillSprite;
        }
        public void ClickedTalisman()
        {
            _GameEventTalismanClicked?.Raise(_SkillsSO);
        }
    }
}
