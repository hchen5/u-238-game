using NPCNS;
using PlayerNS;
using ScriptableObjectsNS;
using SkillsNS;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using TMPro;
using UINS;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.UIElements;
using static SkillsNS.PlayerSkills;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

public class HudCanvas : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _PlayerMoneyText;
    [Header("Scroll View Content")]
    [SerializeField]
    private GameObject _InventoryScrollViewContent;
    [SerializeField]
    private GameObject  _NPCStoreScrollViewContent , _TalismansScrollViewContent, _SkillsScrollViewContent, TpViewContent;
    [Header("UI Prefabs")]
    [SerializeField]
    private GameObject _InventoryItemPrefabUI;
    [SerializeField]
    private GameObject _NPCSellItemPrefabUI, _TalismanPrefabUI, _HealthGOPrefab, _SkillPrefabUI, _PilarPrefabUI;
    [Header("UI Panel")]
    [SerializeField]
    private GameObject _InventoryPanel;
    [SerializeField]
    private GameObject _BuyPanel, _ConfigPanel, _HealthPanel, _TalismaPanel, _SkillsPanel, _TextPanel, _HudSkillPanel, _TpPanel, _FeedBackPanel;
    [SerializeField]
    private float _TextPanelOffsetY, _TextPanelOffsetX;
    PlayerBehaviour _PlayerBehaviour;
    private TalismaScriptableObject _TalismanSeleted;
    private SkillsSO _SkillSelected;
    private List<TalismaScriptableObject> _PlayerEquippedTalismans = new List<TalismaScriptableObject>();
    private List<SkillsSO> _PlayerEquippedSkills = new List<SkillsSO>();
    private List<TalismaScriptableObject> _TalismansInstantiated = new List<TalismaScriptableObject>();
    private List<SkillsSO> _SkillsInstantiated = new List<SkillsSO>();
    [SerializeField]
    private List<TalismanSlot> _TalismanSlots;
    [SerializeField]
    private List<SkillSlot> _SkillsSlotsSlots;
    [SerializeField]
    private GameEventListTalismansEquipped _GameEventTalismansEquipped;
    [SerializeField]
    private GameEventSkillSOList _GameEventSkillSoEquipped;
    [SerializeField]
    private DataBaseSO _DB;
    public TalismaScriptableObject TalismanSeleted { get => _TalismanSeleted;}
    public SkillsSO SkillSelected { get => _SkillSelected; }
    public DataBaseSO DB { get => _DB;}
    public bool BuyPanelActivated => _BuyPanel.activeInHierarchy;

    public delegate bool InputPressed(bool Stop);
    public InputPressed OnStop;

    private void Awake()
    {
        _PlayerBehaviour = FindObjectOfType<PlayerBehaviour>();
        if (_PlayerBehaviour)
        {
            _PlayerBehaviour.OnPlayerload += SetHealthUI;
            _PlayerBehaviour.OnPlayerload += SetTalismanInSlot;
            _PlayerBehaviour.OnPlayerload += SetSkillsInSlot;
        }
    }
    private void Start()
    {
        SetHealthUI();
        if (_PlayerBehaviour)
        {
            _PlayerBehaviour.GetComponent<PlayerBehaviour>().WhenEscape += CloseUIOrOpenConfigUI;
            _PlayerBehaviour.GetComponent<SM_PlayerOnIddle>().WhenEscape += CloseUIOrOpenConfigUI;
            _PlayerBehaviour.GetComponent<SM_PlayerOnGround>().onInventory += OpenInventory;
            _PlayerBehaviour.GetComponent<SM_PlayerOnIddle>().onInventory += OpenInventory;
            _PlayerBehaviour.OnPlayerReceivedDMG += ModifyHealthUI;
            _PlayerBehaviour.OnPlayerPerk += SetHealthUI;
            _PlayerBehaviour.OnPlayerPerkDisable += SetHealthUIPerk;
            //_PlayerBehaviour.OnPlayerload += SetHealthUI;
            //_PlayerBehaviour.OnPlayerload += SetTalismanInSlot;
            //_PlayerBehaviour.OnPlayerload += SetSkillsInSlot;
            SubscribeAtacks(true);
        }
    }
    public void SetFeedBackPanel(string phrase, float time = 2f)
    {
        _FeedBackPanel.GetComponent<TextMeshProUGUI>().SetText(phrase);
        _FeedBackPanel.SetActive(true);
        StartCoroutine(FeedBAckPanelCoroutine(time));
    }
    private IEnumerator FeedBAckPanelCoroutine(float time)
    {
        yield return new WaitForSeconds(time);
        _FeedBackPanel.SetActive(false);
    }
    private void SetTPPanel()
    {   List<string> list = new List<string>();
        for (int i=0; i< TpViewContent.transform.childCount; i++)
        {
            list.Add(TpViewContent.transform.GetChild(i).GetComponentInChildren<Button>().transform.GetChild(0).GetComponent<TextMeshProUGUI>().text);
        }
        foreach (KeyValuePair<string,Vector3> pilar in  _PlayerBehaviour.UnlockedPilars)
        {
            if (!list.Contains(pilar.Key))
            {
                GameObject g = Instantiate(_PilarPrefabUI);
                g.transform.parent = TpViewContent.transform;
                g.GetComponentInChildren<Button>().transform.GetChild(0).GetComponent<TextMeshProUGUI>().SetText(pilar.Key);
                g.GetComponentInChildren<Button>().onClick.AddListener(delegate { ClickedDebug(pilar.Key); });
            }
        }
        if (TpViewContent.transform.childCount > 0)
            EventSystem.current.SetSelectedGameObject(TpViewContent.transform.GetChild(0).GetComponentInChildren<Button>().gameObject);
    }
    private void ClickedDebug(string name)
    {
        _PlayerBehaviour.PlayerSeletedPilarFromHud(name);
    }
    private void SubscribeAtacks(bool Activate)
    {
        foreach (MBSkills sk in _PlayerBehaviour.GetComponents<MBSkills>())
        {
            if (Activate)
            {
                sk._OnUpdate += UpdateSkillTimeHud;
                sk._DrawSkill += UpdateSkillImageHud;
            }
            else
            {
                sk._OnUpdate -= UpdateSkillTimeHud;
                sk._DrawSkill -= UpdateSkillImageHud;
            }
        }
    }
    private void UpdateSkillTimeHud(int indx, float maxTime, float currentTime)
    {
        GameObject Slot = _HudSkillPanel.transform.GetChild(indx).gameObject;
        Slot.transform.GetChild(1).GetComponent<Image>().fillAmount = currentTime / maxTime;
        if (currentTime == 0)
            Slot.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "";
        else
            Slot.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = currentTime.ToString();
    }
    private void UpdateSkillImageHud(int indx, Sprite img)
    {
        GameObject Slot = _HudSkillPanel.transform.GetChild(indx).gameObject;
        Slot.transform.GetChild(0).GetComponent<Image>().sprite = img;
        if (img == null)
            Slot.transform.GetChild(0).gameObject.SetActive(false);
        else
            Slot.transform.GetChild(0).gameObject.SetActive(true);
    }
    public void NPCReadTextCanvas(string text, Transform trans)
    {
        if (_TextPanel == null)
            return;
        if (!_TextPanel.activeInHierarchy)
        {
            _TextPanel.transform.position = (Vector2)trans.position + new Vector2(_TextPanelOffsetX, _TextPanelOffsetY);
            _TextPanel.SetActive(true);
        }
        if (text == null)
            _TextPanel.SetActive(false);
        _TextPanel.GetComponentInChildren<TextMeshProUGUI>().text = text;
    }
    public void GetAllEquippedTalismans()
    {
        _PlayerEquippedTalismans.Clear();
        for (int i = 0; i < 3; i++)
        {
            if (_TalismanSlots[i].TalismanSO != null)
            {
                _PlayerEquippedTalismans.Add(_TalismanSlots[i].TalismanSO);
            }else
                _PlayerEquippedTalismans.Add(null);
        }
        _GameEventTalismansEquipped?.Raise(_PlayerEquippedTalismans);
    }
    private void SetTalismanInSlot()
    {
        _PlayerEquippedTalismans.Clear();
        if (_PlayerBehaviour.EquippedTalismans.Count != 0)
        {
            for (int i = 0; i < 3; i++)
            {
                if (_PlayerBehaviour.EquippedTalismans[i] != null)
                {
                    _PlayerEquippedTalismans.Add(_PlayerBehaviour.EquippedTalismans[i]);
                    _TalismanSlots[i].SetTalismanSO(_PlayerBehaviour.EquippedTalismans[i]);
                }
                else
                    _PlayerEquippedTalismans.Add(null);
            }
        }
        _GameEventTalismansEquipped?.Raise(_PlayerEquippedTalismans);
    }
    public void GetAllEquippedSkills()
    {
        _PlayerEquippedSkills.Clear();
        for (int i = 0; i < 3; i++)
        {
            if (_SkillsSlotsSlots[i].SkillSO != null)
            {
                _PlayerEquippedSkills.Add(_SkillsSlotsSlots[i].SkillSO);
            }
            else
                _PlayerEquippedSkills.Add(null);
        }
        _GameEventSkillSoEquipped?.Raise(_PlayerEquippedSkills);
    }
    private void SetSkillsInSlot()
    {
        _PlayerEquippedSkills.Clear();
        int iteration = 0;
        foreach (EnumSkills ms in _PlayerBehaviour.MySkills)
        {
            SkillsSO so = GetMySkillSO(_PlayerBehaviour.MySkills[iteration]);
            _PlayerEquippedSkills.Add(so);
            _SkillsSlotsSlots[iteration].SetSkillsSO(so);
            iteration++;
        }
        for (int i = iteration; i < 3; i++)
        {
            _PlayerEquippedSkills.Add(null);
        }
    }
    private SkillsSO GetMySkillSO(EnumSkills ms)
    {
        foreach (MBSkills Mso in _PlayerBehaviour.GetComponents<MBSkills>())
        {
            SkillsSO so = Mso.SkillSO;
            if (ms == so._SkillEnum)
            {
                return so;
            }
        }
        return null;
    }
    private void SetTalismansPanel()
    {
        for (int i = 0; i < _PlayerBehaviour.UnlockedTalismans.Count; i++)
        {
            if (!_TalismansInstantiated.Contains(_PlayerBehaviour.UnlockedTalismans[i]))
            {
                GameObject go = Instantiate(_TalismanPrefabUI);
                go.transform.GetChild(0).GetComponent<TalismanUIInfo>().SetTalismanSO(_PlayerBehaviour.UnlockedTalismans[i]);
                go.transform.GetChild(1).GetComponent<TextMeshProUGUI>().SetText(_PlayerBehaviour.UnlockedTalismans[i]._PerkName);
                go.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText(_PlayerBehaviour.UnlockedTalismans[i]._TalismanText.ToString());
                go.transform.SetParent(_TalismansScrollViewContent.transform);
                _TalismansInstantiated.Add(_PlayerBehaviour.UnlockedTalismans[i]);
            }
        }
        SelectFirstTalisma();
    }
    private void SetSkillsPanel()
    {
        for (int i = 0; i < _PlayerBehaviour.UnlockedSkills.Count; i++)
        {
            SkillsSO so = GetMySkillSO(_PlayerBehaviour.UnlockedSkills[i]);
            if (!_SkillsInstantiated.Contains(so))
            {
                GameObject go = Instantiate(_SkillPrefabUI);
                go.transform.GetChild(0).GetComponent<SkillsUIInfo>().SetTalismanSO(so);
                go.transform.GetChild(1).GetComponent<TextMeshProUGUI>().SetText(so._SkillName);
                go.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText(so._SkillDescription);
                go.transform.SetParent(_SkillsScrollViewContent.transform);
                _SkillsInstantiated.Add(so);
            }
        }
        SelectFirstSkill();
    }
    private void SelectFirstSkill()
    {
        if (_SkillsScrollViewContent.transform.childCount != 0)
        {
            EventSystem.current.SetSelectedGameObject(_SkillsScrollViewContent.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().gameObject);
        }
    }
    private void SelectFirstTalisma()
    {
        if(_TalismansScrollViewContent.transform.childCount != 0)
        {
            EventSystem.current.SetSelectedGameObject(_TalismansScrollViewContent.transform.GetChild(0).transform.GetChild(0).GetComponent<Button>().gameObject);
        }
    }
    public void SkillSelectedFunc(SkillsSO tso)
    {
        if (tso == null)
        {
            _SkillSelected = null;
            return;
        }
        if (!_PlayerEquippedSkills.Contains(tso))
            _SkillSelected = tso;
    }
    public void TalismanSelected(TalismaScriptableObject tso)
    {
        if(tso == null)
        {
            _TalismanSeleted = null;
            return;
        }
        if(!_PlayerEquippedTalismans.Contains(tso))
            _TalismanSeleted = tso;
    }
    public void ChangeMoneyGUI(int m) 
    {
        _PlayerMoneyText.SetText(m.ToString());
    }
    private void OpenInventory(bool Active) 
    {
        if (Active)
        {
            if (_InventoryPanel.activeInHierarchy)
            {
                _InventoryPanel.SetActive(false);
                _TalismaPanel.SetActive(true);
                SetTalismansPanel();
            }
            else if (_TalismaPanel.activeInHierarchy)
            {
                _TalismaPanel.SetActive(false);
                _SkillsPanel.SetActive(true);
                SetSkillsPanel();
                SetSkillsInSlot();
            }
            else if (_SkillsPanel.activeInHierarchy)
            {
                _SkillsPanel.SetActive(false);
                _TpPanel.SetActive(true);
                SetTPPanel();
            }
            else
            {
                _TpPanel.SetActive(false);
                _InventoryPanel.SetActive(true);
                SetUpInventory(_PlayerBehaviour.ItemsInventory);
            }
        } else
        {
            _InventoryPanel.SetActive(false);
            _SkillsPanel.SetActive(false);
            _TalismaPanel.SetActive(false);
            _TpPanel.SetActive(false);
        }
    }
    private bool CloseUIOrOpenConfigUI() 
    {
        if (_InventoryPanel.activeInHierarchy || _BuyPanel.activeInHierarchy || _SkillsPanel.activeInHierarchy || _TalismaPanel.activeInHierarchy || _TpPanel.activeInHierarchy)
        {
            _InventoryPanel.SetActive(false);
            _BuyPanel.SetActive(false);
            _SkillsPanel.SetActive(false);
            _TalismaPanel.SetActive(false);
            _TpPanel.SetActive(false);
            return false;
        } 
        else if (_ConfigPanel.activeInHierarchy)
        {
            _ConfigPanel.SetActive(false);
            OnStop?.Invoke(true);
        }
        else
        {
            _ConfigPanel.SetActive(true);
            OnStop?.Invoke(false);
        }
        return true;
    }
    public void SetUpInventory(Dictionary<ItemsScriptableObjects,int> PlayerInventory) 
    {
        if (_InventoryScrollViewContent.transform.childCount != 0)
        {
            for (int i = 0; i < _InventoryScrollViewContent.transform.childCount; i++)
            {
                Destroy(_InventoryScrollViewContent.transform.GetChild(i).gameObject);
            }
        }
        foreach (KeyValuePair<ItemsScriptableObjects, int> a in PlayerInventory)
        {
            GameObject UIItem = Instantiate(_InventoryItemPrefabUI);
            UIItem.transform.GetChild(0).GetComponentInChildren<TextMeshProUGUI>().SetText(a.Key._ItemName);
            UIItem.transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>().SetText(a.Value.ToString());
            UIItem.transform.GetChild(2).GetComponent<Image>().sprite = a.Key._ItemSprite;
            UIItem.transform.SetParent(_InventoryScrollViewContent.transform);
        }
    }
    public void SetUpNPCStoreItems(List<ObjectsAndQuantityCanSell> itemscansell, List<ItemsScriptableObjects> itemscantsell, List<TalismaScriptableObject> talismacantsell,NPCScript n) 
    {
        bool firstitemcanselect = true;
        if (_NPCStoreScrollViewContent.transform.childCount != 0)
        {
            for(int i=0; i< _NPCStoreScrollViewContent.transform.childCount; i++)
            {
                Destroy(_NPCStoreScrollViewContent.transform.GetChild(i).gameObject);
            }
        }
        for (int i = 0; i < itemscansell.Count; i++)
        {
            if (itemscansell[i]._TypeObjectToSell == TypeObjectToSell.Item)
            {
                GameObject UIItem = Instantiate(_NPCSellItemPrefabUI);
                //UIItem.transform.GetChild(0).GetComponent<Image>().sprite = itemscansell[i]._ItemsToSell._ItemSprite;
                UIItem.transform.GetChild(0).GetComponent<NPCSellItemScriptInfo>().SetItemSellInfo(itemscansell[i]._ItemsToSell, null, itemscansell[i]._Quantity,n);
                UIItem.transform.GetChild(1).GetComponent<TextMeshProUGUI>().SetText(itemscansell[i]._ItemsToSell._ItemName.ToString());
                UIItem.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("Price: " + itemscansell[i]._ItemsToSell._Cost.ToString());
                //UIItem.transform.GetChild(3).GetComponent<Image>().sprite = itemscansell[i]._ItemsToSell._ItemSprite;
                UIItem.transform.SetParent(_NPCStoreScrollViewContent.transform);
                if (itemscansell[i]._ItemsToSell._Cost > _PlayerBehaviour.PlayerMoney || itemscantsell.Contains(itemscansell[i]._ItemsToSell))
                {
                    UIItem.transform.GetChild(0).GetComponent<Button>().interactable = false;
                }
                else if (firstitemcanselect && UIItem.transform.GetChild(0).GetComponent<Button>().IsInteractable())
                {
                    EventSystem.current.SetSelectedGameObject(UIItem.transform.GetChild(0).gameObject);
                    firstitemcanselect = false;
                }
            }
            else if (itemscansell[i]._TypeObjectToSell == TypeObjectToSell.Talisma)
            {
                GameObject UIItem = Instantiate(_NPCSellItemPrefabUI);
                //UIItem.transform.GetChild(0).GetComponent<Image>().sprite = itemscansell[i]._TalismaScriptableObject._TalismanSprite;
                UIItem.transform.GetChild(0).GetComponent<NPCSellItemScriptInfo>().SetItemSellInfo(null,itemscansell[i]._TalismaScriptableObject, itemscansell[i]._Quantity,n);
                UIItem.transform.GetChild(1).GetComponent<TextMeshProUGUI>().SetText(itemscansell[i]._TalismaScriptableObject._PerkName.ToString());
                UIItem.transform.GetChild(2).GetComponent<TextMeshProUGUI>().SetText("Price: " + itemscansell[i]._TalismaScriptableObject._Cost.ToString());
                //UIItem.transform.GetChild(3).GetComponent<Image>().sprite = itemscansell[i]._TalismaScriptableObject._TalismanSprite;
                UIItem.transform.SetParent(_NPCStoreScrollViewContent.transform);
                if (itemscansell[i]._TalismaScriptableObject._Cost > _PlayerBehaviour.PlayerMoney || talismacantsell.Contains(itemscansell[i]._TalismaScriptableObject))
                {
                    UIItem.transform.GetChild(0).GetComponent<Button>().interactable = false;
                }
                else if (firstitemcanselect && UIItem.transform.GetChild(0).GetComponent<Button>().IsInteractable())
                {
                    EventSystem.current.SetSelectedGameObject(UIItem.transform.GetChild(0).gameObject);
                    firstitemcanselect = false;
                }
            }
            
        }
    }
    public void ChangeStoreItemsSelected()
    {
        bool firstitemcanselect = true;
        for (int i=0; i < _NPCStoreScrollViewContent.transform.childCount; i++)
        {
            Button b = _NPCStoreScrollViewContent.transform.GetChild(i).GetChild(0).GetComponent<Button>();
            if (b.IsInteractable() && firstitemcanselect)
            {
                EventSystem.current.SetSelectedGameObject(b.gameObject);
                firstitemcanselect = false;
                return;
            }
        }
    }
    private void SetHealthUI() 
    {
        int a = _PlayerBehaviour.Health - _HealthPanel.transform.childCount;
        if (_PlayerBehaviour)
        {
            for (int i = 0; i < a; i++)
            {
                GameObject go = Instantiate(_HealthGOPrefab);
                go.GetComponent<Image>().color = Color.red;
                go.transform.SetParent(_HealthPanel.transform);
            }
            ModifyHealthUI();
        }
    }
    private void SetHealthUIPerk()
    {
        if(_PlayerBehaviour)
        {
            for (int i = _PlayerBehaviour.MaxHealth; i < _HealthPanel.transform.childCount; i++)
            {
                Destroy(_HealthPanel.transform.GetChild(i).gameObject);
            }
            ModifyHealthUI();
        }
    }
    private void ModifyHealthUI() 
    {
        int ContHeartinHUD = _HealthPanel.transform.childCount;
        if (ContHeartinHUD == _PlayerBehaviour.Health)
        {
            for (int i = 0; i < _PlayerBehaviour.Health; i++)
            {
                _HealthPanel.transform.GetChild(i).GetComponent<Image>().color = Color.red;
            }
            return;
        }
        for(int i = _PlayerBehaviour.Health; i < ContHeartinHUD; i++)
        {
            _HealthPanel.transform.GetChild(i).GetComponent<Image>().color = Color.black;
        }
    }
    private void OnDisable()
    {
        if (_PlayerBehaviour)
        {
            _PlayerBehaviour.GetComponent<PlayerBehaviour>().WhenEscape -= CloseUIOrOpenConfigUI;
            _PlayerBehaviour.GetComponent<SM_PlayerOnIddle>().WhenEscape -= CloseUIOrOpenConfigUI;
            _PlayerBehaviour.GetComponent<SM_PlayerOnGround>().onInventory -= OpenInventory;
            _PlayerBehaviour.GetComponent<SM_PlayerOnIddle>().onInventory -= OpenInventory;
            _PlayerBehaviour.OnPlayerReceivedDMG -= ModifyHealthUI;
            _PlayerBehaviour.OnPlayerPerk -= SetHealthUI;
            _PlayerBehaviour.OnPlayerPerkDisable -= SetHealthUIPerk;
            _PlayerBehaviour.OnPlayerload -= SetHealthUI;
            _PlayerBehaviour.OnPlayerload -= SetTalismanInSlot;
            _PlayerBehaviour.OnPlayerload -= SetSkillsInSlot;
            SubscribeAtacks(false);
            //_PlayerBehaviour.onUpdateSkillHudSprite -= UpdateSkillImageHud;
        }
    }
}
