using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UINS
{
    public class TalismanUIInfo : MonoBehaviour
    {
        [SerializeField]
        private TalismaScriptableObject _TalismansSO;
        [SerializeField]
        private GameEventTalismanSO _GameEventTalismanClicked;
        public TalismaScriptableObject TalismansSO { get => _TalismansSO;}
        public void SetTalismanSO(TalismaScriptableObject t)
        {
            _TalismansSO = t;
            transform.GetChild(0).GetComponent<Image>().sprite = _TalismansSO._TalismanSprite;
        }
        public void ClickedTalisman()
        {
            _GameEventTalismanClicked?.Raise(_TalismansSO);
        }
        
    }
}
