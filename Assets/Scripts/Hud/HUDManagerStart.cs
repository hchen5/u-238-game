using PlayerNS;
using SaveDataNS;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HUDManagerStart : MonoBehaviour
{
    [Header("Panel")]
    [SerializeField]
    private GameObject _LoadPanel;
    [SerializeField]
    private GameObject _SettingsPanel, _InitPanel, AskToCreateNewGamePanel, AskToDeleteSlotPanel;
    [SerializeField]
    private Button _FirstButtonInMenu, _FirstButtonSelectedInLoadPanel, _FirstBttnSelectedInSettingsPanel, AcceptCreateNewGameBttn, AcceptDeleteSlotBttn;
    private List<string> files = new List<string>();
    [SerializeField]
    private List<TextMeshProUGUI> _TextButton;
    private string _SavedGameName = "SavedGame";
    private int _DeleteInt = -1;
    public string SavedGameName { get => _SavedGameName;}

    private void Start()
    {
        EventSystem.current.SetSelectedGameObject(_FirstButtonInMenu.gameObject);
    }
    public void ClickedStartButton()
    {
        GetFiles();
        _LoadPanel.SetActive(true);
        _SettingsPanel.SetActive(!_LoadPanel.activeSelf);
        _InitPanel.SetActive(!_LoadPanel.activeSelf);
        EventSystem.current.SetSelectedGameObject(_FirstButtonSelectedInLoadPanel.gameObject);
    }
    public void ClickedSettingsButton() 
    {
        _SettingsPanel.SetActive(true);
        _LoadPanel.SetActive(!_SettingsPanel.activeSelf);
        _InitPanel.SetActive(!_SettingsPanel.activeSelf);
        EventSystem.current.SetSelectedGameObject(_FirstBttnSelectedInSettingsPanel.gameObject);
    }
    public void ReturnMainMenu()
    {
        _InitPanel.SetActive(true);
        _LoadPanel.SetActive(!_InitPanel.activeSelf);
        _SettingsPanel.SetActive(!_InitPanel.activeSelf);
        EventSystem.current.SetSelectedGameObject(_FirstButtonInMenu.gameObject);
    }
    private void GetFiles()
    {
        files.Clear();
        if (!Directory.Exists(Path.Combine(Application.persistentDataPath, "DataSaved")))
        {
            return;
        }
        List<string> f = new List<string>(Directory.GetFiles(Path.Combine(Application.persistentDataPath, "DataSaved")).
            Where(s => s.EndsWith("SavedGame1.json") || s.EndsWith("SavedGame2.json") || s.EndsWith("SavedGame3.json")));
        int cont = 0;
        for (int i = 1; i <= 3; i++)
        {
            string s = "";
            string path = "";
            if(f.Count > cont)
            {
                path = Path.GetFileName(f[cont]);
                s = f[cont];
                s = s.Substring(0, s.Length - 5);
                s = s.Substring(s.Length - 1);
                if (s == i.ToString())
                {
                    files.Add(Path.GetFileName(path));
                    cont++;
                }
                else if (files.Count <= 3)
                    files.Add("null");
            }else if (files.Count <= 3)
                files.Add("null");
        }
        //Debug.Log(files.Count);
        if (files.Count != 0)
        {
            for (int i = 0; i < files.Count; i++)
            {
                if (!files[i].Equals("null"))
                    _TextButton[i].SetText(files[i]);
                else
                    _TextButton[i].SetText("Slot"+(i+1).ToString());
            }
        }
    }
    public void CreateOrLoadGame(int num)
    {
        _SavedGameName = "SavedGame";
        _SavedGameName = _SavedGameName + num.ToString() +".json";
        //Debug.Log(num);
        SaveGameManager.Instance.SetSaveFileName(_SavedGameName);
        if (!Directory.Exists(Path.Combine(Application.persistentDataPath, "DataSaved")))
        {
            Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, "DataSaved"));
        }
        if (File.Exists(Path.Combine(Application.persistentDataPath, "DataSaved", _SavedGameName)))
            SaveGameManager.Instance.LoadData(_SavedGameName);
        else
            UIAskToCreateNewGame();
    }
    private void UIAskToCreateNewGame()
    {
        AskToCreateNewGamePanel.SetActive(true);
        _LoadPanel.SetActive(false);
        EventSystem.current.SetSelectedGameObject(AcceptCreateNewGameBttn.gameObject);
    }
    public void ResponsCreateNewGame(int numr)
    {
        if (numr == 1)
        {
            NewGame();
        }
        else
        {
            ClickedStartButton();
            AskToCreateNewGamePanel.SetActive(false);
        }
    }
    public void DeleteSlot(int numr)
    {
        //Debug.Log(numr + " " + files.Count);
        if (!files[numr].Equals("null"))
        {
            _DeleteInt = numr +1;
            AskToDeleteSlotPanel.SetActive(true);
            _LoadPanel.SetActive(false);
            EventSystem.current.SetSelectedGameObject(AcceptDeleteSlotBttn.gameObject);
        }
        else
            _DeleteInt = -1;
    }
    public void ResponsDeleteSlot(int numr)
    {
        if (numr == 1)
        {
            DeleteFile(_DeleteInt);
        }
        else
        {
            ClickedStartButton();
            AskToDeleteSlotPanel.SetActive(false);
        }
    }
    private void DeleteFile(int num)
    {
        _SavedGameName = "SavedGame";
        _SavedGameName = _SavedGameName + num.ToString() + ".json";
        if (!Directory.Exists(Path.Combine(Application.persistentDataPath, "DataSaved")))
        {
            return;
        }
        if (File.Exists(Path.Combine(Application.persistentDataPath, "DataSaved", _SavedGameName)))
        {
            File.Delete(Path.Combine(Application.persistentDataPath, "DataSaved", _SavedGameName));
            
        }
        _DeleteInt = -1;
        ClickedStartButton();
        AskToDeleteSlotPanel.SetActive(false);
    }
    public void NewGame()
    {
        SaveGameManager.Instance.SaveNewGame();
    }
    public void ClickedQuitButton()
    {
        Application.Quit();
    }
}
public enum HUDManagerEnum { NewGame, DeleteSlot}
