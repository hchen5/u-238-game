using PlayerNS;
using SaveDataNS;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class DoorScript : Interactable, ISaveableDoor
{
    [SerializeField]
    private HudCanvas _HudCanvas;
    private Coroutine _Coroutine;
    [SerializeField]
    private int _ID;
    private bool Opened = false;
    private PlayerBehaviour _PlayerBehaviour;
    [SerializeField]
    private ItemsScriptableObjects _ItemWastedToUnlock;
    [SerializeField]
    private bool _isInteractable = true;
    [SerializeField]
    float TimeToOpen;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            _PlayerBehaviour = collision.transform.GetComponent<PlayerBehaviour>();
            if (_isInteractable)
            {
                InstantExclamation(true);
                _PlayerBehaviour.OnDoorInteractable += AA;
            }
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            _PlayerBehaviour = collision.transform.GetComponent<PlayerBehaviour>();
            if (_isInteractable)
            {
                DestroyExclamation();
                _PlayerBehaviour.OnDoorInteractable -= AA;
            }
        }
    }
    private void AA()
    {
        if (_PlayerBehaviour.ItemsInventory.ContainsKey(_ItemWastedToUnlock))
        {
            if (_Coroutine == null)
                _Coroutine = StartCoroutine(AvisPlayer(true));
        }
        else
            StartCoroutine(AvisPlayer(false));
    }
    IEnumerator AvisPlayer(bool a)
    {
        if (a)
        {
            yield return new WaitForSeconds(0.2f);
            _PlayerBehaviour.SubstractItemFromInventory(_ItemWastedToUnlock);
            //GetComponent<SpriteRenderer>().sprite = null;
            //GetComponent<BoxCollider2D>().isTrigger = true;
            _HudCanvas.SetFeedBackPanel("Has gastado una llave", 3f);
            /*_TextMeshProUGUI.gameObject.SetActive(true);
            _TextMeshProUGUI.SetText("Has gastado una llave");
            yield return new WaitForSeconds(3f);
            _TextMeshProUGUI.gameObject.SetActive(false);*/
            //gameObject.SetActive(false);
            StartCoroutine(Open());
            _PlayerBehaviour.OnDoorInteractable -= AA;
            _Coroutine = null;
            Opened = true;
        }
        else
        {
            _HudCanvas.SetFeedBackPanel("No tienes ninguna llave", 2.5f);
            /*_TextMeshProUGUI.gameObject.SetActive(true);
            _TextMeshProUGUI.SetText("No tienes ninguna llave");
            yield return new WaitForSeconds(2.5f);
            _TextMeshProUGUI.gameObject.SetActive(false);*/
        }

    }
    private IEnumerator Open()
    {
        float DissolvePartition = 1 / (TimeToOpen / 0.1f);
        float Dissolve = 0;
        while (Dissolve < 1)
        {
            yield return new WaitForSeconds(0.1f);
            GetComponent<Renderer>().material.SetFloat("_VerticalDissolve", Dissolve);
            Dissolve += DissolvePartition;
        }
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
    }
    private void OnBecameVisible()
    {
        GetComponent<Renderer>().material.SetFloat("_VerticalDissolve", 0);
    }

    public SaveGameData.DoorInfo Save()
    {
        return new SaveGameData.DoorInfo(_ID, Opened);
    }
    public void Load(SaveGameData.DoorInfo[] _doorinfo)
    {
        for (int i = 0; i < _doorinfo.Length; i++)
        {
            if (_doorinfo[i]._ID == _ID)
            {
                Opened = _doorinfo[i]._Opened;
            }
        }
        if(Opened)
            gameObject.SetActive(false);
    }
}
