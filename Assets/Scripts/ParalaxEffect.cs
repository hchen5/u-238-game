using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class ParalaxEffect : MonoBehaviour
{
    [SerializeField]
    private float _ParalaxVelocity;
    [SerializeField]
    private float _ParalaxRetY;
    [SerializeField]
    bool StartMenu;
    [SerializeField]
    private GameObject _Target;
    private Material _Material;
    private float _antPosX;
    private float _PosY;
    private void Awake()
    {
        _Material = GetComponent<SpriteRenderer>().material;
    }
    private void Start()
    {
        if (!StartMenu)
            transform.position = _Target.transform.position;
        else
            StartCoroutine(Paralax());
        _antPosX = transform.position.x;
        _PosY = transform.position.y;
    }
    private void FixedUpdate()
    {
        if (!StartMenu)
        {
            transform.position = new Vector2(_Target.transform.position.x, _Target.transform.position.y /** _ParalaxRetY*/);
            if (_antPosX - 0.1f > transform.position.x)
                _Material.mainTextureOffset -= new Vector2(_ParalaxVelocity * Time.deltaTime, _ParalaxRetY * Time.deltaTime);
            else if (_antPosX + 0.1f < transform.position.x)
                _Material.mainTextureOffset += new Vector2(_ParalaxVelocity * Time.deltaTime, _ParalaxRetY * Time.deltaTime);
            _antPosX = transform.position.x;
        }
    }
    private IEnumerator Paralax()
    {
        while (true)
        {
            GetComponent<SpriteRenderer>().material.mainTextureOffset += new Vector2(_ParalaxVelocity * Time.unscaledDeltaTime, 0);
            yield return new WaitForEndOfFrame();
        }
    }
}
