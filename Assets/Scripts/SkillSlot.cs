using ScriptableObjectsNS;
using SkillsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillSlot : MonoBehaviour
{
    private SkillsSO _SkillSO;
    [SerializeField]
    private Sprite _ButtonSprite;
    [SerializeField]
    private HudCanvas _HudCanvas;
    public SkillsSO SkillSO { get => _SkillSO; }
    public void SetSkillsSO(SkillsSO t)
    {
        if (t != null)
        {
            _SkillSO = t;
            transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 255);
            transform.GetChild(0).GetComponent<Image>().sprite = _SkillSO._SkillSprite;
        }
    }
    public void ClickedSLot()
    {
        if (_HudCanvas.SkillSelected == null && _SkillSO)
        {
            _SkillSO = null;
            transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 0);
            //transform.GetChild(0).GetComponent<Image>().sprite = _ButtonSprite;
        }
        else if (_HudCanvas.SkillSelected != null)
        {
            _SkillSO = _HudCanvas.SkillSelected;
            transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 255);
            transform.GetChild(0).GetComponent<Image>().sprite = _SkillSO._SkillSprite;
            _HudCanvas.SkillSelectedFunc(null);
        }
        _HudCanvas.GetAllEquippedSkills();
    }
}
