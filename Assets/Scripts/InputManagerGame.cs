using FSMState;
using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class InputManagerGame : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset _InputSys;
    private InputActionAsset _Inputs;
    /*public delegate bool InputPressed();
    public InputPressed WhenEscape;
    public InputPressed OnStop;*/
    private void Awake()
    {
        _Inputs = Instantiate(_InputSys);
        _Inputs.FindActionMap("Menu").Enable();
    }
    private void Start()
    {
        _Inputs.FindAction("Escape").performed += Escape;
    }
    private void Escape(InputAction.CallbackContext context)
    {
        /*bool StopGame = WhenEscape.Invoke();
        Debug.Log(StopGame);
        if (StopGame)
            OnStop.Invoke();*/
    }
    private void OnDisable()
    {
        _Inputs.FindAction("Escape").performed -= Escape;
    }
}
