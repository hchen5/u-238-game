using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using static SkillsNS.PlayerSkills;
using static System.Net.WebRequestMethods;


public class BurnWhenFireBall : MonoBehaviour
{
    [SerializeField]
    float TimeToBurn;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "FireBall")
        {
            StartCoroutine(Burn());
        }
    }
    private IEnumerator Burn()
    {
        float DissolvePartition = 1 / (TimeToBurn / 0.1f);
        float Dissolve = 0;
        while (Dissolve < 1)
        {
            yield return new WaitForSeconds(0.1f);
            GetComponent<Renderer>().material.SetFloat("_Dissolve", Dissolve);
            Dissolve += DissolvePartition;
        }
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
    }
    private void OnBecameVisible()
    {
        GetComponent<Renderer>().material.SetFloat("_Dissolve", 0);
    }
}
