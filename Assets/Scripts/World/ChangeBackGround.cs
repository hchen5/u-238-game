using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBackGround : MonoBehaviour
{
    [SerializeField]
    bool StartMenu;
    [SerializeField]
    GameObject[] triggerNormal;
    [SerializeField]
    GameObject[] triggerCave;
    [SerializeField]
    Sprite Normal;
    [SerializeField]
    Sprite Cave;
    private void Start()
    {
        if (!StartMenu)
        {
            foreach (GameObject trigger in triggerNormal)
            {
                trigger.GetComponent<TriggerEnter>().onPlayer += ChangeBackGroundNormal;
            }
            foreach (GameObject trigger in triggerCave)
            {
                trigger.GetComponent<TriggerEnter>().onPlayer += ChangeBackGroundCave;
            }
        }
        else
        {
            if (transform.parent.GetComponent<BackGrounsIni>().RD == 0)
                GetComponent<SpriteRenderer>().sprite = Cave;
            else
                GetComponent<SpriteRenderer>().sprite = Normal;
        }
    }
    private void ChangeBackGroundCave()
    {
        GetComponent<SpriteRenderer>().sprite = Cave;
    }
    private void ChangeBackGroundNormal()
    {
        GetComponent<SpriteRenderer>().sprite = Normal;
    }
    private void OnDestroy()
    {
        if (!StartMenu)
        {
            foreach (GameObject trigger in triggerNormal)
            {
                if (trigger != null)
                    trigger.GetComponent<TriggerEnter>().onPlayer -= ChangeBackGroundNormal;
            }
            foreach (GameObject trigger in triggerCave)
            {
                if (trigger != null)
                    trigger.GetComponent<TriggerEnter>().onPlayer -= ChangeBackGroundCave;
            }
        }
    }
}
