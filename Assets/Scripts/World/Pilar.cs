using FSMState;
using PlayerNS;
using SaveDataNS;
using SkillsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SkillsNS.PlayerSkills;

public class Pilar : Interactable
{
    private GameObject _Player;
    [SerializeField]
    private string _Name;
    [SerializeField]
    private Pool _PilarParticle;
    public string Name { get => _Name; }
    private Coroutine _MyCoroutine;
    private bool Salir;

    private void SaveGame()
    {
        ResetCoolDown();
        SaveGameManager.Instance.SaveData();
        _MyCoroutine = StartCoroutine(ReActivePlayer());
        GameObject g = _PilarParticle.GetElement();
        g.transform.position = new Vector2 (transform.position.x, transform.position.y + (GetComponent<Collider2D>().bounds.max.y - GetComponent<Collider2D>().bounds.center.y));
    }
    private void ResetCoolDown()
    {
        foreach (EnumSkills sk in _Player.GetComponent<PlayerBehaviour>().MySkills)
        {
            switch (sk)
            {
                case EnumSkills.Dash:
                    _Player.GetComponent<Dash>().ResetCoolDown();
                    break;
                case EnumSkills.FireBall:
                    _Player.GetComponent<FireBall>().ResetCoolDown();
                    break;
                case EnumSkills.Parry:
                    _Player.GetComponent<Parry>().ResetCoolDown();
                    break;
                case EnumSkills.Boomerang:
                    _Player.GetComponent<Boomerang>().ResetCoolDown();
                    break;
            }
        }
    }
    private IEnumerator ReActivePlayer()
    {
        yield return new WaitForSeconds(1f);
        _Player.GetComponent<NewFiniteStateMachine>().ChangeState<SM_PlayerOnGround>();
        if (Salir) 
            _Player = null;
        _MyCoroutine = null;
    }
    protected override void OnInteract()
    {
        SaveGame();
        base.OnInteract();
    }
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        if (collision.tag == "Player")
        {
            Salir = false;
            _Player = collision.gameObject;
        }
    }
    protected override void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerExit2D(collision);
        if (collision.tag == "Player")
        {
            if (_MyCoroutine == null)
                _Player = null;
            Salir = true;
        }
    }
}
