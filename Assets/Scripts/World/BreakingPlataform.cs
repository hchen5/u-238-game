using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using static SkillsNS.PlayerSkills;
using static System.Net.WebRequestMethods;


public class BreakingPlataform : MonoBehaviour
{
    [SerializeField]
    float _TimeToBreak;
    [SerializeField]
    float _TimeToReSpawn;
    [SerializeField]
    float _BreakingAnimTime;
    private IEnumerator BreakingCoroutine()
    {
        float AnimFor = 0;
        ActibeBreakParticle(true);
        while (AnimFor < _TimeToBreak)
        {
            yield return new WaitForSeconds(_BreakingAnimTime);
            AnimFor += _BreakingAnimTime;
        }
        StartCoroutine(ReSpawnCoroutine());
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
        ActibeBreakParticle(false);
    }
    private IEnumerator ReSpawnCoroutine()
    {
        yield return new WaitForSeconds(_TimeToReSpawn);
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<Collider2D>().enabled = true;
    }
    private void ActibeBreakParticle(bool Activate)
    {
        foreach (Transform t in transform) 
        {
            t.gameObject.SetActive(Activate);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
            if (collision.gameObject.GetComponent<Rigidbody2D>().velocity.y < 0.2f)
                StartCoroutine(BreakingCoroutine());
    }
}
