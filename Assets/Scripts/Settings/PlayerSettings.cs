using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSettings : MonoBehaviour
{
    [SerializeField]
    private Scrollbar _SB;
    [SerializeField]
    private Scrollbar _SBM;
    [SerializeField]
    private Scrollbar _SBE;
    [SerializeField]
    private Toggle _Toggle;
    public void SetVolume()
    {
        PlayerPrefs.SetFloat("Volume",_SB.value);
        SettingsGame.Instance.Volume(_SB);
        Debug.Log(_SB.value);
    }
    public void SetMusic()
    {
        PlayerPrefs.SetFloat("Music", _SBM.value);
        SettingsGame.Instance.Music(_SBM);
        Debug.Log(_SB.value);
    }
    public void SetEffects()
    {
        PlayerPrefs.SetFloat("Effects", _SBE.value);
        SettingsGame.Instance.Effects(_SBE);
        Debug.Log(_SB.value);
    }
    public void SetResolution(TMPro.TMP_Dropdown change)
    {
        PlayerPrefs.SetInt("Resolution", change.value);
        SettingsGame.Instance.ChangeResoulution(change);
        Debug.Log(change.value);
    }
    public void SetFullScreen()
    {
        PlayerPrefs.SetInt("FullScreen",_Toggle.isOn == true? 1:0);
        SettingsGame.Instance.FullScreen(_Toggle);
        Debug.Log(_Toggle.isOn == true ? 1 : 0);
    }
}
