using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OnOpenSettings : MonoBehaviour
{
    [SerializeField]
    TMPro.TMP_Dropdown _Resolutions;
    [SerializeField]
    Toggle _FullScreen;
    [SerializeField]
    Scrollbar Volumen;
    [SerializeField]
    Scrollbar Music;
    [SerializeField]
    Scrollbar Effects;
    private void Start()
    {
        Volumen.value = PlayerPrefs.GetFloat("Volume");
        _FullScreen.isOn = PlayerPrefs.GetInt("FullScreen") == 1 ? true: false;
        _Resolutions.value = PlayerPrefs.GetInt("Resolution");
        Music.value = PlayerPrefs.GetFloat("Music");
        Effects.value = PlayerPrefs.GetFloat("Effects");
        //SetResolutionH();
    }
    private void OnEnable()
    {
        //SetVolumeBar();
        //SetResolution();
        FullScreen();
    }
    /*
    private void SetVolumeBar()
    {
        Volumen.value = AudioListener.volume;
    }*/
    private void SetResolution()
    {
        switch (Screen.currentResolution.height)
        {
            case 1080:
                _Resolutions.value = 0;
                break;
            case 1200:
                _Resolutions.value = 1;
                break;
            case 600:
                _Resolutions.value = 2;
                break;
        }
    }
    private void FullScreen()
    {
        if (Screen.fullScreenMode == FullScreenMode.ExclusiveFullScreen) 
            _FullScreen.isOn = true;
        else
            _FullScreen.isOn = false;
    }
    public void ReturnToTiitle()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("StartMenu");
    }
}
