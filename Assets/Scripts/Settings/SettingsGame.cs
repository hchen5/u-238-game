using SaveDataNS;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem.Controls;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class SettingsGame : MonoBehaviour
{
    private static SettingsGame _Instance;
    public static SettingsGame Instance => _Instance;
    private int _Resolution = 0;
    private FullScreenMode _MyWidowMode = FullScreenMode.ExclusiveFullScreen;
    private float _Volumen = 0.5f;
    float MusicVal = 0.5f;
    float EffectsVal = 0.5f;
    private void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        _Resolution = PlayerPrefs.GetInt("Resolution");
        if (PlayerPrefs.GetInt("FullScreen") == 1 ? true : false)
            _MyWidowMode = FullScreenMode.ExclusiveFullScreen;
        else
            _MyWidowMode = FullScreenMode.Windowed;
        SetResolution(_Resolution, _MyWidowMode);
        _Volumen = PlayerPrefs.GetFloat("Volume");
        SetVolumen(_Volumen);
        MusicVal = PlayerPrefs.GetFloat("Music");
        GetComponent<AudioSource>().volume = MusicVal;
    }
    public void ChangeResoulution(TMPro.TMP_Dropdown change)
    {
        _Resolution = change.value;
        SetResolution(_Resolution, _MyWidowMode);
    }
    public void FullScreen(Toggle t)
    {
        if (t.isOn)
            _MyWidowMode = FullScreenMode.ExclusiveFullScreen;
        else
            _MyWidowMode = FullScreenMode.Windowed;
        SetResolution(_Resolution, _MyWidowMode);
    }
    public void Volume(Scrollbar sb)
    {
        _Volumen = sb.value;
        SetVolumen(_Volumen);
    }
    public void Music(Scrollbar sb)
    {
        GetComponent<AudioSource>().volume = MusicVal;
        MusicVal = sb.value;
    }
    public void Effects(Scrollbar sb)
    {
        EffectsVal = sb.value;
        SetAllAudioSourceEffects();
    }
    public void SetAllAudioSourceEffects()
    {
        List<AudioSource> AudioSources = FindObjectsOfType<AudioSource>().ToList();
        foreach (AudioSource audioSource in AudioSources) 
        { 
            if (audioSource.GetComponent<GameManager>() == null)
            {
                audioSource.volume = EffectsVal;
            }
        }
    }
    private void SetResolution(int index, FullScreenMode mode)
    {
        switch (index)
        {
            case 0:
                Screen.SetResolution(1920, 1080, mode);
                break;
            case 1:
                Screen.SetResolution(1920, 1200, mode);
                break;
            case 2:
                Screen.SetResolution(800, 600, mode);
                break;
        }
    }
    private void SetVolumen(float Volume)
    {
        AudioListener.volume = Volume;
    }
}
