using SaveDataNS;
using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalismanSlot : MonoBehaviour
{
    private TalismaScriptableObject _TalismanSO;
    [SerializeField]
    private Sprite _ButtonSprite;
    [SerializeField]
    private HudCanvas _HudCanvas;
    public TalismaScriptableObject TalismanSO { get => _TalismanSO;}
    public void SetTalismanSO(TalismaScriptableObject t)
    {
        if (t != null)
        {
            _TalismanSO = t;
            transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 255);
            transform.GetChild(0).GetComponent<Image>().sprite = _TalismanSO._TalismanSprite;
        }
    }
    public void ClickedSLot()
    {
        if(_HudCanvas.TalismanSeleted == null && _TalismanSO)
        {
            _TalismanSO = null;
            //transform.GetChild(0).GetComponent<Image>().sprite = _ButtonSprite;
            transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 0);
        }
        else if (_HudCanvas.TalismanSeleted != null)
        { 
            _TalismanSO = _HudCanvas.TalismanSeleted;
            transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 255);
            transform.GetChild(0).GetComponent<Image>().sprite = _TalismanSO._TalismanSprite;
            _HudCanvas.TalismanSelected(null);
        }
        _HudCanvas.GetAllEquippedTalismans();
    }
}
