using ItemsNS;
using PlayerNS;
using SaveDataNS;
using ScriptableObjectsNS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

namespace NPCNS
{
    public class NPCScript : Interactable, ISaveableNPC
    {
        [SerializeField]
        private NPCScriptableObjects _NPCSO;
        [SerializeField]
        private HudCanvas _HudCanvas;
        [SerializeField]
        private GameObject _OpenBuyPanelHud;
        [SerializeField]
        private List<ItemsScriptableObjects> _ItemsCantSellMore;
        [SerializeField]
        private List<TalismaScriptableObject> _TalismaCantSellMore;
        private List<ObjectsAndQuantityCanSell> _ObjectsAndQuantityCanSell = new List<ObjectsAndQuantityCanSell>();
        [SerializeField]
        private TextAsset _MyJSON;
        private Dialeg _MyDialegs;
        private int _Itinerate;
        public int Itinerate => _Itinerate;
        private int _Phrase;
        [SerializeField]
        private float _TimeToRead;
        private Coroutine _MyReadCoroutine;
        [SerializeField]
        private int _Index;
        private Dictionary<ItemsScriptableObjects, int> _ItemsQantity = new Dictionary<ItemsScriptableObjects, int>();
        [SerializeField]
        private List<Conditions> _Conditons;
        public int Index { get => _Index;}
        public List<TalismaScriptableObject> TalismaCantSellMore { get => _TalismaCantSellMore;}
        public List<ItemsScriptableObjects> ItemsCantSellMore { get => _ItemsCantSellMore;}
        public delegate void NPCDelegate();
        public event NPCDelegate onNPCTalk;
        private PlayerBehaviour _Player;
        [SerializeField]
        AnimationClip _AnimationClip;

        [Serializable]
        private struct Conditions
        {
            public int ConditionID;
            public List<ItemsScriptableObjects> ConditionsList;
            public GameObject Reward;
            public bool _isRewarded;
        }
        private void Start()
        {
            GetComponent<Animator>().Play(_AnimationClip.name);
            _MyDialegs = JsonUtility.FromJson<Dialeg>(_MyJSON.text);
            if (SaveGameManager.Instance.Loaded)
                return;
            _Itinerate = -1;
            SetItemsQuantity();
        }
        public void SubstractItemOrTalisma(ItemsScriptableObjects i, TalismaScriptableObject t)
        {
            if (t != null)
            { 
                _TalismaCantSellMore.Add(t);
            }
            if (i != null)
            {
                if (_ItemsQantity.ContainsKey(i) && _ItemsQantity[i] > 0)
                {
                    _ItemsQantity[i]--;
                }
                if (_ItemsQantity[i] == 0)
                {
                    if(!_ItemsCantSellMore.Contains(i))
                        _ItemsCantSellMore.Add(i);
                }
            }
        }
        private void SetItemsQuantity()
        {
            for(int i=0; i< _NPCSO._NPCListObjectsToSell.Count; i++)
            {
                if (_NPCSO._NPCListObjectsToSell[i]._TypeObjectToSell == TypeObjectToSell.Item)
                    if (!_ItemsQantity.ContainsKey(_NPCSO._NPCListObjectsToSell[i]._ItemsToSell))
                        _ItemsQantity.Add(_NPCSO._NPCListObjectsToSell[i]._ItemsToSell, _NPCSO._NPCListObjectsToSell[i]._Quantity);
            }
        }
        private void SendListItemsToSellToHud()
        {
            ReadConversation();
            onNPCTalk?.Invoke();
            //_OpenBuyPanelHud.SetActive(true);
            //_HudCanvas.SetUpNPCStoreItems(_NPCSO._NPCListItemsToSell, _ItemsCantSellMore);
        }
        private void ReadConversation()
        {
            if (_Itinerate == -1)
                _Itinerate++;
            else if (_MyDialegs.Conversation[_Itinerate].ConditionID == 0 && _MyDialegs.Conversation.Count() > _Itinerate + 1)
                _Itinerate++;
            else if (_MyDialegs.Conversation[_Itinerate].ConditionID != 0)
            {
                bool Check = true;
                List<ItemsScriptableObjects> ls = _Conditons.First(x => x.ConditionID == _MyDialegs.Conversation[_Itinerate].ConditionID).ConditionsList;
                foreach (ItemsScriptableObjects Its in ls)
                {
                    if (!_Player.ItemsInventory.ContainsKey(Its))
                    {
                        Check = false;
                        break;
                    }
                }
                if (Check)
                {
                    _Itinerate++;
                    foreach (ItemsScriptableObjects Its in ls)
                    {
                        _Player.SubstractItemFromInventory(Its);
                    }
                }
            }
            _Phrase = 0;
            PhraseText();
        }
        private bool? PhraseText()
        {
            if (_MyReadCoroutine != null)
            {
                StopAllCoroutines();
                _MyReadCoroutine = null;
                _HudCanvas.NPCReadTextCanvas(_MyDialegs.Conversation[_Itinerate].Phrase[_Phrase - 1], transform);
                if (_MyDialegs.Conversation[_Itinerate].Phrase.Count() > _Phrase)
                {
                    if (_MyDialegs.Conversation[_Itinerate].Phrase.Count() <= _Phrase && _MyDialegs.Conversation[_Itinerate].Phrase[_Phrase] != "-BUY-")
                    {
                        IfTrueSpawnReward();
                        _HudCanvas.NPCReadTextCanvas(null, transform);
                        return true;
                    }
                }
                return false;
            }
            if (_MyDialegs.Conversation[_Itinerate].Phrase.Count() > _Phrase)
            {
                if (_MyDialegs.Conversation[_Itinerate].Phrase[_Phrase] == "-BUY-")
                {
                    StopAllCoroutines();
                    _MyReadCoroutine = null;
                    _HudCanvas.NPCReadTextCanvas(null, transform);
                    _OpenBuyPanelHud.SetActive(true);
                    _HudCanvas.SetUpNPCStoreItems(SaveGameManager.Instance.Loaded == true? _ObjectsAndQuantityCanSell : _NPCSO._NPCListObjectsToSell, _ItemsCantSellMore, _TalismaCantSellMore, this);
                    return null;
                }
            }
            if (_MyDialegs.Conversation[_Itinerate].Phrase.Count() > _Phrase)
            {
                _MyReadCoroutine = StartCoroutine(ReadPhrase(_MyDialegs.Conversation[_Itinerate].Phrase[_Phrase]));
                _Phrase++;
            } else
            {
                IfTrueSpawnReward();
                _HudCanvas.NPCReadTextCanvas(null, transform);
                return true;
            }
            return false;
        }
        private void IfTrueSpawnReward()
        {
            if (_Itinerate - 1 < 0)
                return;
            if (_MyDialegs.Conversation[_Itinerate - 1].ConditionID != 0)
            {
                Conditions MyCond = _Conditons.First(x => x.ConditionID == _MyDialegs.Conversation[_Itinerate - 1].ConditionID);
                GameObject RW = MyCond.Reward;
                if (RW != null && !MyCond._isRewarded)
                {
                    _Conditons.Remove(MyCond);
                    MyCond._isRewarded = true;
                    _Conditons.Add(MyCond);
                    GameObject obj = Instantiate(RW);
                    obj.transform.position = new Vector2(transform.position.x, transform.position.y + 1f);
                    obj.GetComponent<Rigidbody2D>().AddForce((Vector2.up + Vector2.right) * 5f, ForceMode2D.Impulse);
                }
            }
        }
        private IEnumerator ReadPhrase(string Phrase)
        {
            string PhraseChars = "";
            for (int i = 0; i < Phrase.Length; i++) 
            {
                PhraseChars += Phrase[i];
                yield return new WaitForSeconds(_TimeToRead);
                _HudCanvas.NPCReadTextCanvas(PhraseChars, transform);
            }
            _MyReadCoroutine = null;
        }
        protected override void OnInteract()
        {
            SendListItemsToSellToHud();
            base.OnInteract();
        }
        protected override void OnTriggerEnter2D(Collider2D collision)
        {
            base.OnTriggerEnter2D (collision);
            if (collision.tag == "Player")
            {
                _Player = collision.GetComponent<PlayerBehaviour>();
                collision.GetComponent<SM_PlayerOnIddle>().onText += PhraseText;
            }
        }
        protected override void OnTriggerExit2D(Collider2D collision)
        {
            base.OnTriggerExit2D (collision);
            if (collision.tag == "Player")
            {
                _Player = null;
                collision.GetComponent<SM_PlayerOnIddle>().onText -= PhraseText;
                _HudCanvas.NPCReadTextCanvas(null, transform);
            }
        }
        public SaveGameData.NPCSData Save()
        {
            List<string> list = new List<string>();
            List<string> talisma = new List<string>();
            List<string> listitem = new List<string>();
            List<int> listqantity = new List<int>();
            foreach(KeyValuePair<ItemsScriptableObjects,int> iq in _ItemsQantity)
            {
                listitem.Add(iq.Key._ItemName);
                listqantity.Add(iq.Value);
            }
            foreach ( ItemsScriptableObjects iso in _ItemsCantSellMore)
            {
                list.Add(iso._ItemName);
            }
            foreach (TalismaScriptableObject t in _TalismaCantSellMore)
            {
                talisma.Add(t._PerkName);
            }
            return new SaveGameData.NPCSData(list,_Itinerate,_Index, listitem, listqantity,talisma);
        }
        public void Load(SaveGameData.NPCSData _npcdata)
        {
            _ItemsCantSellMore.Clear();
            _TalismaCantSellMore.Clear();
            _ObjectsAndQuantityCanSell.Clear();
            _ItemsQantity.Clear();
            _Itinerate = _npcdata._Itinerate; 
            for (int i = 0; i< _npcdata._ItemsCantSell.Count; i++)
            {
                _ItemsCantSellMore.Add(LevelManager.Instance.DataBaseSO.GetItem(_npcdata._ItemsCantSell[i]));
            }
            for (int i = 0; i < _npcdata._TalismaCantSell.Count; i++)
            {
                _TalismaCantSellMore.Add(LevelManager.Instance.DataBaseSO.GetTalismans(_npcdata._TalismaCantSell[i]));
            }
            if (_npcdata._Item.Count != _npcdata._Qantity.Count)
                return;
            for(int i= 0; i< _npcdata._Item.Count; i++)
            {
                /*if (_ItemsQantity.ContainsKey(LevelManager.Instance.DataBaseSO.GetItem(_npcdata._Item[i])))
                    _ItemsQantity[LevelManager.Instance.DataBaseSO.GetItem(_npcdata._Item[i])] += 1;
                else*/
                //Debug.Log(Index+": "+LevelManager.Instance.DataBaseSO.GetItem(_npcdata._Item[i]));
                    _ItemsQantity.Add(LevelManager.Instance.DataBaseSO.GetItem(_npcdata._Item[i]), _npcdata._Qantity[i]);
            }
            for (int i = 0; i < _NPCSO._NPCListObjectsToSell.Count; i++)
            {
                if (_NPCSO._NPCListObjectsToSell[i]._TypeObjectToSell == TypeObjectToSell.Talisma)
                {
                    ObjectsAndQuantityCanSell q = new ObjectsAndQuantityCanSell();
                    q._TypeObjectToSell = TypeObjectToSell.Talisma;
                    q._TalismaScriptableObject = _NPCSO._NPCListObjectsToSell[i]._TalismaScriptableObject;
                    _ObjectsAndQuantityCanSell.Add(q);
                }
                if (_NPCSO._NPCListObjectsToSell[i]._TypeObjectToSell == TypeObjectToSell.Item)
                {
                    ObjectsAndQuantityCanSell q = new ObjectsAndQuantityCanSell();
                    q._TypeObjectToSell = TypeObjectToSell.Item;
                    q._ItemsToSell = _NPCSO._NPCListObjectsToSell[i]._ItemsToSell;
                    if(_ItemsQantity.ContainsKey(_NPCSO._NPCListObjectsToSell[i]._ItemsToSell))
                        q._Quantity = _ItemsQantity[_NPCSO._NPCListObjectsToSell[i]._ItemsToSell];
                    _ObjectsAndQuantityCanSell.Add(q);
                }
            }
        }
    }
}

