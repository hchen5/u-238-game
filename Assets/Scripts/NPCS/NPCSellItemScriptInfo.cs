using NPCNS;
using PlayerNS;
using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NPCSellItemScriptInfo : MonoBehaviour
{
    private ItemsScriptableObjects _SellItemInfoSO;
    private TalismaScriptableObject _SellTalismaInfoSO;
    public ItemsScriptableObjects SellItemInfoSO { get => _SellItemInfoSO;}
    public TalismaScriptableObject SellTalismaInfoSO { get => _SellTalismaInfoSO;}
    [SerializeField]
    private GameEventItemSO _GameEventItemSO;
    [SerializeField]
    private GameEventSimple _GameEventChangeSelectedButton;
    NPCScript Npc;
    private int _Quantity = 0;
    public void SetItemSellInfo(ItemsScriptableObjects item, TalismaScriptableObject talisma, int quantity, NPCScript npc) 
    {
        _SellItemInfoSO = item;
        _SellTalismaInfoSO = talisma;
        _Quantity = quantity;
        Npc = npc;
        transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 255);
        transform.GetChild(0).GetComponent<Image>().sprite = talisma != null? talisma._TalismanSprite: item._ItemSprite;
        
    }
    public void PlayerBullItem()
    {
        _GameEventItemSO?.Raise(_SellItemInfoSO, _SellTalismaInfoSO);
        if (_Quantity > 0)

            _Quantity--;
        if (_Quantity == 0)
        {
            GetComponent<Button>().interactable = false;
            transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 80);
            _GameEventChangeSelectedButton?.Raise();
        }
        if (_SellItemInfoSO != null)
            Npc.SubstractItemOrTalisma(_SellItemInfoSO, null);
        if (_SellTalismaInfoSO != null)
            Npc.SubstractItemOrTalisma(null, _SellTalismaInfoSO);
    }
    public void NotInteractableButtonIfCantBuy(int playermoney)
    {
        if (_SellItemInfoSO != null)
        {
            if (_SellItemInfoSO._Cost > playermoney && GetComponent<Button>().IsInteractable())
            {
                GetComponent<Button>().interactable = false;
                transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 80);
                if (EventSystem.current.currentSelectedGameObject == null)
                {
                    _GameEventChangeSelectedButton?.Raise();
                }
            }
        }else if(_SellTalismaInfoSO != null)
        {
            if (_SellTalismaInfoSO._Cost > playermoney && GetComponent<Button>().IsInteractable())
            {
                GetComponent<Button>().interactable = false;
                transform.GetChild(0).GetComponent<Image>().color = new Vector4(255, 255, 255, 80);
                if (EventSystem.current.currentSelectedGameObject == null)
                {
                    _GameEventChangeSelectedButton?.Raise();
                }
            }
        }
    }
}
