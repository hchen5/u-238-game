using NPCNS;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCToVillage : MonoBehaviour
{
    [SerializeField]
    Transform TPPos;
    bool TPtoVillage;
    void Start()
    {
        if (GetComponent<NPCScript>().Itinerate != -1)
        {
            TPtoVillage = true;
            transform.position = TPPos.position;
        }
        if (!TPtoVillage)
            GetComponent<NPCScript>().onNPCTalk += ToVillage;
    }
    private void ToVillage()
    {
        TPtoVillage = true;
    }
    private void OnBecameInvisible()
    {
        if (TPtoVillage) 
        { 
            transform.position = TPPos.position;
            GetComponent<NPCScript>().onNPCTalk -= ToVillage;
        }
    }
}
