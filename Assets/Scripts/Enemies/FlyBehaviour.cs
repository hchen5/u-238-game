using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
namespace EnemiesNS
{
    public class FlyBehaviour : Enemy
    {
        private NavMeshAgent _Agent;
        private void Awake()
        {
            _Agent = GetComponent<NavMeshAgent>();
            _Pooleable =GetComponent<Pooleable>();
        }
        private void Start()
        {
            _EnemyHealth = _EnemySO._MaxHealth;
            _Agent.updateRotation = false;
            _Agent.updateUpAxis = false;
            _Agent.speed = _EnemySO._Speed;
        }

        public void SetPosition(Vector2 pos)
        {
            _Agent.Warp(pos);
        }
        internal override void SetTarget(GameObject target)
        {
            base.SetTarget(target);
        }
        protected override void Update()
        {
            base.Update();
            FollowTarget();
        }
        private void FollowTarget()
        {
            if (_Target != null)
            {
                _Agent.SetDestination(_Target.transform.position);
            }
        }
        private void OnEnable()
        {
            _EnemyHealth = _EnemySO._MaxHealth;
            _Agent.updateRotation = false;
            _Agent.updateUpAxis = false;
            _Agent.speed = _EnemySO._Speed;
        }
        private void OnDisable()
        {
            _Target = null;
            //_Agent.isStopped = true;
            //_Agent.ResetPath();
        }
    }
}