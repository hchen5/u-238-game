using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using static UnityEngine.GraphicsBuffer;

namespace EnemiesNS
{
    public class CarnivorousPlantFlyTrapBehaviour : Enemy
    {
        [SerializeField]
        private float _TimeBeforeReturn = 0.4f;
        private bool NoIsInTargetPosition = true;
        private Vector3 _TargetPositionV3;
        private LineRenderer _Li;
        public EnemiesScriptableObjects SO=> _EnemySO;
        [SerializeField]
        Sprite _Atack;
        [SerializeField]
        Sprite _Iddle;
        private void Awake()
        {
            _Pooleable = GetComponentInParent<Pooleable>();
            _RigidBody2D = GetComponent<Rigidbody2D>();
            _Li = GetComponentInParent<LineRenderer>();
        }
        private void Start()
        {
            _InitialPosition = transform.parent.position;
            _EnemyHealth = _EnemySO._MaxHealth;
        }
        public void AttackPlayer(GameObject PlayerPosition) 
        {
            _Target = PlayerPosition;
            _TargetPositionV3 = PlayerPosition.transform.position;
            _MovementDir = _TargetPositionV3 - transform.position;
            _RigidBody2D.AddForce(_MovementDir.normalized * _EnemySO._Speed, ForceMode2D.Impulse);
            GetComponent<SpriteRenderer>().sprite = _Atack;
            transform.up = _MovementDir;
        }
        private void CheckDIstanceToTarget() 
        {
            _Li.SetPosition(1, transform.localPosition) ;
            if (Vector2.Distance(_TargetPositionV3, transform.position) <= 0.4f)
            {
                if (NoIsInTargetPosition)
                {
                    _RigidBody2D.velocity = Vector2.zero;
                    NoIsInTargetPosition = false;
                    StartCoroutine(WaitTime());
                }
            }
        }
        protected override void OnDie()
        {
            base.OnDie();
            //GetComponentInParent<Pooleable>().ReturnToPool();
        }
        IEnumerator WaitTime()
        {
            GetComponent<SpriteRenderer>().sprite = _Iddle;
            IsReturn = true;
            yield return new WaitForSeconds(_TimeBeforeReturn);
            _MovementDir = _InitialPosition - transform.position;
            _RigidBody2D.AddForce(_MovementDir.normalized * _EnemySO._Speed, ForceMode2D.Impulse);
        }
        private bool IsReturn = false;
        private void Return() 
        {
            if (Vector3.Distance(_InitialPosition, transform.position) <= 0.4f)
            {
                if (IsReturn)
                {
                    gameObject.SetActive(false);
                    _RigidBody2D.velocity = Vector2.zero;
                    IsReturn = false;
                    NoIsInTargetPosition = true;
                }
            }
        }
        protected override void Update()
        {
            CheckDIstanceToTarget();
            Return();
        }
        public void setstart()
        {
            _EnemyHealth = _EnemySO._MaxHealth;
        }
        private void OnEnable()
        {
        }
    }
}
