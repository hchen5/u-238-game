﻿using EnemiesNS;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Rendering.Universal;
using static UnityEngine.GraphicsBuffer;
using Color = UnityEngine.Color;

namespace EnemiesNS
{
    public class MimeticBehaviour : Enemy
    {
        private NavMeshAgent _Agent;
        private bool _Enemy = false;
        private Light2D _Light;
        private CircleCollider2D _PatrollingCollider;
        private Vector3 point;
        private void Awake()
        {
            _Pooleable =GetComponent<Pooleable>();
            _Agent = GetComponent<NavMeshAgent>();
            _Light = GetComponent<Light2D>();
        }
        private void Start()
        {
            _EnemyHealth = _EnemySO._MaxHealth;
            _Agent.updateRotation = false;
            _Agent.updateUpAxis = false;
            _Agent.speed = _EnemySO._Speed;
            point = Point(_PatrollingCollider);
            _Agent.SetDestination(point);
        }
        private void OnEnable()
        {
            _EnemyHealth = _EnemySO._MaxHealth;

        }
        /*
        public void SetTarget(GameObject targetgo)
        {
            if(_Enemy)
                _Target = targetgo;
        }*/
        public void SetPosition(Vector2 pos)
        {
            GetComponent<NavMeshAgent>().Warp(pos);
        }
        internal override void SetTarget(GameObject target)
        {
            base.SetTarget(null);
            if (_Enemy)
                _Target = target;
        }
        protected override void Update()
        {
            base.Update();
            FollowTarget();
        }
        public void SetCampamentCollider(CircleCollider2D collider, bool enemie)
        {
            _PatrollingCollider = collider;
            _Enemy = enemie;
            point = Point(_PatrollingCollider);
            _Agent.SetDestination(point);
            if (!enemie)
                gameObject.layer = LayerMask.NameToLayer("Default");
            else
                gameObject.layer = LayerMask.NameToLayer("Enemy");
        }
        private Vector2 Point(CircleCollider2D collider)
        {
            Vector2 circleCenter = collider.transform.position;
            float circleRadius = collider.radius;
            float randomAngle = Random.Range(0f, 2f * Mathf.PI);
            float randomRadius = Random.Range(0f, circleRadius);
            float x = circleCenter.x + randomRadius * Mathf.Cos(randomAngle);
            float y = circleCenter.y + randomRadius * Mathf.Sin(randomAngle);
            Vector2 randomPointInCircle = new Vector2(x, y);
            return randomPointInCircle;
        }
        private void OnDisable()
        {
            _Target = null;
        }
        private void FollowTarget()
        {
            if (_Target != null && _Enemy)
            {
                _Light.enabled = false;
                _Agent.SetDestination(_Target.transform.position);
            }
            if (_Agent.remainingDistance <= 0.5f && _Target == null)
            {
                if (_PatrollingCollider)
                {
                    point = Point(_PatrollingCollider);
                    _Agent.SetDestination(point);
                }
            }
        }
    }
}