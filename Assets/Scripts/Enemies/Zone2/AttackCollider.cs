using EnemiesNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCollider : MonoBehaviour
{
    [SerializeField]
    private bool _ExplodeCollider = false;
    private bool _Exploded = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (_ExplodeCollider)
            {
                if (!_Exploded)
                {
                    GetComponentInParent<Enemy>().SetExplode(true);
                    _Exploded = true;
                }
                return;
            }
            GetComponentInParent<Enemy>().SetTarget(collision.gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (!_ExplodeCollider && GetComponentInParent<Enemy>())
            {
                GetComponentInParent<Enemy>().SetTarget(null);
            }
        }
    }
    private void OnEnable()
    {
        _Exploded = false;
    }
}
