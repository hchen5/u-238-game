using EnemiesNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class EnergyBomb : MonoBehaviour
{
    private Rigidbody2D _RigidBody2D;
    private float _TimeTodisappear;
    private Enemy2Behaviour _Enemy;
    private float _AddForce;
    private bool _Active = true;
    private void Awake()
    {
        _RigidBody2D = GetComponent<Rigidbody2D>();
    }
    public void SetArm(float time,float addforce, Enemy2Behaviour e)
    {
        _TimeTodisappear = time;
        _Enemy = e;
        _AddForce = addforce;
        StartCoroutine(Disappear());
        StartCoroutine(Rotate());
        _Active = true;
    }
    public void SetTimeTODisappear(float time)
    {
        _TimeTodisappear = time;
        StartCoroutine(Disappear());
    }
    IEnumerator Rotate()
    {
        //Debug.Log(transform.rotation.z);
        float rotation = transform.rotation.z;
        _RigidBody2D.AddForce(transform.right);
        yield return new WaitForSeconds(0.5f) ;
        while (_Active)
        {
            rotation -= 5;
            _RigidBody2D.AddForce(transform.right * _AddForce);
            //transform.rotation = Quaternion.Euler(0, 0, rotation);
            yield return new WaitForSeconds(0.1f);
        }
        
    }
    private IEnumerator Disappear()
    {
        yield return new WaitForSeconds(_TimeTodisappear);
        _Active = false;
        GetComponent<Pooleable>().ReturnToPool();
        //gameObject.SetActive(false);
    }
}
