using Bosses;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
namespace EnemiesNS
{
    public class Enemy2Behaviour : BossParent
    {
        [SerializeField]
        private int Damage1;
        [SerializeField]
        private int Damage2;
        [SerializeField]
        private float _TimeBeetwenChooseAttacks;
        [SerializeField]
        private Transform _BossPosition;
        //[SerializeField]
        //private float _InmuneTime;
        [Header("Arm")]
        //[SerializeField]
        //private GameObject _ArmWeapon;
        [SerializeField]
        private Transform _ArmWeaponPosition;
        [SerializeField]
        private GameObject _Player;
        [SerializeField]
        private float _ArmVelocity;
        [SerializeField]
        private float _TimeToReturn;
        private Animator _Animator;
        [SerializeField]
        private Pool _ArmPool;
        private bool _Idle = false;
        [Header("Energy Bomb")]
        [SerializeField]
        private Transform[] _InmuneAttackPoints;
        //[SerializeField]
        //private GameObject _EnergyBomb;
        [SerializeField]
        private float _EnergyBombTime;
        [SerializeField]
        private float _Velocity;
        [SerializeField]
        private Pool _EnergyBombPool;
        [Header("Attack2")]
        [SerializeField]
        private float _Attack2Duration;
        [SerializeField]
        private float _TimeBetwnBombs;
        [SerializeField]
        private float _TimeToSetVelocityBombs;
        [SerializeField]
        private float _BombVelocity;
        private bool _Attack2;
        [Header("Laser Attack")]
        //[SerializeField]
        //private GameObject _LaserAttack;
        [SerializeField]
        private float _TimeBeetwnLaser;
        [SerializeField]
        private float _LaserSpawnPositionBossYPlus;
        [SerializeField]
        private Pool _LaserAttackPool;
        private Rigidbody2D _Rigidbody2D;
        public delegate void Enemy2Die();
        public event Enemy2Die OnEvent;
        private void Awake()
        {
            _Animator =GetComponent<Animator>();
            _Rigidbody2D = GetComponent<Rigidbody2D>();
        }
        private void Start()
        {
            _DMG = _EnemyHealth;
            _Animator.Play("Reposo");
        }
        public void InitBoss()
        {
            IdleAnime();
            StartCoroutine(BossInPosition());
        }
        IEnumerator BossInPosition()
        {
            while (Vector2.Distance(_BossPosition.position, transform.position) > 0.5f)
            {
                _Rigidbody2D.velocity = (_BossPosition.transform.position - transform.position).normalized * 4f;
                yield return new WaitForSeconds(0.1f);
            }
            _Rigidbody2D.velocity = Vector3.zero;
            StartCoroutine(ChooseAttackC());
        }
        private void StopBoss()
        {
            StopAllCoroutines();
            _Idle = false;
        }
        protected override void OnDie()
        {
            base.OnDie();
            StopBoss();
            _Rigidbody2D.velocity = Vector3.zero;
            OnEvent?.Invoke();
        }
        private void IdleAnime()
        {
            _Idle = true;
            _Animator.Play("Idle");
        }
        private void InmuneAnimAttack()
        {
            _Inmune = true;
            _Animator.Play("Inmune");
            _Idle = false;
        }
        public void ArmReturned()
        {
            IdleAnime();
            StartCoroutine(ChooseAttackC());
        }
        private void Attackanim1()
        {
            _Animator.Play("Attack1");
            _Idle = false;
        }
        private void NoInmuneAnim()
        {
            _Animator.Play("NoInmune");
            _Idle = false;
        }
        private void NoInmune()
        {
            IdleAnime();
            _Inmune = false;
            _Idle = true;
        }
        protected override void OnDamageRecived()
        {
            if (!_Inmune)
            {
                base.OnDamageRecived();
            }
        }
        //L'utilitza l'animator Attack1
        private void Attack1()
        {
            //GameObject go = Instantiate(_ArmWeapon);
            GameObject go = _ArmPool.GetElement();
            go.transform.SetPositionAndRotation(_ArmWeaponPosition.position,Quaternion.identity);
            go.GetComponent<Boss2Arm>().SetArm(_Player, _TimeToReturn, _ArmVelocity,this);
            go.GetComponent<Rigidbody2D>().velocity = (_Player.transform.position - transform.position).normalized * 10f;
        }
        private void Attack2()
        {
            _Animator.Play("Attack2");
            _Idle = false;
        }
        private void Attack3Anim()
        {
            _Animator.Play("Attack3");
            _Idle = false;
        }
        private void StartAttack3()
        {
            StartCoroutine(Attack3E());
        }
        IEnumerator Attack3E()
        {
            for (int i = 0; i < 10; i++)
            {
                //GameObject g = Instantiate(_LaserAttack);
                GameObject g = _LaserAttackPool.GetElement();
                g.transform.position = new Vector3(_Player.transform.position.x, transform.position.y + _LaserSpawnPositionBossYPlus, 0);
                g.transform.rotation = Quaternion.Euler(Vector3.forward * -90);
                //Destroy(g,1.5f);
                yield return new WaitForSeconds(_TimeBeetwnLaser);
            }
            _Idle = true;
            StartCoroutine(ChooseAttackC());
        }
        /*
        private void DieAnim()
        {
            _Idle = false;
            StopAllCoroutines();
            _Animator.Play("DieAnim");
        }*/
        private void Attack2Rep()
        {
            _Animator.Play("Attack2Rep");
            _Attack2 = true;
            StartCoroutine(Attack2EnergyBombs());
            StartCoroutine(Attack2DurationCoroutine());
        }
        IEnumerator Attack2EnergyBombs()
        {
            while (_Attack2)
            {
                //GameObject go = Instantiate(_EnergyBomb);
                GameObject go = _EnergyBombPool.GetElement();
                go.transform.position = transform.position;
                yield return new WaitForSeconds(_TimeToSetVelocityBombs);
                go.GetComponent<EnergyBomb>().SetTimeTODisappear(_EnergyBombTime);
                go.GetComponent<Rigidbody2D>().velocity = (_Player.transform.position - go.transform.position).normalized * _BombVelocity;
                yield return new WaitForSeconds(_TimeBetwnBombs);
            }
        }
        IEnumerator Attack2DurationCoroutine()
        {
            yield return new WaitForSeconds(_Attack2Duration);
            _Attack2 = false;
            _Idle = true;
            StartCoroutine(ChooseAttackC());
        }
        //Animator event
        private void InmuneAttack()
        {
            StartCoroutine(InmuneAE());
        }
        IEnumerator InmuneAE()
        {
            GameObject[] g = new GameObject[_InmuneAttackPoints.Length];
            for (int i= 0; i< _InmuneAttackPoints.Length; i++)
            {
                //GameObject go = Instantiate(_EnergyBomb);
                GameObject go = _EnergyBombPool.GetElement();
                go.transform.position = _InmuneAttackPoints[i].position;
                go.transform.rotation = _InmuneAttackPoints[i].rotation;
                g[i] = go;
                yield return new WaitForSeconds(0.3f);
            }
            for (int i = 0; i < _InmuneAttackPoints.Length; i++)
            {
                g[i].GetComponent<EnergyBomb>().SetArm(_EnergyBombTime, _Velocity, this);
            }
            NoInmuneAnim();
            yield return new WaitForSeconds(1f);
            StartCoroutine(ChooseAttackC());
        }
        IEnumerator ChooseAttackC()
        {
                yield return new WaitForSeconds(_TimeBeetwenChooseAttacks);
                ChooseAttack();
        }
        private void ChooseAttack()
        {
            if (_Idle)
            {
                int i = Random.Range(0,4);
                if (i == 0)
                    Attackanim1();
                else if (i == 1)
                    Attack2();
                else if (i == 2)
                    InmuneAnimAttack();
                else if (i == 3)
                    Attack3Anim();
            }
        }
    }
}
