using EnemiesNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2Arm : MonoBehaviour
{
    private Rigidbody2D _RigidBody2D;
    private GameObject _Target;
    private float _TimeTodisappear;
    private float _Velocity;
    private Enemy2Behaviour _Enemy;
    private void Awake()
    {
        _RigidBody2D = GetComponent<Rigidbody2D>();
    }
    public void SetArm(GameObject t, float time, float velocity, Enemy2Behaviour e)
    {
        _Target = t;
        _TimeTodisappear = time;
        _Velocity = velocity;
        _Enemy = e;
        StartCoroutine(Disappear());
    }
    private void Update()
    {
        FollowTarget();
    }
    private IEnumerator Disappear()
    {
        yield return new WaitForSeconds(_TimeTodisappear);
        _Enemy.ArmReturned();
        gameObject.SetActive(false);
    }
    private void FollowTarget()
    {
        if (_Target != null)
        {
            _RigidBody2D.velocity = (_Target.transform.position - transform.position).normalized * _Velocity;
            transform.right = _Target.transform.position -transform.position;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            _Enemy.ArmReturned();
            GetComponent<Pooleable>().ReturnToPool();
            //gameObject.SetActive(false);
        }
    }
}
