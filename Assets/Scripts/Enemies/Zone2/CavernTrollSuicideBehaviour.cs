using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;
using Debug = UnityEngine.Debug;

namespace EnemiesNS
{
    public class CavernTrollSuicideBehaviour : Enemy
    {
        private bool _Debug;
        [SerializeField]
        private LayerMask _IgnoreLayer;
        private bool patroling = false;
        private Animator _Anim;
        [SerializeField]
        private GameObject _gameObject;
        [SerializeField]
        private GameObject _ExplosionCOllider;
        [SerializeField]
        private float _ExplosionRadiusMultiply =2f;
        private void Awake()
        {
            _Pooleable = GetComponent<Pooleable>();
            _Anim = GetComponent<Animator>();
            _RigidBody2D = GetComponent<Rigidbody2D>();
        }
        
        private void Start()
        {
            _EnemyCanExplode = true;
            Assert.IsNotNull(_EnemySO);
            _EnemyHealth = _EnemySO._MaxHealth;
            _Anim.Play("MushRun");
        }
        private void OnEnable()
        {
            _ExplosionCOllider.SetActive(true);
            _gameObject.SetActive(false);
            _Explode = false;
            _EnemyHealth = _EnemySO._MaxHealth;
            _Anim.Play("MushRun");
            _EnemyCanExplode = true;
        }
        protected override void Update()
        {
            base.Update();
            Movement();
        }
        protected override void Movement()
        {
            base.Movement();
            CheckChangeWay();
            if (_Target != null && !Explode)
            {
                RaycastHit2D hit = Physics2D.Raycast(transform.position, _Target.transform.position - transform.position, Vector2.Distance(_Target.transform.position, transform.position), ~_IgnoreLayer);
                RaycastHit2D hit2dDownr = Physics2D.Raycast(transform.position + new Vector3((GetComponent<CapsuleCollider2D>().size.x / 2) + 0.1f, 0, 0), Vector2.down, GetComponent<CapsuleCollider2D>().size.y / 2 + 0.1f,
                LayerMask.GetMask("Obstacles"));
                RaycastHit2D hit2dDownl = Physics2D.Raycast(transform.position - new Vector3((GetComponent<CapsuleCollider2D>().size.x / 2) + 0.1f, 0, 0), Vector2.down, GetComponent<CapsuleCollider2D>().size.y / 2 + 0.1f,
                    LayerMask.GetMask("Obstacles"));
                    if (hit)
                    {
                        if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Player"))
                        {
                            if (!hit2dDownr && !hit2dDownl)
                            {
                                _MovementDir = ((_Target.transform.position.x - transform.position.x) * Vector2.right + Vector2.up * Physics2D.gravity).normalized;
                            }
                            else
                            {
                                patroling = false;
                                _MovementDir = new Vector2(_Target.transform.position.x - transform.position.x, _RigidBody2D.velocity.y).normalized;
                            }
                        }
                        else
                            patroling = true;
                }
            }
            else
                patroling = true;
            _ExplosionCOllider.transform.position = transform.position;
        }
        private void OnDisable()
        {
            _Target = null;
        }
        protected override void ExplodeEnemy()
        {
            base.ExplodeEnemy();
            StartCoroutine(ExploteE());
        }
        private IEnumerator ExploteE()
        {
            _ExplosionCOllider.gameObject.SetActive(true);
            _ExplosionCOllider.GetComponent<CircleCollider2D>().radius *= _ExplosionRadiusMultiply;
            _gameObject.SetActive(true);
            _gameObject.transform.SetParent(null);
            StartCoroutine(returnexplosiongameobject());
            yield return new WaitForSeconds(0.1f);
            _ExplosionCOllider.gameObject.SetActive(false);
            _ExplosionCOllider.GetComponent<CircleCollider2D>().radius /= _ExplosionRadiusMultiply;
            //_Anim.Play("Explosion");
            OnDie();
        }
        private IEnumerator returnexplosiongameobject()
        {
            _gameObject.GetComponent<Animator>().Play("ExplosionGame");
            yield return new WaitForSeconds(1f);
            _gameObject.transform.SetParent(transform);
            _gameObject.SetActive(false);
        }
        private void CheckChangeWay()
        {
            if (!patroling)
                return;
            RaycastHit2D hit2dr = Physics2D.Raycast(transform.position, Vector2.right, (GetComponent<CapsuleCollider2D>().size.x / 2) + 0.1f, LayerMask.GetMask("Obstacles"));
            RaycastHit2D hit2dl = Physics2D.Raycast(transform.position, Vector2.left, (GetComponent<CapsuleCollider2D>().size.x / 2) + 0.1f, LayerMask.GetMask("Obstacles"));
            RaycastHit2D hit2dDownr = Physics2D.Raycast(transform.position + new Vector3((GetComponent<CapsuleCollider2D>().size.x / 2) + 0.1f, 0, 0), Vector2.down, GetComponent<CapsuleCollider2D>().size.y / 2 + 0.1f,
                LayerMask.GetMask("Obstacles"));
            RaycastHit2D hit2dDownl = Physics2D.Raycast(transform.position - new Vector3((GetComponent<CapsuleCollider2D>().size.x / 2) + 0.1f, 0, 0), Vector2.down, GetComponent<CapsuleCollider2D>().size.y / 2 + 0.1f,
                LayerMask.GetMask("Obstacles"));
            if (_Debug)
            {
                UnityEngine.Debug.DrawRay(transform.position, new Vector2(transform.localScale.x / 2 + 0.1f, 0), Color.red, 1f);
                UnityEngine.Debug.DrawRay(transform.position, -new Vector2(transform.localScale.x / 2 + 0.1f, 0), Color.black, 1f);
                UnityEngine.Debug.DrawRay(transform.position + new Vector3((transform.localScale.x / 2) + 0.1f, 0, 0), -new Vector3(0, transform.localScale.y / 2 + 0.1f, 0), Color.red, 1f);
            }
            if (!hit2dDownr || hit2dr)
            {
                _MovementDir = Vector2.left;
            }
            else if (!hit2dDownl || hit2dl)
            {
                _MovementDir = Vector2.right;
            }
            if (_MovementDir.x < 0)
            {
                transform.rotation = Quaternion.identity;
            }
            else if (_MovementDir.x > 0)
            {
                transform.rotation = Quaternion.Euler(Vector3.up * 180);
                
            }
        }
    }
}