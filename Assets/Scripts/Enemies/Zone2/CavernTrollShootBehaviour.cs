using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.AI;

using UnityEngine.Rendering.Universal;

namespace EnemiesNS
{
    public class CavernTrollShootBehaviour : Enemy
    {
        [SerializeField]
        private float _CooldownBttwShoot = 2f;
        private bool _CanShoot = true;
        [SerializeField]
        private LayerMask _IgnoreLayer;
        private void Awake()
        {
            _Pooleable = GetComponent<Pooleable>();
        }
        private void Start()
        {
            _EnemyHealth = _EnemySO._MaxHealth;
            GetComponent<Animator>().Play("archeridle");
        }
        internal override void SetTarget(GameObject target)
        {
            base.SetTarget(target);
        }
        private void OnEnable()
        {
            _EnemyHealth = _EnemySO._MaxHealth;
            GetComponent<Animator>().Play("archeridle");
        }
        private void OnDisable()
        {
            _Target = null;
        }
        /*
        public void SetTarget(GameObject target)
        {
            _Target = target;
        }*/
        protected override void Update()
        {
            base.Update();
            Shoot();
        }
        private void Shoot()
        {
            if (_Target != null && _CanShoot && CanShoot())
            {
                GetComponent<Animator>().Play("shootarcher");
                StartCoroutine(ShootE());
                _CanShoot = false;
                if ((_Target.transform.position - transform.position).x < 0)
                {
                    transform.rotation = Quaternion.Euler(Vector3.up * 180);
                }
                else if ((_Target.transform.position - transform.position).x > 0)
                {
                    transform.rotation = Quaternion.identity;
                }
            }
            
        }
        private IEnumerator ShootE()
        {
            yield return new WaitForSeconds(1.2f);
            if (_Target != null)
            {
                GameObject bullet = LevelManager.Instance.Bullet.GetElement();
                bullet.transform.position = transform.position;
                bullet.GetComponent<Bullet>().BulletDirection(_Target);
                bullet.transform.right = _Target.transform.position - bullet.transform.position;
            }
            StartCoroutine(Cooldown());
        }
        private bool CanShoot()
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, _Target.transform.position - transform.position,Vector2.Distance(_Target.transform.position, transform.position), ~_IgnoreLayer);
            if (hit)
            {
                if (hit.transform.gameObject.layer != LayerMask.NameToLayer("Player"))
                {
                    return false;
                }
            }
            return true;
        }
        private IEnumerator Cooldown()
        {
            yield return new WaitForSeconds(_CooldownBttwShoot);
            _CanShoot = true;
        }
    }
}