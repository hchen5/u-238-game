using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ExplosionAnim : MonoBehaviour
{
    [SerializeField]
    private Transform _parent;

    private void OnEnable()
    {
        StartCoroutine(setparent());
        transform.position = _parent.position + Vector3.down;
    }

    IEnumerator setparent()
    {
        GetComponent<Animator>().Play("ExplosionGame");
        yield return new WaitForSeconds(1f) ;
        transform.SetParent(_parent);
    }
}
