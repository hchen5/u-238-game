using EnemiesNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private float _Speed;
    public void BulletDirection(GameObject g)
    {
        if (g == null)
            return;
        GetComponent<Rigidbody2D>().velocity = (g.transform.position - transform.position).normalized * _Speed;
        if ((g.transform.position - transform.position).x < 0)
        {
            transform.rotation = Quaternion.Euler(Vector3.up * 180);
        }
        else if ((g.transform.position - transform.position).x > 0)
        {
            transform.rotation = Quaternion.identity;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            GetComponent<Pooleable>().ReturnToPool();
        }
        if (collision.gameObject.layer == LayerMask.NameToLayer("Obstacles"))
        {
            GetComponent<Pooleable>().ReturnToPool();
        }
        if(collision.gameObject.layer == LayerMask.NameToLayer("PlayerWeapon"))
            GetComponent<Pooleable>().ReturnToPool();
    }
}
