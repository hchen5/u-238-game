using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace EnemiesNS
{
    public class MoleBehaviour : Enemy
    {
        private SpriteRenderer _SpriteRenderer;
        [SerializeField]
        private LayerMask _IgnoreLayer;
        private Animator _Animator;
        //private bool _CanAppearDisappear = true;
        //private bool _AnimStopped;
        [SerializeField]
        private AnimationClip _AppearClip;
        [SerializeField]
        private AnimationClip _DisappearClip;
        [SerializeField]
        private AnimationClip _RunClip;
        private void Awake()
        {
            _Pooleable = GetComponent<Pooleable>();
            _SpriteRenderer = GetComponent<SpriteRenderer>();
            _Animator = GetComponent<Animator>();
            _RigidBody2D = GetComponent<Rigidbody2D>();
        }
        private void Start()
        {
            _EnemyCanExplode = true;
            Assert.IsNotNull(_EnemySO);
            _EnemyHealth = _EnemySO._MaxHealth;
            _RigidBody2D = GetComponent<Rigidbody2D>();
        }
        protected override void Update()
        {
            base.Update();
            Movement();
        }
        private void OnEnable()
        {
            _EnemyCanExplode = true;
            _EnemyHealth = _EnemySO._MaxHealth;
        }
        protected override void Movement()
        {
            base.Movement();
            if (_Target == null)
            {
                _MovementDir = Vector3.zero;
                EnemyInvisibleAndWait(true);
            }
            if (_Target != null)
            {
                RaycastHit2D hit = Physics2D.Raycast(transform.position, _Target.transform.position - transform.position, Vector2.Distance(_Target.transform.position, transform.position), ~_IgnoreLayer);
                RaycastHit2D hit2dDownr = Physics2D.Raycast(transform.position + new Vector3((GetComponent<CapsuleCollider2D>().size.x / 2) + 0.1f, 0, 0), Vector2.down, GetComponent<CapsuleCollider2D>().size.y / 2 + 0.1f,
                LayerMask.GetMask("Obstacles"));
                RaycastHit2D hit2dDownl = Physics2D.Raycast(transform.position - new Vector3((GetComponent<CapsuleCollider2D>().size.x / 2) + 0.1f, 0, 0), Vector2.down, GetComponent<CapsuleCollider2D>().size.y / 2 + 0.1f,
                    LayerMask.GetMask("Obstacles"));
                if (hit)
                {
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Player"))
                    {
                        EnemyInvisibleAndWait(false);
                        if (!hit2dDownr && !hit2dDownl)
                        {
                            _MovementDir = ((_Target.transform.position.x - transform.position.x) * Vector2.right + Vector2.up * Physics2D.gravity).normalized;
                        }
                        else
                        {
                            _MovementDir = new Vector2(_Target.transform.position.x - transform.position.x, _RigidBody2D.velocity.y).normalized;
                        }
                    }
                    else
                    {
                        EnemyInvisibleAndWait(true);
                        _MovementDir = Vector3.zero;
                    }
                }
                if (_MovementDir.x < 0)
                {
                    transform.rotation = Quaternion.identity;
                }
                else if (_MovementDir.x > 0)
                {
                    transform.rotation = Quaternion.Euler(Vector3.up * 180);

                }
            }
        }
        private void EnemyInvisibleAndWait(bool a)
        {
                _SpriteRenderer.enabled = !a;
                _Animator.Play("RunGhost");
        }

    }
}