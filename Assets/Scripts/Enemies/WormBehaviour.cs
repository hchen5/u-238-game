using PlayerNS;
using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using static PlayerNS.PlayerBehaviour;

namespace EnemiesNS
{
    public class WormBehaviour : Enemy
    {
        private bool _Debug;
        private void Awake()
        {
            _Pooleable = GetComponent<Pooleable>();
            _RigidBody2D = GetComponent<Rigidbody2D>();
        }
        private void Start()
        {
            Assert.IsNotNull(_EnemySO);
            _EnemyHealth = _EnemySO._MaxHealth;
            //_InitialPosition = transform.position;
        }
        protected override void Update()
        {
            base.Update();
            Movement();
        }
        protected override void Movement()
        {
            base.Movement();
            CheckChangeWay();
        }
        private void CheckChangeWay()
        {
            RaycastHit2D hit2dr = Physics2D.Raycast(transform.position, Vector2.right, (GetComponent<BoxCollider2D>().size.x / 2) + 0.1f, LayerMask.GetMask("Obstacles"));
            RaycastHit2D hit2dl = Physics2D.Raycast(transform.position, Vector2.left, (GetComponent<BoxCollider2D>().size.x / 2) + 0.1f, LayerMask.GetMask("Obstacles"));
            Vector3 BottomLeft = transform.position + new Vector3(-(GetComponent<BoxCollider2D>().size.x / 2) - GetComponent<BoxCollider2D>().offset.x, - GetComponent<BoxCollider2D>().size.y / 2 + GetComponent<BoxCollider2D>().offset.y, 0);
            Vector3 BottomRight = transform.position + new Vector3((GetComponent<BoxCollider2D>().size.x / 2) + GetComponent<BoxCollider2D>().offset.x, - GetComponent<BoxCollider2D>().size.y / 2 + GetComponent<BoxCollider2D>().offset.y, 0);
            RaycastHit2D hit2dDownr = Physics2D.Raycast(BottomRight, Vector2.down, GetComponent<BoxCollider2D>().size.y / 2 + GetComponent<BoxCollider2D>().offset.y + 0.1f,
                LayerMask.GetMask("Obstacles"));
            RaycastHit2D hit2dDownl = Physics2D.Raycast(BottomLeft, Vector2.down, GetComponent<BoxCollider2D>().size.y / 2 + GetComponent<BoxCollider2D>().offset.y + 0.1f,
                LayerMask.GetMask("Obstacles"));
            if (_Debug)
            {
                Debug.DrawRay(BottomLeft, Vector2.down * (GetComponent<BoxCollider2D>().size.y / 2 + GetComponent<BoxCollider2D>().offset.y + 0.1f), Color.red, 1f);
                Debug.DrawRay(BottomRight, Vector2.down * (GetComponent<BoxCollider2D>().size.y / 2 + GetComponent<BoxCollider2D>().offset.y + 0.1f), Color.black, 1f);
            }
            if (!hit2dDownr || hit2dr)
            {
                _MovementDir = Vector2.left;
            }
            else if (!hit2dDownl || hit2dl)
            {
                _MovementDir = Vector2.right;
            }
            if (_MovementDir.x < 0)
            {
                transform.rotation = Quaternion.Euler(Vector3.up * 180);
            }
            else if (_MovementDir.x > 0)
            {
                transform.rotation = Quaternion.identity;
            }
        }
        
        private void OnEnable()
        {
            _EnemyHealth = _EnemySO._MaxHealth;
        }
        //private Rigidbody2D _RigidBody2D;
        //private Vector2 _MovementDir = Vector2.right;
        //private int _EnemyHealth = 0;
        //[SerializeField]
        //private EnemiesScriptableObjects _EnemySO;
        /*
        private void Update()
        {
            Movement();
            if (Input.GetKeyDown(KeyCode.O))
            {
                //_EnemySO.DropMoney(GameManager.Instance.Money1Pool, GameManager.Instance.Money5Pool, GameManager.Instance.Money10Pool, transform.position);
            }
        }*/
        /*
        private void Movement() 
        {
            _RigidBody2D.velocity = _MovementDir * _EnemySO._Speed;
        }
        public void RecibeDMGFromPlayer(int Damage)
        {
            if (_EnemyHealth - Damage > 0)
                _EnemyHealth -= Damage;
            else
            {
                _EnemyHealth = 0;
                OnDie();
            }
        }
        private void OnDie()
        {
            GetComponent<Pooleable>().ReturnToPool();
            Debug.Log("EnemyHealth: " + _EnemyHealth);
            GameManager GMI = GameManager.Instance;
            _EnemySO.DropMoney(GMI.Money1Pool, GMI.Money5Pool, GMI.Money10Pool, transform.position);
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.transform.tag == "Player")
            {
                //RecibeDMGFromPlayer();
            }
        }*/
    }
}