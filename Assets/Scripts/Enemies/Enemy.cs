using SaveDataNS;
using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using Unity.VisualScripting;
using UnityEngine;
namespace EnemiesNS
{
    public class Enemy : MonoBehaviour, ISaveableEnemies
    {
        [SerializeField]
        protected EnemiesScriptableObjects _EnemySO;
        protected int _EnemyHealth = 0;
        protected Rigidbody2D _RigidBody2D;
        protected Vector2 _MovementDir = Vector2.right;
        protected Vector3 _InitialPosition;
        protected GameObject _Target;
        private int AddMoneyPerk = 0;
        private Coroutine _CoroutineBleeding;
        private bool _PlayerBleedingPerk = false;
        private bool _HaveBleedingActive = false;
        private ReturnValue _ReturnValue;
        private bool _Died = false;
        private bool _CanRespawn;
        private int _Index = -1;
        protected bool _Explode = false;
        protected bool _EnemyCanExplode = false;
        private Vector3 _Transform;
        public GameObject Target { get => _Target; }
        public bool CanRespawn { get => _CanRespawn;}
        public EnemiesScriptableObjects EnemySo { get => _EnemySO; }
        public bool Explode { get => _Explode;}
        protected Pooleable _Pooleable;
        private void Awake()
        {
            _CanRespawn = _EnemySO._CanRespawn;
            _Pooleable= GetComponent<Pooleable>();
        }
        public void SetIndexWhenSpawn(int index, Vector3 position)
        {
            _Index = index;
            _Transform = position;
        }
        internal virtual void SetTarget(GameObject target)
        {
            _Target = target;
        }
        internal void SetExplode(bool t)
        {
            if (!_EnemyCanExplode)
                return;
            _Explode = t;
            ExplodeEnemy();
        }
        protected virtual void ExplodeEnemy()
        {
            _MovementDir = Vector2.zero;
        }
        public void ReturnToPoolEnemies()
        {
            ReturnToPool();
        }
        protected virtual void OnDie()
        {          
            LevelManager LMI = LevelManager.Instance;
            _EnemySO.DropMoney(LMI.Money1Pool, LMI.Money5Pool, LMI.Money10Pool, transform.position,AddMoneyPerk);
            _Died = true;
            _Target = null;
            ReturnToPool();
        }
        protected virtual void RecibeDMGFromPlayer(DamageToEnemy DTE)
        {
            ReduceHealth(DTE.Damage);
            if (DTE.CanUseBleedPerk)
                SetBleedingPerk(DTE.BleedPerkActive,DTE.ReturnValue);
            if (_PlayerBleedingPerk && !_HaveBleedingActive)
                Bleeding(_ReturnValue);
            if (DTE.MoneyPerkActive)
                PerkMoney(DTE.MoneyPerKPercent);
        }
        private void ReduceHealth(int Damage)
        {
            //StartCoroutine(ChangeColor());
            DamgeParticle();
            if (_EnemyHealth - Damage > 0)
            {
                _EnemyHealth -= Damage;
            }
            else if (!_Died)
            {
                _EnemyHealth = 0;
                OnDie();
            }
        }
        private void DamgeParticle()
        {
            GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("Effects");
            GameObject g = LevelManager.Instance.DamageParticles.GetElement();
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().Play();
            //Debug.Log(GetComponent<Collider2D>().bounds.max.x);
            g.transform.position = new Vector2(transform.position.x, transform.position.y + (GetComponent<Collider2D>().bounds.max.y - GetComponent<Collider2D>().bounds.center.y));
        }
        /*private IEnumerator ChangeColor()
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            yield return new WaitForSeconds(0.1f);
            GetComponent<SpriteRenderer>().color = Color.white;
        }*/
        protected virtual void ReturnToPool()
        {
             _Pooleable.ReturnToPool();
            _EnemyHealth = _EnemySO._MaxHealth;
            _Died = false;
            AddMoneyPerk = 0;
            _Target = null;
            _Index = -1;
            if (_CoroutineBleeding != null)
                _CoroutineBleeding = null;
        }
        protected virtual void Update()
        {
        }
        private void SetBleedingPerk(bool bleedperkactive, ReturnValue r)
        {
            _PlayerBleedingPerk = bleedperkactive;
            _ReturnValue = r;
        }
        protected void Bleeding(ReturnValue perk)
        {
            _CoroutineBleeding = StartCoroutine(StartBleed(perk._PerkValue,perk._TimeToBleed));
            StartCoroutine(StopBleed(perk._TotalTime));
            _HaveBleedingActive = true;
        }
        IEnumerator StartBleed(float DMG, float Time)
        {
            while (true)
            {
                yield return new WaitForSeconds(Time);
                ReduceHealth((int)DMG);
            }
        }
        IEnumerator StopBleed(float TimeToStop)
        {
            yield return new WaitForSeconds(TimeToStop);
            if (_CoroutineBleeding != null)
            {
                StopCoroutine(_CoroutineBleeding);
                _CoroutineBleeding = null;
                _HaveBleedingActive = false;
            }
        }
        public void PerkMoney(int addmoneyperc)
        {
            int money = (_EnemySO._MoneyQuantity * addmoneyperc) / 100;
            AddMoneyPerk = money;
        }
        protected virtual void Movement()
        {
            if (_RigidBody2D != null)
            {
                _RigidBody2D.velocity = _MovementDir * _EnemySO._Speed;
            }
        }
        public void ResetEnemyPosition()
        {
            _Target = null;
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("PlayerWeapon")))
            {
                RecibeDMGFromPlayer(collision.gameObject.GetComponent<DamageToEnemy>());
            }
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("PlayerWeapon")))
            {
                RecibeDMGFromPlayer(collision.gameObject.GetComponent<DamageToEnemy>());
            }
        }
        public SaveGameData.EnemiesData Save()
        {
                return new SaveGameData.EnemiesData(_Index,EnemySo._CanRespawn,_Transform, _EnemySO._EnemyType);
        }
        public void Load(SaveGameData.EnemiesData _enemiesdata)
        {
            throw new System.NotImplementedException();
        }
    }
}