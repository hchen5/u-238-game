using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EnemiesNS
{
    public class EnemyAttackDMG : MonoBehaviour
    {
        [SerializeField]
        private bool _isManualDamage;
        [SerializeField]
        private EnemiesScriptableObjects _EnemySO;
        [SerializeField]
        private int _BossDamage;
        private int _DMG;
        public int DMG { get => _DMG; }
        private void Start()
        {
            if (!_isManualDamage)
                _DMG = _EnemySO._DMG;
            else
                _DMG = _BossDamage;
        }
    }
}