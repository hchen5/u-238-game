using EnemiesNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyColliderFollow : MonoBehaviour
{
    private FlyBehaviour _Fly;
    private MimeticBehaviour _Mimetic;
    [SerializeField]
    private float _CircleCollider2DRadius = 3f;
    private void Start()
    {
        if (GetComponentInParent<FlyBehaviour>())
            _Fly = GetComponentInParent<FlyBehaviour>();
        else if (GetComponentInParent<MimeticBehaviour>())
            _Mimetic = GetComponentInParent<MimeticBehaviour>();
        GetComponent<CircleCollider2D>().radius = _CircleCollider2DRadius;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (_Fly != null)
            {
                if (_Fly.Target == null)
                    _Fly.SetTarget(collision.gameObject);
            }
            else if (_Mimetic != null) {
                if(_Mimetic.Target == null)
                    _Mimetic.SetTarget(collision.gameObject);
            } 
        }
    }
}
