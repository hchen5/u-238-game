using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemyObjects : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Enemy")))
        {
            collision.gameObject.GetComponent<Pooleable>().ReturnToPool();
        }
    }
}
