using SaveDataNS;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bosses
{
    public abstract class BossParent : MonoBehaviour, ISaveableBoss
    {
        [SerializeField]
        protected int _EnemyHealth;
        protected int _DMG;
        protected bool _Respawn = true;
        public bool Respawn => _Respawn;
        [SerializeField]
        protected int _MoneyQuantity;
        [SerializeField]
        protected int _ID;
        protected bool _Inmune = false;
        public int ID { get => _ID; }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("PlayerWeapon")))
            {
                RecibeDMGFromPlayer(collision.gameObject.GetComponent<DamageToEnemy>().Damage);
            }
        }
        private void Start()
        {
            _DMG = _EnemyHealth;
        }
        protected void RecibeDMGFromPlayer(int Damage)
        {
            if (!_Inmune)
            {
                DamgeParticle();
                if (_DMG - Damage > 0)
                {
                    _DMG -= Damage;
                    OnDamageRecived();
                }
                else
                {
                    _DMG = 0;
                    OnDie();
                }
            }
        }
        private void DamgeParticle()
        {
            GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("Effects");
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().Play();
            GameObject g = LevelManager.Instance.DamageParticles.GetElement();
            //Debug.Log(GetComponent<Collider2D>().bounds.max.x);
            g.transform.position = new Vector2(transform.position.x, transform.position.y + (GetComponent<Collider2D>().bounds.max.y - GetComponent<Collider2D>().bounds.center.y));
        }
        private IEnumerator ChangeColor()
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            yield return new WaitForSeconds(0.1f);
            GetComponent<SpriteRenderer>().color = Color.white;
        }
        protected virtual void FixedUpdate() { }
        protected virtual void OnDamageRecived() { }
        protected virtual void OnDie()
        {
            _Respawn = false;
            DropMoney();
            gameObject.transform.position = new Vector2(1000, 1000);
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
            //gameObject.SetActive(false);
        }
        public SaveGameData.BossData Save()
        {
            Debug.Log(_Respawn);
            return new SaveGameData.BossData(_ID,_Respawn);
        }
        public void Load(SaveGameData.BossData[] _bossdata)
        {
            for(int i = 0; i< _bossdata.Length; i++)
            {
                if (_bossdata[i]._ID == _ID)
                    _Respawn = _bossdata[i]._BossRespawn;
            }
            if(!Respawn)
                gameObject.transform.position = new Vector2(1000, 1000);
        }
        private void DropMoney()
        {
            int moneyquantitytospawn = _MoneyQuantity;
            while (moneyquantitytospawn > 0)
            {
                float xr = UnityEngine.Random.Range(-90, 90);
                switch (moneyquantitytospawn)
                {
                    case >= 10:
                        GameObject go = LevelManager.Instance.Money10Pool.GetElement();
                        go.transform.position = transform.position;
                        go.GetComponent<Rigidbody2D>().AddForce(new Vector2(xr / 100, 1) * 5f, ForceMode2D.Impulse);
                        moneyquantitytospawn -= 10;
                        break;
                    case int i when moneyquantitytospawn >= 5 && moneyquantitytospawn < 10:
                        GameObject go1 = LevelManager.Instance.Money5Pool.GetElement();
                        go1.transform.position = transform.position;
                        go1.GetComponent<Rigidbody2D>().AddForce(new Vector2(xr / 100, 1) * 5f, ForceMode2D.Impulse);
                        moneyquantitytospawn -= 5;
                        break;
                    case < 5:
                        GameObject go2 = LevelManager.Instance.Money1Pool.GetElement();
                        go2.transform.position = transform.position;
                        go2.GetComponent<Rigidbody2D>().AddForce(new Vector2(xr / 100, 1) * 5f, ForceMode2D.Impulse);
                        moneyquantitytospawn -= 1;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
   
