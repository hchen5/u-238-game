using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StonesWarn : MonoBehaviour
{
    [SerializeField]
    float TimeToGo;
    float NormalGravity;
    private void Awake()
    {
        NormalGravity = GetComponent<Rigidbody2D>().gravityScale;
    }
    private void OnEnable()
    {
        StartCoroutine(WarnActive());
    }
    private IEnumerator WarnActive()
    {
        GetComponent<Rigidbody2D>().gravityScale = 0;
        transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(TimeToGo);
        transform.GetChild(0).gameObject.SetActive(false);
        GetComponent<Rigidbody2D>().gravityScale = NormalGravity;
    }
    private void OnDisable()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        GetComponent<Rigidbody2D>().gravityScale = NormalGravity;
        StopAllCoroutines();
    }
}
