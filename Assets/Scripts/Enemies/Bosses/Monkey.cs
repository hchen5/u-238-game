using EnemiesNS;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

namespace Bosses
{
    public class Monkey : BossParent
    {
        [SerializeField]
        private GameObject _Player;
        [Header("InitState")]
        [SerializeField]
        private float _TimeGroundPunchStuneInitBoss;
        [SerializeField]
        private float _GroundPunchGravityInitState;
        [Header("NormalState")]
        [SerializeField] 
        private float _TimeToAtackMele;
        [SerializeField]
        private float _TimeAtackMele;
        [SerializeField]
        private float _DistanceToAtackMele;
        [SerializeField]
        private int2 _Jumps;
        [SerializeField] 
        private float _JumpForce;
        [SerializeField]
        private int _DegreesWhenJump;
        [SerializeField]
        private float _DistanceToAtackSpecial1;
        [SerializeField]
        private int2 _ProbabilityAtacks;
        [SerializeField]
        private int _HealthToChangePhase;
        [SerializeField]
        private int2 _JumpsToRest;
        [SerializeField]
        private float _RestTime;
        private int _RdJumpsToRest;
        private int _RdJumpsRestDone;
        private int _JumpsDone;
        private int _JumpsToDone;
        private Rigidbody2D _Rigidbody;
        private enum MonkeyEnum {InitState, NormalState1, AtackSpecial1, AtackSpecial2, NormalState2 }
        private MonkeyEnum _State; 
        [Header("AtackSpecial 1")]
        [SerializeField]
        private int2 _TrhoughtStones;
        [SerializeField]
        private float _TimeBetweenStones;
        [SerializeField]
        private float _JumpCealingForce;
        [SerializeField]
        private float _GroundPunchGravity;
        [SerializeField]
        private float _GroundPunchDamage;
        [SerializeField]
        private float _TimeGroundPunchStuneBoss;
        [SerializeField]
        private float _TimeToSpawnGroundPunch;
        [SerializeField]
        private float _GroundPunchTriggerVelocity;
        [SerializeField]
        private Pool _StonesPool;
        [Header("AtackSpecial 2")]
        [SerializeField]
        private int2 _ProbabilityEnemyCucFly;
        [SerializeField]
        private float _OffSetSpawnCuC;
        [SerializeField]
        private float _OffSetSpawnFly;
        [SerializeField]
        private float _TimeToSpawnEnemies;
        [SerializeField]
        private Pool _CucsPool;
        [SerializeField]
        private Pool _FliesPool;
        [SerializeField]
        private int _EnemyLimit;
        [Header("TransitionPhase")]
        [SerializeField]
        private float _JumpForceTransition;
        [SerializeField]
        private Vector2 _PositionPhase2;
        [Header("Phase2")]
        [SerializeField]
        private float _DistanceToAtackMelePhase2;
        [SerializeField]
        private float _TimeToAtackPhase2;
        [SerializeField]
        private float _TimeAtackingPhase2;
        [SerializeField]
        private float _TimeToComprovePlayerPhase2;
        [SerializeField]
        private float _SpeedBossPhase2;
        [Header("DROP")]
        [SerializeField]
        private GameObject _UnlockedHabilitie;
        [SerializeField]
        private float _DrpedImpulse;
        private float _NormalGravity;
        private List<GameObject> _EnemiesList;
        private bool _EnemyTransitionPhase2 = true;
        bool _changePhase;
        public delegate void MonkeyDelegate();
        public event MonkeyDelegate OnInit;
        public event MonkeyDelegate OnChangePhase;
        public event MonkeyDelegate OnDieEvent;
        public event MonkeyDelegate OnGetReward;
        [Header("Animations")]
        [SerializeField]
        private BossAnimationScriptable _AnimationScriptable;
        [SerializeField]
        GameObject Sparks;
        private Animator _Anim;
        bool AtackSpecial1parche;
        private void Awake()
        {
            _changePhase = false;
            _DMG = _EnemyHealth;
            _RdJumpsToRest = UnityEngine.Random.Range(_JumpsToRest[0], _JumpsToRest[1]);
            _EnemiesList = new List<GameObject>();
            _Rigidbody = GetComponent<Rigidbody2D>();
            _NormalGravity = 3;
            _State = MonkeyEnum.InitState;
            _Anim = GetComponent<Animator>();
        }
        private void Start()
        {
            if (!_Respawn)
            {
                gameObject.transform.position = new Vector2(1000, 1000);
                gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
            }
            _CucsPool = _CucsPool.GetComponent<LevelManager>().WormEnemy;
            _FliesPool = _FliesPool.GetComponent<LevelManager>().FlyEnemy;
            GetComponentInChildren<TriggerEnter>().onPlayer += EnterTrigger;
        }
        public void InitBoss()
        {
            AtackSpecial1parche = false;
            gameObject.SetActive(true);
            GetComponent<Rigidbody2D>().gravityScale = _GroundPunchGravityInitState;
            _Anim.Play(_AnimationScriptable._Air.name);
        }
        private IEnumerator InitStuneBoss()
        {
            _Anim.Play(_AnimationScriptable._Iddle.name);
            yield return new WaitForSeconds(_TimeGroundPunchStuneInitBoss);
            OnInit?.Invoke();
            StartNormalState();
        }
        protected override void FixedUpdate()
        {
            switch (_State)
            {
                case MonkeyEnum.AtackSpecial1:
                    transform.position = new Vector2(_Player.transform.position.x, transform.position.y);
                    break; 
                case MonkeyEnum.NormalState2:
                    break;
            }
        }
        private void StartNormalState()
        {
            _Rigidbody.gravityScale = _NormalGravity;
            _State = MonkeyEnum.NormalState1;
            _JumpsDone = 0;
            int Prob1 = _Jumps[0];
            int Prob2 = _Jumps[1];
            _JumpsToDone = UnityEngine.Random.Range(Prob1, Prob2);
            CheckWhatToDo();
        }
        private void CheckWhatToDo()
        {
            Vector2 Bottom = new Vector2(transform.position.x, transform.position.y - GetComponent<CapsuleCollider2D>().size.y / 2);
            Vector2 PosL = new Vector2(transform.position.x - GetComponent<CapsuleCollider2D>().size.x / 2, transform.position.y);
            Vector2 PosR = new Vector2(transform.position.x + GetComponent<CapsuleCollider2D>().size.x / 2, transform.position.y);
            RaycastHit2D rhitR = Physics2D.Raycast(PosR, Vector2.right, 0.1f, LayerMask.GetMask("Obstacles"));
            RaycastHit2D rhitL = Physics2D.Raycast(PosL, -Vector2.right, 0.1f, LayerMask.GetMask("Obstacles"));
            RaycastHit2D rhitD = Physics2D.Raycast(Bottom, Vector2.up, Mathf.Infinity, LayerMask.GetMask("Player", "Obstacles"));
            if (_changePhase)
            {
                TransitionPhase2();
                return;
            }
            if (_RdJumpsToRest <= _RdJumpsRestDone)
            {
                StartCoroutine(Resting());
                return;
            }
            if (rhitD.collider != null && (rhitL.collider != null || rhitR.collider != null))
            {
                if (rhitD.collider.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
                {
                    AtackSpecial1();
                    return;
                }
            }
            if (_JumpsDone >= _JumpsToDone)
            {
                ChoseAtackSpecial();
                return;
            }
            if ((_Player.transform.position - transform.position).magnitude < _DistanceToAtackMele)
            {
                AtackMele();
                return;
            }
            else
            {
                Jump();
                return;
            }
        }
        private void TransitionPhase2()
        {
            _State = MonkeyEnum.NormalState2;
            _Rigidbody.AddForce(_JumpForceTransition * Vector2.up, ForceMode2D.Impulse);
            _Anim.Play(_AnimationScriptable._Jump.name);
        }
        private IEnumerator Resting()
        {
            _Anim.Play(_AnimationScriptable._Iddle.name);
            _RdJumpsRestDone = 0;
            _RdJumpsToRest = UnityEngine.Random.Range(_JumpsToRest[0], _JumpsToRest[1]);
            yield return new WaitForSeconds(_RestTime);
            CheckWhatToDo();
        }
        private void ChoseAtackSpecial()
        {
            ReLoadListEnemies();
            Vector2 PosL = new Vector2(transform.position.x - GetComponent<CapsuleCollider2D>().size.x / 2, transform.position.y);
            Vector2 PosR = new Vector2(transform.position.x + GetComponent<CapsuleCollider2D>().size.x / 2, transform.position.y);
            RaycastHit2D rhitR = Physics2D.Raycast(PosR, Vector2.right, 2.5f, LayerMask.GetMask("Obstacles"));
            RaycastHit2D rhitL = Physics2D.Raycast(PosL, -Vector2.right, 2.5f, LayerMask.GetMask("Obstacles"));
            if (rhitR.collider != null || rhitL.collider != null)
            {
                AtackSpecial1();
                return;
            }
            int RdAtk = UnityEngine.Random.Range(1, 101);
            if ((_Player.transform.position - transform.position).magnitude < _DistanceToAtackSpecial1)
            {
                AtackSpecial1();
                return;
            }
            if (RdAtk <= _ProbabilityAtacks[0] || _EnemiesList.Count >= _EnemyLimit)
                AtackSpecial1();
            else
                AtackSpecial2();
        }
        private void Jump()
        {
            _Anim.Play(_AnimationScriptable._Jump.name);
            Vector2 DirectionToPlayer = (_Player.transform.position - transform.position).normalized;
            Vector3 Direction;
            Vector2 FinalDirection;
            if (DirectionToPlayer.x > 0)
                Direction = -Vector3.forward;
            else
                Direction = Vector3.forward;
            FinalDirection = Quaternion.AngleAxis(_DegreesWhenJump, Direction) * Vector2.up;
            transform.forward = Direction;
            _Rigidbody.AddForce(FinalDirection * _JumpForce);
            _JumpsDone++;
            _RdJumpsRestDone++;
        }
        private void AtackMele()
        {
            //_JumpsDone = 0;
            Vector2 DirectionToPlayer = (_Player.transform.position - transform.position).normalized;
            Vector3 Direction;
            if (DirectionToPlayer.x > 0)
                Direction = -Vector3.forward;
            else
                Direction = Vector3.forward;
            transform.forward = Direction;
            _Anim.Play(_AnimationScriptable._Atack.name);
            //StartCoroutine(Atacking());
        }
        /*private IEnumerator Atacking()
        {
            yield return new WaitForSeconds(_TimeToAtackMele);
            transform.GetChild(0).gameObject.SetActive(true);
            yield return new WaitForSeconds(_TimeAtackMele);
            transform.GetChild(0).gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            CheckWhatToDo();
        }*/
        public void FinishAtack1()
        {
            CheckWhatToDo();
        }
        public void FinishAtack2()
        {
            CheckWhatToDoPhase2();
        }
        private void AtackSpecial1()
        {
            _Rigidbody.AddForce(Vector2.up * _JumpCealingForce);
            GetComponent<CapsuleCollider2D>().isTrigger = true;
            StartCoroutine(AtackSpecial1Coroutine());
        }
        private IEnumerator AtackSpecial1Coroutine()
        {
            AtackSpecial1parche = true;
            int RdStones = UnityEngine.Random.Range(_TrhoughtStones[0], _TrhoughtStones[1]);
            int StonesTrhoughts = 0;
            yield return new WaitForSeconds(_TimeBetweenStones);
            while (RdStones > StonesTrhoughts)
            {
                Sparks.SetActive(true);
                _State = MonkeyEnum.AtackSpecial1;
                GameObject obj = _StonesPool.GetElement();
                obj.transform.position = transform.position;
                StonesTrhoughts++;
                yield return new WaitForSeconds(_TimeBetweenStones);
            }
            _Rigidbody.gravityScale = _GroundPunchGravity;
            Sparks.SetActive(false);
            AtackSpecial1parche = false;
        }
        /*private IEnumerator AtackSpecial1GroundPunch()
        {
            _Anim.Play(_AnimationScriptable._GroundPunch.name);
            yield return new WaitForSeconds(_TimeToSpawnGroundPunch);
            GameObject trig1 = transform.GetChild(1).gameObject;
            GameObject trig2 = transform.GetChild(2).gameObject;
            trig1.SetActive(true);
            trig2.SetActive(true);
            trig1.GetComponent<Rigidbody2D>().velocity = trig1.transform.right * _GroundPunchTriggerVelocity;
            trig2.GetComponent<Rigidbody2D>().velocity = trig2.transform.right * _GroundPunchTriggerVelocity;
            Vector2 posTrig1 = trig1.transform.position;
            Vector2 posTrig2 = trig2.transform.position;
            yield return new WaitForSeconds(_TimeGroundPunchStuneBoss);
            trig1.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            trig2.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            trig1.SetActive(false);
            trig2.SetActive(false);
            trig1.transform.position = posTrig1;
            trig2.transform.position = posTrig2;
            StartNormalState();
        }*/
        private IEnumerator GroundPunchDamage()
        {
            GameObject trig1 = transform.GetChild(1).gameObject;
            GameObject trig2 = transform.GetChild(2).gameObject;
            trig1.SetActive(true);
            trig2.SetActive(true);
            trig1.GetComponent<Rigidbody2D>().velocity = trig1.transform.right * _GroundPunchTriggerVelocity;
            trig2.GetComponent<Rigidbody2D>().velocity = trig2.transform.right * _GroundPunchTriggerVelocity;
            Vector2 posTrig1 = trig1.transform.position;
            Vector2 posTrig2 = trig2.transform.position;
            yield return new WaitForSeconds(_TimeGroundPunchStuneBoss);
            trig1.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            trig2.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            trig1.SetActive(false);
            trig2.SetActive(false);
            trig1.transform.position = posTrig1;
            trig2.transform.position = posTrig2;
            StartNormalState();
        }
        private void AtackSpecial2()
        {
            _Anim.Play(_AnimationScriptable._Iddle.name);
            _State = MonkeyEnum.AtackSpecial2;
            StartCoroutine(AtackSpecial2Coroutine());
        }
        private IEnumerator AtackSpecial2Coroutine()
        {
            bool? CucDirection = null; // Right = True, Left = False, Not Spawn = Null
            bool? FlyDirection = null; // Right = True, Left = False, Not Spawn = Null
            GameObject obj;
            for (int i = 0; i < 2; i++)
            {
                int RdEnemie = UnityEngine.Random.Range(1, 101);
                if (RdEnemie < _ProbabilityEnemyCucFly[0])
                {
                    float PosX;
                    float PosY = GetComponent<Collider2D>().bounds.min.y;

                    if (CucDirection == null)
                    {
                        int RdDir = UnityEngine.Random.Range(0, 2);
                        if (RdDir == 0)
                        {
                            PosX = transform.position.x + transform.localScale.x / 2 + _OffSetSpawnCuC;
                            CucDirection = true;
                        }
                        else
                        {
                            PosX = transform.position.x - transform.localScale.x / 2 - _OffSetSpawnCuC;
                            CucDirection = false;
                        }
                    } else
                    {
                        if ((bool)CucDirection)
                            PosX = transform.position.x - transform.localScale.x / 2 - _OffSetSpawnCuC;
                        else
                            PosX = transform.position.x + transform.localScale.x / 2 + _OffSetSpawnCuC;
                    }

                    Vector2 Position = new Vector2 (PosX, PosY);
                    obj = _CucsPool.GetElement();
                    obj.transform.position = Position;
                } else
                {
                    float PosX;
                    float PosY = transform.position.y + transform.localScale.y / 2 + _OffSetSpawnFly;

                    if (FlyDirection == null)
                    {
                        int RdDir = UnityEngine.Random.Range(0, 2);
                        if (RdDir == 0)
                        {
                            PosX = transform.position.x + transform.localScale.x / 2 + _OffSetSpawnFly;
                            FlyDirection = true;
                        }
                        else
                        {
                            PosX = transform.position.x - transform.localScale.x / 2 - _OffSetSpawnFly;
                            FlyDirection = false;
                        }
                    }
                    else
                    {
                        if ((bool)FlyDirection)
                            PosX = transform.position.x - transform.localScale.x / 2 - _OffSetSpawnFly;
                        else
                            PosX = transform.position.x + transform.localScale.x / 2 + _OffSetSpawnFly;
                    }

                    Vector2 Position = new Vector2(PosX, PosY);
                    obj = _FliesPool.GetElement(); 
                    obj.GetComponent<FlyBehaviour>().SetPosition(Position);
                }
                _EnemiesList.Add(obj);
            }
            yield return new WaitForSeconds(_TimeToSpawnEnemies);
            _State = MonkeyEnum.NormalState1;
            StartNormalState();
        }
        private void ReLoadListEnemies()
        {
            List<GameObject> newlist = new List<GameObject>();
            foreach (GameObject obj in _EnemiesList) 
            { 
                if (obj != null) 
                {
                    if (obj.activeInHierarchy)
                    {
                        newlist.Add(obj);
                    }
                }
            }
            _EnemiesList = newlist;
        }
        protected override void OnDamageRecived()
        {
            if (_HealthToChangePhase >= _DMG)
                _changePhase = true;
            base.OnDamageRecived();
        }
        protected override void OnDie()
        {
            GameObject obj = Instantiate(_UnlockedHabilitie);
            obj.transform.position = transform.position;
            obj.GetComponent<Rigidbody2D>().AddForce(Vector2.up * _DrpedImpulse, ForceMode2D.Impulse);
            obj.GetComponent<UnlockSkill>().OnTake += GetReward;
            OnDieEvent?.Invoke();
            base.OnDie();
        }
        private void GetReward(GameObject obj)
        {
            obj.GetComponent<UnlockSkill>().OnTake -= GetReward;
            OnGetReward?.Invoke();
        }
        private void EnterTrigger()
        {
            if (_State == MonkeyEnum.NormalState2) 
            {
                _EnemyTransitionPhase2 = false;
                GetComponentInChildren<TriggerEnter>().onPlayer -= EnterTrigger;
                _Rigidbody.gravityScale = _NormalGravity;
                CheckWhatToDoPhase2();
            }
        }
        private void CheckWhatToDoPhase2()
        {
            Vector2 Direction = (_Player.transform.position - transform.position);
            transform.right = new Vector2(-Direction.normalized.x, 0);
            RaycastHit2D rhit = Physics2D.Raycast(transform.position, Direction.normalized, _DistanceToAtackMelePhase2, LayerMask.GetMask("Obstacles"));
            if (Direction.magnitude <= _DistanceToAtackMelePhase2 && rhit.collider == null)
            {
                _Rigidbody.velocity = Vector2.zero;
                _Anim.Play(_AnimationScriptable._Atack2.name);
            }
            else
                StartCoroutine(PerseguirPlayer());
        }
        private IEnumerator PerseguirPlayer()
        {
            _Anim.Play(_AnimationScriptable._Walk.name);
            Vector2 Direction = new Vector2((_Player.transform.position - transform.position).normalized.x, 0);
            _Rigidbody.velocity = new Vector2(Direction.x * _SpeedBossPhase2, _Rigidbody.velocity.y);
            yield return new WaitForSeconds(_TimeToComprovePlayerPhase2);
            CheckWhatToDoPhase2();
        }
        /*private IEnumerator AtackPhase2()
        {
            _Anim.Play(_AnimationScriptable._Atack.name);
            _Rigidbody.velocity = Vector2.zero;
            yield return new WaitForSeconds(_TimeToAtackPhase2);
            transform.GetChild(0).gameObject.SetActive(true);
            yield return new WaitForSeconds(_TimeAtackingPhase2);
            transform.GetChild(0).gameObject.SetActive(false);
            CheckWhatToDoPhase2();
        }*/
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacles")))
            {
                Vector2 Bottom = new Vector2(transform.position.x, transform.position.y - GetComponent<CapsuleCollider2D>().size.y / 2);
                RaycastHit2D rhitD = Physics2D.Raycast(Bottom, -Vector2.up, 2f, LayerMask.GetMask("Obstacles"));
                Vector2 Up = new Vector2(transform.position.x, transform.position.y + GetComponent<CapsuleCollider2D>().size.y / 2);
                RaycastHit2D rhitU = Physics2D.Raycast(Up, Vector2.up, 2f, LayerMask.GetMask("Obstacles"));
                if (rhitD.collider != null)
                {
                    if (_Rigidbody.gravityScale == _NormalGravity)
                    {
                        if (_State != MonkeyEnum.NormalState2)
                        {
                            _Rigidbody.gravityScale = 0;
                            _Rigidbody.velocity = Vector2.zero;
                        }
                        else
                        {
                            _Rigidbody.gravityScale = 0;
                            _Rigidbody.velocity = Vector2.zero;
                            GetComponent<CapsuleCollider2D>().isTrigger = false;
                            transform.position = _PositionPhase2;
                            _Anim.Play(_AnimationScriptable._Iddle.name);
                            OnChangePhase?.Invoke();
                        }
                    }
                } else if (rhitU.collider != null)
                {
                    GetComponent<CapsuleCollider2D>().isTrigger = false;
                    if (_State != MonkeyEnum.InitState)
                        _State = MonkeyEnum.NormalState1;
                }
            }
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacles")))
            {
                Vector2 Bottom = new Vector2(transform.position.x, transform.position.y - GetComponent<CapsuleCollider2D>().size.y / 2);
                RaycastHit2D rhitD = Physics2D.Raycast(Bottom, -Vector2.up, 3f, LayerMask.GetMask("Obstacles"));

                if (rhitD.collider != null && _State != MonkeyEnum.NormalState2 && !AtackSpecial1parche)
                {
                    _Rigidbody.velocity = Vector3.zero;
                    if (_Rigidbody.gravityScale == _NormalGravity)
                    {
                        CheckWhatToDo();
                    }
                    else if (_Rigidbody.gravityScale == _GroundPunchGravity && _State != MonkeyEnum.InitState)
                        _Anim.Play(_AnimationScriptable._GroundPunch.name);
                    else if (_State == MonkeyEnum.InitState)
                        StartCoroutine(InitStuneBoss());
                } 
            }
        }
        private void OnCollisionExit2D(Collision2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacles")))
            {
                //Vector2 UP = new Vector2(transform.position.x, transform.position.y - GetComponent<CapsuleCollider2D>().size.y / 2);
                //RaycastHit2D rhitU = Physics2D.Raycast(UP, -Vector2.up, 1f, LayerMask.GetMask("Obstacles"));
                if (_State == MonkeyEnum.NormalState2 && _EnemyTransitionPhase2)
                {
                    GetComponent<CapsuleCollider2D>().isTrigger = true;
                }
            }
        }
    }
}

