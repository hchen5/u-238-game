using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Barriles : MonoBehaviour
{
    [SerializeField]
    float _Velocity;
    [SerializeField]
    _DirectionX _DirectionEnum;
    [SerializeField]
    bool _Breakable;
    Rigidbody2D _Rigidbody;
    enum _DirectionX { left, right }
    Vector2 _Direction;
    Vector2 _InitDirection;
    [SerializeField]
    Sprite _SpriteBreak;
    [SerializeField]
    Sprite _SpriteNotBreak;
    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody2D>();
        if (_DirectionEnum == _DirectionX.left)
            _InitDirection = Vector2.left;
        else
            _InitDirection = Vector2.right;
    }
    /*void Start()
    {
        _Direction = _InitDirection;
        _Rigidbody.velocity = _Direction * _Velocity;
        StartCoroutine(CheckDirection());
    }*/
    private IEnumerator CheckDirection()
    {
        while (true)
        {
            Vector3 BottomLeft = transform.position + new Vector3(-(GetComponent<CircleCollider2D>().radius) - GetComponent<CircleCollider2D>().offset.x, GetComponent<CircleCollider2D>().offset.y, 0);
            Vector3 BottomRight = transform.position + new Vector3((GetComponent<CircleCollider2D>().radius) + GetComponent<CircleCollider2D>().offset.x, GetComponent<CircleCollider2D>().offset.y, 0);
            RaycastHit2D hit2dr = Physics2D.Raycast(BottomRight, Vector2.right, 0.1f, LayerMask.GetMask("Obstacles"));
            RaycastHit2D hit2dl = Physics2D.Raycast(BottomLeft, Vector2.left, 0.1f, LayerMask.GetMask("Obstacles"));
            if (_Direction == Vector2.left)
                transform.Rotate(new Vector3(0, 0, 50));
            else
                transform.Rotate(new Vector3(0, 0, -50));
            if (hit2dr.collider != null || hit2dl.collider != null)
            {
                _Direction = -_Direction;
                _Rigidbody.velocity = _Direction * _Velocity;
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("PlayerWeapon")))
        { 
            if (_Breakable)
            {
                GetComponent<Pooleable>().ReturnToPool();
            }
        }
    }
    public void SetBreakable(bool breakable)
    {
        if (breakable)
            GetComponent<SpriteRenderer>().sprite = _SpriteBreak;
        else
            GetComponent<SpriteRenderer>().sprite = _SpriteNotBreak;
        _Breakable = breakable;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        _Rigidbody.velocity = _Direction * _Velocity;
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }
    private void OnEnable()
    {
        _Direction = _InitDirection;
        _Rigidbody.velocity = _Direction * _Velocity;
        StartCoroutine(CheckDirection());
    }
}
