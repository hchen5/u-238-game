using SaveDataNS;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

namespace EnemiesNS
{
    public class CarnivorousPlantDetectCollider : MonoBehaviour,ISaveableEnemies
    {
        private GameObject _CarnivorousPlant;
        [SerializeField]
        private float _FixedCooldown = 5f;
        private float _Cooldown = 0f;
        private Vector3 _InitialPosition;
        private int _Index = -1;
        public void SetIndexWhenSpawn(int index)
        {
            _Index = index;
        }
        private void Awake()
        {
            _CarnivorousPlant = transform.GetChild(0).gameObject;
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Player") && _Cooldown <= 0)
            {
                if (!Raycast(collision.gameObject))
                {
                    _CarnivorousPlant.transform.position = transform.position;
                    _CarnivorousPlant.SetActive(true);
                    _CarnivorousPlant.GetComponent<CarnivorousPlantFlyTrapBehaviour>().AttackPlayer(collision.gameObject);
                    StartCoroutine(RefreshCooldown());
                }
            }
        }
        private bool Raycast(GameObject _Target)
        {
            RaycastHit2D hit2d = Physics2D.CircleCast(this.transform.position, _CarnivorousPlant.GetComponent<CircleCollider2D>().radius,_Target.transform.position - transform.position,
                Vector2.Distance(transform.position,_Target.transform.position), LayerMask.GetMask("Obstacles"));
            return hit2d;
        }
        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Player") && _Cooldown <= 0)
            {
                if (!Raycast(collision.gameObject))
                {
                    _CarnivorousPlant.transform.position = transform.position;
                    _CarnivorousPlant.SetActive(true);
                    _CarnivorousPlant.GetComponent<CarnivorousPlantFlyTrapBehaviour>().AttackPlayer(collision.gameObject);
                    StartCoroutine(RefreshCooldown());
                }
            }
        }
        public void returnpoole()
        {
            GetComponent<Pooleable>().ReturnToPool();
            _CarnivorousPlant.GetComponent<CarnivorousPlantFlyTrapBehaviour>().setstart();
        }
        private void OnEnable()
        {
            
        }
        private void OnDisable()
        {
            _Index = -1;
        }
        IEnumerator RefreshCooldown() 
        {
            _Cooldown = _FixedCooldown;
            while (_Cooldown > 0)
            {
                yield return new WaitForSeconds(0.1f);
                _Cooldown -= 0.1f;
            }
        }
        public SaveGameData.EnemiesData Save()
        {
            CarnivorousPlantFlyTrapBehaviour c = _CarnivorousPlant.GetComponent<CarnivorousPlantFlyTrapBehaviour>();
            return new SaveGameData.EnemiesData(_Index,c.SO._CanRespawn,transform.position,EnemyEnum.Carnivorous);
        }

        public void Load(SaveGameData.EnemiesData _enemiesdata)
        {
            throw new System.NotImplementedException();
        }
    }
}