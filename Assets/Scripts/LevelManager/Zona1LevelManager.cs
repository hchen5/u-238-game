using Bosses;
using FSMState;
using PlayerNS;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zona1LevelManager : MonoBehaviour
{
    [Header("Scene Objects")]
    [SerializeField]
    private GameObject _TrigerBoss1;
    [SerializeField] 
    private List<GameObject> _ClosedDoors;
    [SerializeField]
    private GameObject _Boss;
    [SerializeField]
    private GameObject _Player;
    [SerializeField]
    private GameObject _Techo;
    [Header("Boss")]
    [SerializeField]
    bool isBossDead;
    [SerializeField]
    private Vector2 _PositionCameraPhase1 = Vector2.zero;
    [SerializeField]
    private Vector2 _PositionCameraToMovePhase1 = Vector2.zero;
    [SerializeField]
    private GameObject _PlataformPhase2;
    [SerializeField]
    Transform _BarrilesSpawnPoint;
    [SerializeField]
    Pool BarrilesPool;
    [SerializeField]
    float TimeSpawnBarrile;
    [SerializeField]
    GameObject ReturnBarrilesYStonesTrigger;
    [SerializeField]
    Vector2 PosTriggerReturnPhase2;
    Vector2 PosTriggerReturnPhase1;
    bool BarrilRompible;
    private void Start()
    {
        PosTriggerReturnPhase1 = ReturnBarrilesYStonesTrigger.transform.position;
        if (/*!isBossDead &&*/ _Boss.GetComponent<Monkey>().Respawn)
            _TrigerBoss1.GetComponent<TriggerEnter>().onPlayer += InitBoss;
        else
        {
            _Techo.SetActive(false);
            _PlataformPhase2.SetActive(true);
        }
    }
    private void InitBoss()
    {
        CloseDoors();
        Camera.main.GetComponent<CameraInGame>().onCameraRaisePosition += CloseTransitionToInitBoss;
        _Player.GetComponent<NewFiniteStateMachine>().ChangeState<SM_PlayerOnIddle>();
        Camera.main?.GetComponent<CameraInGame>().GoToPosition(_PositionCameraPhase1, 0.1f);
    }
    private void CloseTransitionToInitBoss()
    {
        _Boss.GetComponent<Monkey>().InitBoss();
        _TrigerBoss1.GetComponent<TriggerEnter>().onPlayer -= InitBoss;
        Camera.main.GetComponent<CameraInGame>().onCameraRaisePosition -= CloseTransitionToInitBoss;
        _Boss.GetComponent<Monkey>().OnInit += FightBoss;
    }
    private void FightBoss()
    {
        _Player.GetComponent<NewFiniteStateMachine>().ChangeState<SM_PlayerOnGround>();
        _Boss.GetComponent<Monkey>().OnInit -= FightBoss;
        _Boss.GetComponent<Monkey>().OnChangePhase += TransitionPhase;
    }
    private void CloseDoors()
    {
        foreach(GameObject go in _ClosedDoors) 
            go.SetActive(true);
    }
    private void TransitionPhase()
    {
        _Player.GetComponent<NewFiniteStateMachine>().ChangeState<SM_PlayerOnIddle>();
        Camera.main.GetComponent<CameraInGame>().onCameraRaisePosition += CloseTransitionPhase2Boss;
        _Techo.SetActive(false);
        Camera.main?.GetComponent<CameraInGame>().GoToPosition(_PositionCameraToMovePhase1, 0.25f);
        _PlataformPhase2.SetActive(true);
        StartCoroutine(SpawnBarril());
    }
    private void CloseTransitionPhase2Boss()
    {
        Camera.main.GetComponent<CameraInGame>().onCameraRaisePosition -= CloseTransitionToInitBoss;
        Camera.main?.GetComponent<CameraInGame>().ChangeStatePlataform();
        _Player.GetComponent<NewFiniteStateMachine>().ChangeState<SM_PlayerOnGround>();
        ReturnBarrilesYStonesTrigger.transform.position = PosTriggerReturnPhase2;
        _Boss.GetComponent<Monkey>().OnDieEvent += OnDieFunc;
    }
    private void OnDieFunc()
    {
        _Boss.GetComponent<Monkey>().OnDieEvent -= OnDieFunc;
        _Boss.GetComponent<Monkey>().OnGetReward += OpenDoors;
        StopAllCoroutines();
        //ReturnBarrilesYStonesTrigger.transform.position = PosTriggerReturnPhase1;
    }
    private void OpenDoors()
    {
        _Boss.GetComponent<Monkey>().OnGetReward -= OpenDoors;
        foreach (GameObject go in _ClosedDoors)
            go.SetActive(false);
    }
    private void OnDisable()
    {
        if (_TrigerBoss1 != null)
            _TrigerBoss1.GetComponent<TriggerEnter>().onPlayer -= InitBoss;
    }
    private IEnumerator SpawnBarril()
    {
        while (true)
        {
            GameObject go = BarrilesPool.GetElement();
            go.transform.position = _BarrilesSpawnPoint.position;
            if (!BarrilRompible)
            {
                go.GetComponent<Barriles>().SetBreakable(BarrilRompible);
            }
            BarrilRompible = !BarrilRompible;
            yield return new WaitForSeconds(TimeSpawnBarrile);
        }
    }
}
