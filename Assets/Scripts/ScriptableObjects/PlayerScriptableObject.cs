using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjectsNS
{
    [CreateAssetMenu(fileName = "PlayerScriptableObject", menuName = "ScriptableObjects/PlayerSO")]
    public class PlayerScriptableObject : ScriptableObject
    {
        public int _Health;
        public float _SpeedForce;
        public float _SpeedForceLimiter;
        public float _MovementForceLimiter;
        public float _MovementSpeedLimiter;
        [Range(1f, 90f)]
        public float _ClimbJumpDegreesToClimb;
        [Range(1f, 90f)]
        public float _ClimbJumpDegreesToSeparate;
        public float _CrounchSpeed;
        public float _JumpForce;
        public float _DobleJumpForce;
        public float _ClimbJumpForce;
        public bool _DoubleJumpUnlocked;
        public bool _ClimbUnlocked;
        [Range(0.1f,1f)]
        public float _GravityScaleReduction;
        public float _GravityScaleWhenFalling;
        public float _OnEnemyCollisionForce;
        public float _TimeInmuneWhenCollisionEnemy;
        public float _DistanceTriggerBasicAtack;
        public float _ForcePogoJumpWhenAtack;
    }
}