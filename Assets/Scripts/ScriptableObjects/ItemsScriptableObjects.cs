using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ItemsScriptableObjects", menuName = "ScriptableObjects/ItemsSO")]

public class ItemsScriptableObjects : ScriptableObject
{
    public GameObject _ItemGameObject;
    public string _ItemName;
    public ItemType _ItemType;
    public int _Cost;
    public Sprite _ItemSprite;
}
public enum ItemType { Economy, Item, Talismans }
