using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SkillsNS.PlayerSkills;

[CreateAssetMenu(fileName = "SkillsPlayer", menuName = "ScriptableObjects/SkillsPlayer")]
public class SkillsSO : ScriptableObject
{
    public EnumSkills _SkillEnum;
    public string _SkillName;
    public string _SkillDescription;
    public Sprite _SkillSprite;
    public float _CoolDown;
}
