using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjectsNS
{
    [CreateAssetMenu(fileName = "EnemiesScriptableObjects", menuName = "ScriptableObjects/EnemySO")]
    public class EnemiesScriptableObjects : ScriptableObject
    {
        public EnemyEnum _EnemyType;
        public int _MaxHealth;
        public float _Speed;
        public int _DMG;
        public Sprite _Sprite;
        public int _MoneyQuantity;
        public bool _CanRespawn;
        public void DropMoney(Pool money1,Pool money5,Pool money10, Vector3 SpawnPosition, int addmoneyquantityperk) 
        {
            int moneyquantitytospawn = _MoneyQuantity + addmoneyquantityperk;
            while (moneyquantitytospawn > 0)
            {
                //float x = Random.Range(0, 2) == 0 ? -1 : 1;
                float xr = Random.Range(-90,90);
                switch (moneyquantitytospawn)
                {
                    case >= 10:
                        GameObject go = money10.GetElement();
                        go.transform.position = SpawnPosition;
                        go.GetComponent<Rigidbody2D>().AddForce(new Vector2(xr/100, 1)  * 5f,ForceMode2D.Impulse);
                        moneyquantitytospawn -= 10;
                        //Debug.Log("10");
                        break;
                    case int i when moneyquantitytospawn >= 5 && moneyquantitytospawn < 10:
                        GameObject go1 = money5.GetElement();
                        go1.transform.position = SpawnPosition;
                        go1.GetComponent<Rigidbody2D>().AddForce(new Vector2(xr/100, 1)  * 5f, ForceMode2D.Impulse);
                        moneyquantitytospawn -= 5;
                        //Debug.Log("5");
                        break;
                    case < 5:
                        GameObject go2 = money1.GetElement();
                        go2.transform.position = SpawnPosition;
                        go2.GetComponent<Rigidbody2D>().AddForce(new Vector2(xr / 100,1) * 5f, ForceMode2D.Impulse);
                        moneyquantitytospawn -= 1;
                        //Debug.Log("1");
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
