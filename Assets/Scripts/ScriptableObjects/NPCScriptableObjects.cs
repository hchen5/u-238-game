using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ScriptableObjectsNS
{
    [CreateAssetMenu(fileName = "NPCScriptableObjects", menuName = "ScriptableObjects/NPCSO")]
    public class NPCScriptableObjects : ScriptableObject
    {
        //public List<ItemsScriptableObjects> _NPCListItemsToSell;
        public List<ObjectsAndQuantityCanSell> _NPCListObjectsToSell;
        //public List<TalismaScriptableObject> _NPCListTalismanToSell;
        public TextAsset _NPCTextAsset;
        public Sprite _NPCSprite;
    }
    [Serializable]
    public struct ObjectsAndQuantityCanSell
    {
        public TypeObjectToSell _TypeObjectToSell;
        public TalismaScriptableObject _TalismaScriptableObject;
        public ItemsScriptableObjects _ItemsToSell;
        public int _Quantity;
    }
    public enum TypeObjectToSell { Item, Talisma }
}