using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BossScriptableObject", menuName = "ScriptableObjects/BossAnimSO")]
public class BossAnimationScriptable : ScriptableObject
{
    public AnimationClip _Iddle;
    public AnimationClip _Jump;
    public AnimationClip _Air;
    public AnimationClip _Atack;
    public AnimationClip _Atack2;
    public AnimationClip _GroundPunch;
    public AnimationClip _Walk;
}
