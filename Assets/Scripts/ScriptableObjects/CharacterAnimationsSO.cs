using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjectsNS
{
    [CreateAssetMenu(fileName = "NPCScriptableObjects", menuName = "ScriptableObjects/CharacterAnimationsSO")]
    public class CharacterAnimationsSO : ScriptableObject
    {
        public AnimationClip _Iddle;
        public AnimationClip _Walk;
        public AnimationClip _Air;
        public AnimationClip _Climb;
        public AnimationClip _CrounchIddle;
        public AnimationClip _Crounch;
        public AnimationClip _AtackForward;
        public AnimationClip _AtackDown;
        public AnimationClip _AtackUp;
    }
}
