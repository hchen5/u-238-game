using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemiePositionSO", menuName = "ScriptableObjects/EnemiePSO")]

public class EnemiePositionSO : ScriptableObject
{
    public List<EnemiePosition> _ListEnemyPositionToSpawn;
}
