using PlayerNS;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ScriptableObjectsNS
{
    [CreateAssetMenu(fileName = "TalismaScriptableObject", menuName = "ScriptableObjects/TalismaSO")]
    public class TalismaScriptableObject : ScriptableObject
    {
        public string _PerkName;
        public Sprite _TalismanSprite;
        public int _Cost;
        [TextArea]
        public string _TalismanText;
        public PerkType _PerkType;
        public float _Range;
        public int _DMG;
        public int _Health;
        public int _MoneyPercent;
        public float _Speed;
        public ReturnValue _DMGTime;
        public float _CoolDownReductionPercent;
        private ReturnValue perkt;
        public ReturnValue AddPerkToPlayer(PlayerBehaviour _P)
        {
            //float perk = 0;
            //ReturnValue perkreturn;
            switch (_PerkType)
            {
                case PerkType.Range:
                    perkt._PerkValue = _P.DefaultPlayerHitColliderLocalScale.x + _Range;
                    perkt._TotalTime = 0;
                    //perk = _P.DefaultPlayerHitColliderLocalScale.x + _Range;
                    break;
                case PerkType.Damage:
                    perkt._PerkValue = _P.DMG + _DMG;
                    perkt._TotalTime = 0;
                    //perk = _P.DMG + _DMG;
                    break;
                case PerkType.Health:
                    perkt._PerkValue = _P.PlayerScriptableObject._Health + _Health;
                    perkt._TotalTime = 0;
                    //perk = _P.PlayerScriptableObject._Health + _Health;
                    break;
                case PerkType.Money:
                    perkt._PerkValue = _MoneyPercent;
                    perkt._TotalTime = 0;
                    //perk = _MoneyPercent;
                    break;
                case PerkType.Bleeding:
                    perkt = _DMGTime;
                    break;
                case PerkType.Cooldown:
                    perkt._PerkValue = _CoolDownReductionPercent;
                    perkt._TotalTime = 0;
                    break;
                case PerkType.Speed:
                    perkt._PerkValue = _P.PlayerScriptableObject._SpeedForce + _Speed;
                    perkt._TotalTime = 0;
                    //perk = _P.PlayerScriptableObject._SpeedForce + _Speed;
                    break;
            }
            return perkt;
            //return perk;
        }
    }
    [Serializable]
    public struct ReturnValue
    {
        public float _PerkValue;
        public float _TimeToBleed;
        public float _TotalTime;
    }
    public enum PerkType { Range, Damage, Health, Bleeding, Cooldown, Money, Speed }
}