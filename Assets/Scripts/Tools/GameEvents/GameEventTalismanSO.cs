using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ScriptableObjectsNS
{
    [CreateAssetMenu(fileName = "GameEventTalismanSO", menuName = "GameEvents/GameEvents/GameEventTalismanSO")]
    public class GameEventTalismanSO : GameEvent<TalismaScriptableObject>
    {

    }
}
