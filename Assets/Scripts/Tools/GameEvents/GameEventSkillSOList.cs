using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEventSkillsSO", menuName = "GameEvents/GameEvents/GameEventSkillsSOList")]
public class GameEventSkillSOList : GameEvent<List<SkillsSO>>
{
}
