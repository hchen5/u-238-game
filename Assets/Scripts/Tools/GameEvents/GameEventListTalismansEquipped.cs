using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "GameEventListTalismansEquipped", menuName = "GameEvents/GameEvents/GameEventListTalismansEquipped")]
public class GameEventListTalismansEquipped : GameEvent<List<TalismaScriptableObject>>
{
    
}
