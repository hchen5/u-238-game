using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEventSkillsSO", menuName = "GameEvents/GameEvents/GameEventSkillsSO")]
public class GameEventSkillSo : GameEvent<SkillsSO>
{

}
