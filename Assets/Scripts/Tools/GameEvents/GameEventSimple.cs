using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ScriptableObjectsNS
{
    [CreateAssetMenu(fileName = "GameEventSimple", menuName = "GameEvents/GameEvents/GameEventSimple")]
    public class GameEventSimple : GameEvent
    {

    }
}