using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "GameEventItemSO", menuName = "GameEvents/GameEvents/GameEventItemSO")]

public class GameEventItemSO: GameEvent<ItemsScriptableObjects, TalismaScriptableObject>
{
    
}
