using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "GameEventSimple", menuName = "GameEvents/GameEvents/GameEventObjectBoolean")]
public class GameEventObjectBoolean : GameEvent<GameObject, bool> { }