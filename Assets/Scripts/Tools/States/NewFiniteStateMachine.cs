using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace FSMState
{
    public class NewFiniteStateMachine : MonoBehaviour
    {
        IState[] _States;

        IState _CurrentState = null;
        public IState CurrentState => _CurrentState;

        private void Awake()
        {
            _States = GetComponents<IState>();

            foreach (IState state in _States)
                state.Exit();
        }

        public T GetState<T>() where T : IState
        {
            return (T) _States.First(state => state.GetType() == typeof(T));
        }

        public void ChangeState<T>() where T : IState
        {
            T state = GetState<T>();
            Assert.IsFalse(state == null);

            if (_CurrentState != null)
            {
                _CurrentState.Exit();
            }

            _CurrentState = state;
            _CurrentState.Init();
        }
    }
}
