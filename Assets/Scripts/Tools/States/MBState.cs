using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSMState
{
    public abstract class MBState : MonoBehaviour, IState
    {
        public virtual void Init()
        {
            //Debug.Log("Init" + GetComponent<NewFiniteStateMachine>().CurrentState);
            enabled = true;
        }

        public virtual void Exit()
        {
            //Debug.Log("Exit" + GetComponent<NewFiniteStateMachine>().CurrentState);
            enabled = false;
        }
    }
}

