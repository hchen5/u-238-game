using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FSMState
{
    public interface IState
    {
        public void Init();
        public void Exit();
    }
}
