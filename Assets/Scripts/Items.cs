using PlayerNS;
using SaveDataNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ItemsNS
{
    public class Items : MonoBehaviour, ISaveableObjects
    {
        [SerializeField]
        private ItemsScriptableObjects _ItemSO;
        public ItemsScriptableObjects ItemSO { get => _ItemSO; }
        private Collider2D _Collider;
        private Rigidbody2D _Rigidbody;
        private void Start()
        {
            _Collider = GetComponent<Collider2D>();
            _Rigidbody = GetComponent<Rigidbody2D>();
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "EffectorCollect")
            {
                _Collider.isTrigger = true;
                _Rigidbody.gravityScale = 0;
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == "EffectorCollect")
            {
                _Collider.isTrigger = false;
                _Rigidbody.gravityScale = 1;
                _Rigidbody.velocity = Vector3.zero;
            }
        }

        public SaveGameData.ObjectsInTheWorldData Save()
        {
            return new SaveGameData.ObjectsInTheWorldData(transform.position);
        }

        public void Load(bool active)
        {
            if(!active)
                gameObject.SetActive(false);
        }
    }
}

