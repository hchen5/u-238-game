using EnemiesNS;
using PlayerNS;
using SaveDataNS;
using ScriptableObjectsNS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Experimental.AI;
using UnityEngine.SceneManagement;

public enum EnemyEnum { Worm, Fly, Mole,Carnivorous, Mimetic, Firefly, CavernTrollShoot, CavernTrollSuicide }
public class LevelManager : MonoBehaviour
{
    private static LevelManager _Instance;
    public static LevelManager Instance => _Instance;
    [Header("Pools Money")]
    [SerializeField]
    private Pool _Money1Pool;
    [SerializeField]
    private Pool _Money5Pool;
    [SerializeField]
    private Pool _Money10Pool;
    [Header("Pool Enemies")]
    [SerializeField]
    private Pool _WormEnemy;
    [SerializeField]
    private Pool _FlyEnemy;
    [SerializeField]
    private Pool _CarnivorousEnemy;
    [SerializeField]
    private Pool _MimeticEnemy;
    [SerializeField]
    private Pool _CavernTrollSuicide;
    [SerializeField]
    private Pool _CavernTrollShoot;
    [SerializeField]
    private Pool _MoleEnemy;
    [Header("Bullet")]
    [SerializeField]
    private Pool _Bullet;
    [Header("List Spawn Enemy in position")]
    [SerializeField]
    private List<EnemiePosition> _EnemiesInPosition;
    private List<int> _EnemieCanRespawn = new List<int>();
    private List<EnemiePosition> _EnemieCanRespawn2 = new List<EnemiePosition>();
    [Header("DataBase")]
    [SerializeField]
    private DataBaseSO _DataBaseSO;
    [Header("Canvas")]
    [SerializeField]
    private HudCanvas _HudCanvas;
    public Pool Money1Pool { get => _Money1Pool; }
    public Pool Money5Pool { get => _Money5Pool; }
    public Pool Money10Pool { get => _Money10Pool; }
    public Pool WormEnemy { get => _WormEnemy; }
    public Pool FlyEnemy { get => _FlyEnemy; }
    public Pool CarnivorousEnemy { get => _CarnivorousEnemy; }
    public DataBaseSO DataBaseSO { get => _DataBaseSO; }
    public List<EnemiePosition> EnemiesInPosition { get => _EnemiesInPosition; }
    public List<EnemiePosition> EnemieCanRespawn { get => _EnemieCanRespawn2; }
    public Pool Bullet { get => _Bullet;}
    public Pool MoleEnemy { get => _MoleEnemy;}
    [SerializeField]
    Pool _DamageParticles;
    public Pool DamageParticles => _DamageParticles;
    private void Awake()
    {
        SceneManager.sceneLoaded += LoadGameSceneLoaded;
        if (_Instance == null)
        {
            _Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        //DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        _HudCanvas.OnStop += Pause;
        if (!SaveGameManager.Instance.Loaded)
        {
            Debug.Log("NoLoaded");
            SetCanRespawnEnemies();
            SpawnEnemies(EnemiesInPosition);
        }
        LoadAudioEffects();
    }
    private void LoadAudioEffects()
    {
        GameManager.Instance.GetComponent<SettingsGame>().SetAllAudioSourceEffects();
    }
    private bool Pause(bool Stop)
    {
        if (Stop)
            Time.timeScale = 1;
        else
            Time.timeScale = 0;
        return true;
    }
    private void LoadGameSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name != "World")
        {
            Destroy(gameObject);
        }
        SceneManager.sceneLoaded -= LoadGameSceneLoaded;
    }
    public void SpawnEnemies(List<EnemiePosition> e)
    {
        GameObject g = null;
        for (int i = 0; i < e.Count; i++)
        {
            switch (e[i].EnemyType)
            {
                case EnemyEnum.Worm:
                    g = _WormEnemy.GetElement();
                    g.GetComponent<WormBehaviour>().SetIndexWhenSpawn(i, e[i]._Position.position);
                    g.transform.position = e[i]._Position.position;
                    /*
                    if (g.GetComponent<WormBehaviour>().EnemySo._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }*/
                    break;
                case EnemyEnum.Mole:
                    g = _MoleEnemy.GetElement();
                    g.GetComponent<MoleBehaviour>().SetIndexWhenSpawn(i, e[i]._Position.position);
                    g.transform.position = e[i]._Position.position;
                    /*
                    if (g.GetComponent<MoleBehaviour>().EnemySo._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }*/
                    break;
                case EnemyEnum.Fly:
                    g = _FlyEnemy.GetElement();
                    g.GetComponent<FlyBehaviour>().SetIndexWhenSpawn(i, e[i]._Position.position);
                    g.GetComponent<FlyBehaviour>().SetPosition(e[i]._Position.position);
                    /*
                    if (g.GetComponent<FlyBehaviour>().EnemySo._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }*/
                    break;
                case EnemyEnum.Carnivorous:
                    g = _CarnivorousEnemy.GetElement();
                    g.transform.GetComponent<CarnivorousPlantDetectCollider>().SetIndexWhenSpawn(i);
                    g.transform.position = e[i]._Position.position;
                    /*
                    if (g.transform.GetChild(0).GetComponent<CarnivorousPlantFlyTrapBehaviour>().EnemySo._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }*/
                    break;
                case EnemyEnum.Mimetic:
                    g = _MimeticEnemy.GetElement();
                    g.GetComponent<MimeticBehaviour>().SetIndexWhenSpawn(i, e[i]._Position.position);
                    g.GetComponent<MimeticBehaviour>().SetCampamentCollider(e[i]._Position.transform.GetComponent<CircleCollider2D>(),true);
                    g.GetComponent<MimeticBehaviour>().SetPosition(e[i]._Position.position);
                    /*
                    if (g.GetComponent<MimeticBehaviour>().EnemySo._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }*/
                    break;
                case EnemyEnum.Firefly:
                    g = _MimeticEnemy.GetElement();
                    g.GetComponent<MimeticBehaviour>().SetIndexWhenSpawn(i, e[i]._Position.position);
                    g.GetComponent<MimeticBehaviour>().SetCampamentCollider(e[i]._Position.transform.GetComponent<CircleCollider2D>(), false);
                    g.GetComponent<MimeticBehaviour>().SetPosition(e[i]._Position.position);
                    //_EnemieCanRespawn.Add(i);
                    break;
                case EnemyEnum.CavernTrollSuicide:
                    g = _CavernTrollSuicide.GetElement();
                    g.transform.GetComponent<CavernTrollSuicideBehaviour>().SetIndexWhenSpawn(i, e[i]._Position.position);
                    g.transform.position = e[i]._Position.position;
                    /*
                    if (g.transform.GetComponent<CavernTrollSuicideBehaviour>().EnemySo._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }*/
                    break;
                case EnemyEnum.CavernTrollShoot:
                    g = _CavernTrollShoot.GetElement();
                    g.transform.GetComponent<CavernTrollShootBehaviour>().SetIndexWhenSpawn(i, e[i]._Position.position);
                    g.transform.position = e[i]._Position.position;
                    /*
                    if (g.transform.GetComponent<CavernTrollShootBehaviour>().EnemySo._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }*/
                    break;
            }
        }
    }
    public void LoadCanRespawn()
    {
        _EnemieCanRespawn2.Clear();
        for (int i = 0; i < _EnemieCanRespawn.Count; i++) 
        {
            _EnemieCanRespawn2.Add(EnemiesInPosition[_EnemieCanRespawn[i]]);
        }
        SpawnEnemies(_EnemieCanRespawn2);
    }
    public void SetCanRespawnEnemies()
    {
        //Debug.Log(EnemiesInPosition.Count);
        _EnemieCanRespawn.Clear();
        for (int i = 0; i < EnemiesInPosition.Count; i++)
        {
            switch (EnemiesInPosition[i].EnemyType)
            {
                case EnemyEnum.Worm:
                    if (_DataBaseSO.GetEnemies(EnemyEnum.Worm)._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }
                    break;
                case EnemyEnum.Fly:
                    if (_DataBaseSO.GetEnemies(EnemyEnum.Fly)._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }
                    break;
                case EnemyEnum.Mole:
                    if (_DataBaseSO.GetEnemies(EnemyEnum.Mole)._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }
                    break;
                case EnemyEnum.Carnivorous:
                    if (_DataBaseSO.GetEnemies(EnemyEnum.Carnivorous)._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }
                    break;
                case EnemyEnum.Mimetic:
                    if (_DataBaseSO.GetEnemies(EnemyEnum.Mimetic)._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }
                    break;
                case EnemyEnum.Firefly:
                    _EnemieCanRespawn.Add(i);
                    break;
                case EnemyEnum.CavernTrollSuicide:
                    if (_DataBaseSO.GetEnemies(EnemyEnum.CavernTrollSuicide)._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }
                    break;
                case EnemyEnum.CavernTrollShoot:
                    if (_DataBaseSO.GetEnemies(EnemyEnum.CavernTrollShoot)._CanRespawn)
                    {
                        _EnemieCanRespawn.Add(i);
                    }
                    break;
            }
        }
    }
    private void OnDisable()
    {
        _HudCanvas.OnStop -= Pause;
    }
}
[Serializable]
public struct EnemiePosition
{
    public EnemyEnum EnemyType;
    public Transform _Position;
}

