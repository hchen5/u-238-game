using ScriptableObjectsNS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using static SaveDataNS.SaveGameData;
using static SkillsNS.PlayerSkills;
namespace SaveDataNS
{
    public class SaveGameData
    {
        //Structs
        [Serializable]
        public struct PlayerData
        {
            public Vector3 _Position;
            public int _Vida;
            public int _Money;
            public bool _DoubleJumpUnlocked;
            public bool _ClimbUnlocked;
            public List<PlayerInventory> _Inventory;
            public List<string> _TalismanUnlocked;
            public List<string> _TalismanEquipped;
            public List<EnumSkills> _AttacksUnlocked;
            public List<EnumSkills> _AttacksEquipped;
            public List<string> _PilarName;
            public List<Vector3> _PilarPosition;
            public PlayerData(Vector3 _position, int vida,int money,bool doublejump, bool climb, List<PlayerInventory> inventory, List<string> talismanunlocked, List<string> talismanequipped, 
                List<EnumSkills> skills, List<EnumSkills> skillsequipped, List<string> pilarname, List<Vector3> pilarpos)
            {
                _Position = _position;
                _Vida = vida;
                _Money = money;
                _DoubleJumpUnlocked = doublejump;
                _ClimbUnlocked = climb;
                _Inventory = inventory;
                _TalismanUnlocked = talismanunlocked;
                _AttacksUnlocked = skills;
                _TalismanEquipped = talismanequipped;
                _AttacksEquipped = skillsequipped;
                _PilarName = pilarname;
                _PilarPosition = pilarpos;
            }
        }
        [Serializable]
        public struct ObjectsInTheWorldData
        {
            public Vector3 _Position;
            public ObjectsInTheWorldData(Vector3 position)
            {
                _Position = position;
            }
        }
        [Serializable]
        public struct NPCSData
        {
            public int _Index;
            public List<string> _ItemsCantSell;
            public int _Itinerate;
            public List<string> _Item;
            public List<int> _Qantity;
            public List<string> _TalismaCantSell;
            public NPCSData(List<string> itemscantsell, int itinerate,int index,List<string> item, List<int> qantity,List<string> t)
            {
                _ItemsCantSell = itemscantsell;
                _Itinerate = itinerate;
                _Index = index;
                _Item = item;
                _Qantity = qantity;
                _TalismaCantSell = t;
            }
        }
        [Serializable]
        public struct EnemiesData
        {
            public int _Index;
            public bool _CanRespawn;
            public Vector3 _Transform;
            public EnemyEnum _Type;
            public EnemiesData(int index, bool c, Vector3 t,EnemyEnum e)
            {
                _Index = index;
                _CanRespawn = c;
                _Transform = t;
                _Type = e;
            }
        }
        [Serializable]
        public struct BossData
        {
            public int _ID;
            public bool _BossRespawn;
            public BossData(int id,bool a)
            {
                _ID = id;
                _BossRespawn = a;
            }
        }
        [Serializable]
        public struct DoorInfo
        {
            public int _ID;
            public bool _Opened;
            public DoorInfo(int id, bool opened)
            {
                _ID= id;
                _Opened = opened;
            }
        }
        public void SavePlayerData(ISaveablePlayer _playerData)
        {
            _Player = _playerData.Save();
        }
        public void  SaveObjectsInTheWorldData(ISaveableObjects[] _ObjectsData)
        {
            _Objects = new ObjectsInTheWorldData[_ObjectsData.Length];
            for (int i = 0; i < _ObjectsData.Length; i++)
            {
                _Objects[i] =_ObjectsData[i].Save(); 
            }
        }
        public void SaveNPCData(ISaveableNPC[] _NPCData)
        {
            _NPCs = new NPCSData[_NPCData.Length];
            for (int i = 0; i < _NPCData.Length; i++)
            {
                _NPCs[i] = _NPCData[i].Save();
            }
        }
        public void SaveEnemiesData(ISaveableEnemies[] _EnemiesData)
        {
            _Enemies = new EnemiesData[_EnemiesData.Length];
            for (int i = 0; i < _EnemiesData.Length; i++)
            {
                _Enemies[i] = _EnemiesData[i].Save();
            }
        }
        public void SaveBossData(ISaveableBoss[] _BossData)
        {
            _Boss = new BossData[_BossData.Length];
            for (int i = 0; i < _BossData.Length; i++)
            {
                _Boss[i] = _BossData[i].Save();
            }
        }
        public void SaveDoor(ISaveableDoor[] _DoorData)
        {
            _DoorInfo = new DoorInfo[_DoorData.Length];
            for (int i = 0; i < _DoorData.Length; i++)
            {
                _DoorInfo[i] = _DoorData[i].Save();
            }
        }
        //Variables
        public PlayerData _Player;
        public ObjectsInTheWorldData[] _Objects;
        public NPCSData[] _NPCs;
        public EnemiesData[] _Enemies;
        public BossData[] _Boss;
        public DoorInfo[] _DoorInfo;
    }
    //Interfaces
    public interface ISaveablePlayer
    {
        public PlayerData Save();
        public void Load(PlayerData _playerdata);
    }  
    public interface ISaveableObjects
    {
        public ObjectsInTheWorldData Save();
        public void Load(bool active);
    }  
    public interface ISaveableNPC
    {
        public NPCSData Save();
        public void Load(NPCSData _npcdata);
    }
    public interface ISaveableEnemies
    {
        public EnemiesData Save();
        public void Load(EnemiesData _enemiesdata);
    }
    public interface ISaveableBoss
    {
        public BossData Save();
        public void Load(BossData[] _bossdata);
    }
    public interface ISaveableDoor 
    {
        public DoorInfo Save();
        public void Load(DoorInfo[] _doorinfo);
    }
    ///////////////////////////////////////////
    [Serializable]
    public struct PlayerInventory
    {
        public string _Name;
        public int _Stacked;
    }
}