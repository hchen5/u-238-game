using ItemsNS;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ScriptableObjectsNS
{
    [CreateAssetMenu(fileName = "DataBaseSO", menuName = "ScriptableObjects/DataBaseSO")]
    public class DataBaseSO : ScriptableObject
    {
        public List<ItemsScriptableObjects> _AllItemsSO;
        public List<TalismaScriptableObject> _AllTalismansSO;
        public List<EnemiesScriptableObjects> _EnemieSO;
        public ItemsScriptableObjects GetItem(string name)
        {
            return _AllItemsSO.FirstOrDefault(item => item._ItemName == name);
        }
        public TalismaScriptableObject GetTalismans(string name)
        {
            return _AllTalismansSO.FirstOrDefault(talismans => talismans._PerkName == name);
        }
        public EnemiesScriptableObjects GetEnemies(EnemyEnum enemyenum)
        {
            return _EnemieSO.FirstOrDefault(enemie => enemie._EnemyType == enemyenum);
        }
    }
}
