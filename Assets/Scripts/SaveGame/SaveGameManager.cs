using Bosses;
using EnemiesNS;
using ItemsNS;
using NPCNS;
using PlayerNS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SaveDataNS
{
    public class SaveGameManager : MonoBehaviour
    {
        private string saveFileName = "SavedGame1.json";
        public string saveFileNamePublic => saveFileName;
        private static SaveGameManager _Instance;
        private string _PathData;
        private bool _NewGame = false;
        [SerializeField]
        private string _GameSceneNameToLoad = "World";
        private bool _Loaded = false;
        public static SaveGameManager Instance => _Instance;
        public bool Loaded { get => _Loaded;}

        private void Awake()
        {
            if (_Instance == null)
            {
                _Instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            DontDestroyOnLoad(gameObject);
        }
        public void SetSaveFileName(string name)
        {
            saveFileName = name;
        }
        public void SaveNewGame()
        {
            Debug.Log("Save");
            _NewGame = true;
            SceneManager.sceneLoaded += LoadGameSceneLoaded;
            SceneManager.LoadScene(_GameSceneNameToLoad);
        }
        public void SaveData()
        {
            ISaveablePlayer saveablePlayer = FindObjectOfType<PlayerBehaviour>();
            ISaveableObjects[] saveableObjects = FindObjectsOfType<Items>();
            ISaveableNPC[] saveableNPCs = FindObjectsOfType<NPCScript>();
            ISaveableEnemies[] saveableEnemies = FindObjectsOfType<Enemy>();
            ISaveableEnemies[] saveableEnemies2 = FindObjectsOfType<CarnivorousPlantDetectCollider>();
            ISaveableEnemies[] Allsaveableenemies = saveableEnemies.Concat(saveableEnemies2).ToArray();
            ISaveableBoss[] saveableBosses = FindObjectsOfType<BossParent>();
            ISaveableDoor[] saveableDoors = FindObjectsOfType<DoorScript>();
            SaveGameData data = new SaveGameData();
            data.SaveObjectsInTheWorldData(saveableObjects);
            data.SavePlayerData(saveablePlayer);
            data.SaveNPCData(saveableNPCs);
            data.SaveEnemiesData(Allsaveableenemies);
            data.SaveBossData(saveableBosses);
            data.SaveDoor(saveableDoors);
            string jsonData = JsonUtility.ToJson(data);
            try
            {
                Debug.Log(jsonData);
                string rutaAnterior = Path.Combine(Application.persistentDataPath, "DataSaved");
                string rutaFinal = Path.Combine(Application.persistentDataPath, "DataSaved", saveFileName);
                if (!Directory.Exists(rutaAnterior))
                {
                    Directory.CreateDirectory(rutaAnterior);
                }
                File.WriteAllText(rutaFinal, jsonData);
                //Debug.Log(Path.Combine(Application.persistentDataPath, "DataSaved", saveFileName));
            }
            catch (Exception e)
            {
                Debug.LogError($"Error while trying to save {Path.Combine(Application.persistentDataPath, "DataSaved" ,saveFileName)} with exception {e}");
            }
        }
        public void LoadData(string pathfile)
        {
            try
            {
                _PathData = pathfile;
                SceneManager.sceneLoaded += LoadGameSceneLoaded;
                SceneManager.LoadScene(_GameSceneNameToLoad);
                //Debug.Log(_GameSceneNameToLoad);
            }
            catch (Exception e)
            {
                Debug.LogError($"Error while trying to load {Path.Combine(Application.persistentDataPath, saveFileName)} with exception {e}");
            }
        }
        IEnumerator SaveNewGameC()
        {
            yield return new WaitForSeconds(1f);
            SaveData();
            _NewGame = false;
            _Loaded = false;
        }
        private void LoadGameSceneLoaded(Scene scene, LoadSceneMode mode) 
        {
            SceneManager.sceneLoaded -= LoadGameSceneLoaded;
            if (_NewGame)
            {
                StartCoroutine(SaveNewGameC());
                return;
            }
            _Loaded = true;
            if (_PathData == null)
                return;
            string rutaFinal = Path.Combine(Application.persistentDataPath, "DataSaved", _PathData);
            Debug.Log(rutaFinal);
            string jsonData = File.ReadAllText(rutaFinal);
            Debug.Log(jsonData);
            SaveGameData data = new SaveGameData();
            JsonUtility.FromJsonOverwrite(jsonData, data);
            PlayerBehaviour player = FindObjectOfType<PlayerBehaviour>();
            LoadNPCs(data._NPCs);
            LoadItems(data._Objects);
            LoadDoor(data._DoorInfo);
            player.Load(data._Player);
            LoadBosses(data._Boss);
            LoadEnemy(data);
        }
        private void LoadDoor(SaveGameData.DoorInfo[] doors)
        {
            DoorScript[] doorScript = FindObjectsOfType<DoorScript>();
            for (int j = 0; j < doorScript.Length; j++)
            {
                doorScript[j].Load(doors);
            }
        }
        private void LoadBosses(SaveGameData.BossData[] bosses)
        {
            BossParent[] bp = FindObjectsOfType<BossParent>();
            for (int j = 0; j < bp.Length; j++)
            {
                bp[j].Load(bosses);
            }
        }
        private void LoadNPCs(SaveGameData.NPCSData[] npcs)
        {
            NPCScript[] npc = FindObjectsOfType<NPCScript>();
            for (int i = 0; i < npc.Length; i++)
            {
                for (int j = 0; j < npcs.Length; j++)
                {
                    if (npc[i].Index == npcs[j]._Index)
                    {
                        npc[i].Load(npcs[j]);
                    }
                }
            }
        }
        private void LoadItems(SaveGameData.ObjectsInTheWorldData[] Itemsdata)
        {
            Items[] items = FindObjectsOfType<Items>();
            for (int i = 0; i < items.Length; i++)
            {
                bool a = false;
                for (int j = 0; j < Itemsdata.Length; j++)
                {
                    if ((int)items[i].transform.position.x == (int)Itemsdata[j]._Position.x && (int)items[i].transform.position.y == (int) Itemsdata[j]._Position.y)
                    {
                        a = true;
                    }
                }
                items[i].Load(a);
            }
        }
        private void LoadEnemy(SaveGameData enemie)
        {
            /*
            LevelManager.Instance.SetCanRespawnEnemies();
            //Tots els que poden respawnejat list
            List<int> enemyindex = new List<int>(LevelManager.Instance.EnemieCanRespawn);
            List<EnemiePosition> enemielist =  new List<EnemiePosition>();
            enemielist.Clear();
            //Comprobar que los enemigos que pueden respawnear y a�adir los que no puede respawnear pero no lo has matado
            for (int i = 0; i < enemie._Enemies.Length; i++)
            {
                if (enemyindex.Contains(enemie._Enemies[i]._Index) == false)
                {
                    Debug.Log(enemie._Enemies[i]._Index);
                    enemyindex.Add(enemie._Enemies[i]._Index);
                }
            }
            for (int i = 0; i< LevelManager.Instance.EnemiesInPosition.Count; i++)
            {
                bool a = false;
                for (int j = 0; j < enemyindex.Count; j++)
                {
                    if (i == enemyindex[j] && !a)
                    {
                        Debug.Log(i +" "+ enemyindex[j]);
                        enemielist.Add(LevelManager.Instance.EnemiesInPosition[enemyindex[j]]);
                        a=true;
                        //Debug.Log(enemyindex[j] + " " + LevelManager.Instance.EnemiesInPosition[enemyindex[j]].EnemyType);
                    }
                }
            }
            
            Debug.Log(enemielist.Count);
            LevelManager.Instance.SpawnEnemies(enemielist);*/
            LevelManager.Instance.SetCanRespawnEnemies();
            List<EnemiePosition> enemielist = new List<EnemiePosition>();
            for (int i = 0; i < enemie._Enemies.Length; i++)
            {
                if (!enemie._Enemies[i]._CanRespawn)
                {
                    EnemiePosition e = new EnemiePosition();
                    for (int j = 0; j < LevelManager.Instance.EnemiesInPosition.Count; j++)
                    {
                        if (LevelManager.Instance.EnemiesInPosition[j]._Position.position == enemie._Enemies[i]._Transform)
                        {
                            e._Position = LevelManager.Instance.EnemiesInPosition[j]._Position;
                        }
                    }
                    e.EnemyType = enemie._Enemies[i]._Type;
                    enemielist.Add(e);
                }
            }
            LevelManager.Instance.LoadCanRespawn();
            LevelManager.Instance.SpawnEnemies(enemielist);
        }
    }
}
