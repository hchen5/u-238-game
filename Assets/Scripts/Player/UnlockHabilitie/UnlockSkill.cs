using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SkillsNS.PlayerSkills;

public class UnlockSkill : Interactable
{
    [SerializeField]
    Sprite _Sprite = null;
    [SerializeField]
    EnumSkills _Skill = EnumSkills.None;
    [SerializeField]
    bool _UnlockClimb = false;
    [SerializeField]
    bool _UnlockDobleJump = false;
    public delegate void UnlockSkillDelegate(GameObject obj);
    public event UnlockSkillDelegate OnTake;

    public EnumSkills Skills => _Skill;
    public bool UnlokedClimb => _UnlockClimb;
    public bool UnlockDobleJump => _UnlockDobleJump;

    private void Start()
    {
        _Sprite = GetComponent<SpriteRenderer>().sprite = _Sprite;
    }
    protected void OnTriggerStay2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        Vector2 Bottom = new Vector2(transform.position.x, transform.position.y - GetComponent<CircleCollider2D>().radius / 2 + GetComponent<CircleCollider2D>().offset.y);
        Vector2 Up = new Vector2(transform.position.x, transform.position.y + GetComponent<CircleCollider2D>().radius / 2 - GetComponent<CircleCollider2D>().offset.y);
        RaycastHit2D rhitD = Physics2D.Raycast(Bottom, Vector2.down, 0.4f, LayerMask.GetMask("Obstacles"));
        RaycastHit2D rhitU = Physics2D.Raycast(Up, Vector2.up, 2f, LayerMask.GetMask("Obstacles"));
        if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacles")) && rhitD.collider != null && rhitU.collider == null)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().gravityScale = 0;
        }
        if (collision.tag == "Player")
            collision.GetComponent<PlayerBehaviour>().OnInteractable += OnInteract2;
    }
    protected override void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerExit2D(collision);
        if (collision.tag == "Player" && gameObject.activeInHierarchy)
            StartCoroutine(DesuscribeRoutine(collision));
    }
    private IEnumerator DesuscribeRoutine(Collider2D collision)
    {
        yield return new WaitForSeconds(1);
        collision.GetComponent<PlayerBehaviour>().OnInteractable -= OnInteract2;
    }
    protected void OnInteract2()
    {
        OnTake?.Invoke(gameObject);
    }
}
