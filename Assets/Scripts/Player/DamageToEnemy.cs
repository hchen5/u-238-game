using PlayerNS;
using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageToEnemy : MonoBehaviour
{
    [SerializeField]
    private int _Damage;
    [SerializeField]
    private bool _CanUseBleedPerk;
    private bool _BleedPerkActive = false;
    private ReturnValue _ReturnValue;
    private bool _MoneyPerkActive = false;
    private int _MoneyPerKPercent;
    public int Damage { get => _Damage; set => _Damage = value; }
    public bool BleedPerkActive { get => _BleedPerkActive;}
    public ReturnValue ReturnValue { get => _ReturnValue;}
    public bool MoneyPerkActive { get => _MoneyPerkActive;}
    public int MoneyPerKPercent { get => _MoneyPerKPercent;}
    public bool CanUseBleedPerk { get => _CanUseBleedPerk;}

    public void BleedSetting(bool BleedPerkActive, ReturnValue perkvalue)
    {
        if (_CanUseBleedPerk) {
            _BleedPerkActive = BleedPerkActive;
            _ReturnValue = perkvalue;
        } 
    }
    public void MoneyPerkSetting(bool moneyperkactive, int moneyperkpercent)
    {
        _MoneyPerkActive = moneyperkactive;
        _MoneyPerKPercent = moneyperkpercent;
    }
}
