using FSMState;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace PlayerNS
{
    public class BasicAtack : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Enemy")))
            {
                if (transform.parent.GetComponent<SM_PlayerAtack>().AtackDown)
                {
                    transform.parent.GetComponent<Rigidbody2D>().velocity = new Vector2(transform.parent.GetComponent<Rigidbody2D>().velocity.x, 0);
                    transform.parent.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, transform.parent.GetComponent<PlayerBehaviour>().PlayerScriptableObject._ForcePogoJumpWhenAtack), ForceMode2D.Impulse);
                }
            }
        }
    }
}
