using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerNS
{
    /*public class PlayerActions : MonoBehaviour
    {
        PlayerBehaviour _PlayerBehaviour;
        Rigidbody2D _RigidBody2D;
        Vector2 _ForceDirection;
        Vector2 _LookDirection;
        bool _IsCrounching = false;
        //public Vector2 ForceDirection { get => _ForceDirection;}
        private PlayerScriptableObject _PlayerSO;
        private float _CapsuleCollider2DScale;
        private Vector3 _TransformScale;
        [SerializeField]
        private LayerMask _GroundMask;
        private void Awake()
        {
            _RigidBody2D = GetComponent<Rigidbody2D>();
            _PlayerBehaviour = GetComponent<PlayerBehaviour>();
            _PlayerBehaviour.Inputs.FindActionMap("Movement");
        }
        private void Start()
        {
            _PlayerSO = _PlayerBehaviour.PlayerScriptableObject;
            /*_PlayerBehaviour.Inputs.FindAction("Crunch").started += CrounchPlayer;
            _PlayerBehaviour.Inputs.FindAction("Crunch").canceled += CancelCrounchPlayer;
            _CapsuleCollider2DScale = GetComponent<CapsuleCollider2D>().size.y;
            _TransformScale = transform.localScale;
        }
        /// <summary>
        /// A�adir velocidad al player
        /// </summary>
        /// <param name="v"></param>
        /*public void MovePlayer(Vector2 v) 
        {
            _ForceDirection = v;
        }
        /// <summary>
        /// A�ade fuerza de impulso en el vector2D up al RigidBody2d
        /// </summary>
        /// <param name="JumpForce">La fuerza de impulso que le a�adiras al RigidBody</param>
        public void JumpPlayer() 
        {
            _RigidBody2D.AddForce(Vector2.up * _PlayerSO._JumpForce, ForceMode2D.Impulse);
        }
        public void CrounchPlayer(InputAction.CallbackContext context) 
        {
            _PlayerBehaviour.NormalCollider.enabled = false;
            _PlayerBehaviour.CrounchCollider.enabled = true;
            _IsCrounching = true;
        }
        public void CancelCrounchPlayer(InputAction.CallbackContext context)
        {
            if (CheckIfUnderPlatform())
                StartCoroutine(CheckIfUnderPlatformAndStand());
            else
            {
                _PlayerBehaviour.NormalCollider.enabled = true;
                _PlayerBehaviour.CrounchCollider.enabled = false;
                _IsCrounching = false;
            }
        }
        bool CheckIfUnderPlatform()
        {
            bool hitRAndL = false;
            RaycastHit2D hitl = Physics2D.Raycast(_PlayerBehaviour.NormalCollider.transform.position - new Vector3((_PlayerBehaviour.NormalCollider.transform.localScale.x / 2), 0, 0), Vector2.up, _PlayerBehaviour.NormalCollider.transform.localScale.y +0.1f,_GroundMask);
            Debug.DrawRay(_PlayerBehaviour.NormalCollider.transform.position - new Vector3((_PlayerBehaviour.NormalCollider.transform.localScale.x / 2), 0, 0)
                , new Vector2(0, (_PlayerBehaviour.NormalCollider.transform.localScale.y + 0.1f)),Color.red, 2f);

            RaycastHit2D hitr = Physics2D.Raycast(_PlayerBehaviour.NormalCollider.transform.position + new Vector3((_PlayerBehaviour.NormalCollider.transform.localScale.x /2),0,0),
                Vector2.up, _PlayerBehaviour.NormalCollider.transform.localScale.y + 0.1f, _GroundMask);
            Debug.DrawRay(_PlayerBehaviour.NormalCollider.transform.position + new Vector3((_PlayerBehaviour.NormalCollider.transform.localScale.x / 2), 0, 0), 
                new Vector2(0, (_PlayerBehaviour.NormalCollider.transform.localScale.y + 0.1f)),
                Color.blue, 2f);
            if (!hitl & !hitr) 
            {
                hitRAndL = false;
            }
            else
                hitRAndL = true;
            return hitRAndL;
        }
        IEnumerator CheckIfUnderPlatformAndStand() 
        {
            while (CheckIfUnderPlatform()) 
            {
                yield return new WaitForSeconds(0.5f);
            }
            _PlayerBehaviour.NormalCollider.enabled = true;
            _PlayerBehaviour.CrounchCollider.enabled = false;
            _IsCrounching = false;
        }
        public void JumpPlayerClimb(Vector2 JumpVector)
        {
            _RigidBody2D.AddForce(JumpVector * _PlayerSO._ClimbJumpForce, ForceMode2D.Impulse);
        }
    }*/
}
