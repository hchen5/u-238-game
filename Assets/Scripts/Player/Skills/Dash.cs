using PlayerNS;
using SkillsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SkillsNS.PlayerSkills;

namespace SkillsNS
{
    public class Dash : MBSkills
    {
        [SerializeField]
        float _ImpulseForce;
        protected override void Awake()
        {
            _MySkill = EnumSkills.Dash;
            base.Awake();
        }
        public override void Atack()
        {
            if (!isCoolDownDone) return;
            Vector2 Direction = GetComponent<PlayerBehaviour>().LookDirection;
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 0);
            GetComponent<Rigidbody2D>().AddForce(Direction * _ImpulseForce, ForceMode2D.Impulse);
            base.Atack();
        }
    }
}
