using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SkillsNS.PlayerSkills;

namespace SkillsNS
{
    public class FireBall : MBSkills
    {
        [SerializeField]
        Pool _Pool;
        [SerializeField]
        float _Velocity;
        [SerializeField]
        float _SpawnLife;
        protected override void Awake()
        {
            _MySkill = EnumSkills.FireBall;
            base.Awake();
        }
        public override void Atack()
        {
            if (!isCoolDownDone) return;
            Vector2 Direction = GetComponent<PlayerBehaviour>().ShootDirection;
            GameObject FireBall = _Pool.GetElement();
            FireBall.transform.position = transform.position;
            FireBall.SetActive(true);
            FireBall.GetComponent<Rigidbody2D>().velocity = Direction * _Velocity;
            FireBall.GetComponent<DamageToEnemy>().MoneyPerkSetting(perkmoneyactive,perkmoney);
            StartCoroutine(SpawnLife(FireBall));
            base.Atack();
        }
        private IEnumerator SpawnLife(GameObject FireBall)
        {
            yield return new WaitForSeconds(_SpawnLife);
            _Pool.ReturnElement(FireBall);
        }
    }
}
