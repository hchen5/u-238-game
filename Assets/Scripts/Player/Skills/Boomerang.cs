using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using static SkillsNS.PlayerSkills;
using static System.Net.WebRequestMethods;

namespace SkillsNS
{
    public class Boomerang : MBSkills
    {
        [SerializeField]
        GameObject _Boomerang;
        [SerializeField]
        float _Velocity;
        [SerializeField]
        float _maxVelocity;
        [SerializeField]
        float _CoolDownWhenReturn;
        [SerializeField]
        float _SpawnLife;
        [SerializeField]
        float _ResistenceVelocityByTime;
        private Coroutine BoomerangCoroutine;
        private bool _ReturnBoomerang;
        public bool ReturnBoomerang => _ReturnBoomerang;
        private bool _TP;
        protected override void Awake()
        {
            _MySkill = EnumSkills.Boomerang;
            base.Awake();
        }
        protected override void Start()
        {
            _Boomerang.GetComponent<BoomerangObject>().PlayerEnter += OnBoomerangReturn;
            _TP = true;
            base.Start();
        }
        public override void Atack()
        {
            if (!_TP)
            {
                TP();
                return;
            }
            if (!isCoolDownDone) return;
            _TP = false;
            _ReturnBoomerang = false;
            Vector2 Direction = GetComponent<PlayerBehaviour>().ShootDirection;
            _Boomerang.transform.position = transform.position;
            _Boomerang.SetActive(true);
            _Boomerang.GetComponent<DamageToEnemy>().MoneyPerkSetting(perkmoneyactive, perkmoney);
            BoomerangCoroutine = StartCoroutine(TimeToReturn(-Direction));
            base.Atack();
        }
        private void TP()
        {
            if (CheckIfTP())
            {
                OnBoomerangReturn();
                transform.position = _Boomerang.transform.position;
            }
        }
        private bool CheckIfTP()
        {
            CapsuleCollider2D NormalColider = (CapsuleCollider2D) GetComponent<PlayerBehaviour>().NormalCollider;
            RaycastHit2D rhitR = Physics2D.Raycast(_Boomerang.transform.position, Vector2.right, NormalColider.size.x / 2, LayerMask.GetMask("Obstacles"));
            RaycastHit2D rhitL = Physics2D.Raycast(_Boomerang.transform.position, -Vector2.right, NormalColider.size.x / 2, LayerMask.GetMask("Obstacles"));
            RaycastHit2D rhitD = Physics2D.Raycast(_Boomerang.transform.position, -Vector2.up, NormalColider.size.y / 2, LayerMask.GetMask("Obstacles"));
            RaycastHit2D rhitU = Physics2D.Raycast(_Boomerang.transform.position, Vector2.up, NormalColider.size.y / 2, LayerMask.GetMask("Obstacles"));
            if (rhitR.collider == null && rhitL.collider == null && rhitD.collider == null && rhitU.collider == null)
                return true;
            return false;
        }
        private IEnumerator TimeToReturn(Vector2 Direction)
        {
            float Velocity = -_Velocity;
            float CurrentTime = 0;
            float MaxVelocity = _maxVelocity;
            while (true)
            {
                CurrentTime += 0.01f;
                Vector2 Dir = Direction;
                if (Velocity > MaxVelocity)
                    Velocity = MaxVelocity;
                else if (Velocity < -MaxVelocity)
                    Velocity = -MaxVelocity;
                else
                    Velocity += _ResistenceVelocityByTime;
                if (Velocity > 0)
                {
                    _ReturnBoomerang = true;
                    Dir = (transform.position - _Boomerang.transform.position).normalized;
                }
                _Boomerang.GetComponent<Rigidbody2D>().velocity = Velocity * Dir;
                if (CurrentTime > _SpawnLife)
                {
                    _Boomerang.SetActive(false);
                    break;
                }
                yield return new WaitForSeconds(0.01f);
            }
        }
        private void OnBoomerangReturn()
        {
            _TP = true;
            StopCoroutine(BoomerangCoroutine);
            CoolDown = _CoolDownWhenReturn;
            CurrentTime = 0;
            _Boomerang.SetActive(false);
        }
        private void OnDestroy()
        {
            if (_Boomerang != null)
                _Boomerang.GetComponent<BoomerangObject>().PlayerEnter -= OnBoomerangReturn;
        }
    }
}
