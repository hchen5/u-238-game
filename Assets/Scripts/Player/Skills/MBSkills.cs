using PlayerNS;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using static SkillsNS.PlayerSkills;

namespace SkillsNS
{
    public abstract class MBSkills : MonoBehaviour, ISkills
    {
        [SerializeField]
        private SkillsSO _SkillSO;
        public SkillsSO SkillSO => _SkillSO;
        private float _CoolDown;
        protected float CoolDown;
        private float _CurrentCoolDown;
        private bool _isCoolDownDone = true;
        protected float CurrentTime;
        protected bool isCoolDownDone => _isCoolDownDone;
        protected EnumSkills _MySkill;
        public EnumSkills MySkill => _MySkill;
        private Coroutine _MyCoolDownCoroutine;
        private float _CoolDownCanvas;
        protected bool perkmoneyactive = false;
        protected int perkmoney;
        public delegate void UpdateTimeHud(int idx, float maxTime, float currentTime);
        public event UpdateTimeHud _OnUpdate;
        public delegate void DrawSkillDelegate(int idx, Sprite img);
        public event DrawSkillDelegate _DrawSkill;
        protected virtual void Awake()
        {
            GetComponent<PlayerBehaviour>().OnPlayerMoneyPerk += PerkMoney;
        }
        protected virtual void Start()
        {
            _CoolDown = _SkillSO._CoolDown;
            _CurrentCoolDown = _CoolDown;
            _CoolDownCanvas = 0;
        }
        public virtual void Atack() 
        {
            CoolDown = _CurrentCoolDown;
            if (_isCoolDownDone)
                _MyCoolDownCoroutine = StartCoroutine(CoolDownRoutine());
        }
        private IEnumerator CoolDownRoutine()
        {
            _isCoolDownDone = false;
            CurrentTime = 0;
            while (CurrentTime < CoolDown) 
            {
                _CoolDownCanvas = CoolDown - CurrentTime;
                CurrentTime += 1;
                CallCanvas(_CoolDownCanvas);
                yield return new WaitForSeconds(1f);
            }
            _isCoolDownDone = true;
            CallCanvas(0);
        }
        public void ResetCoolDown()
        {
            if (_MyCoolDownCoroutine != null)
                StopCoroutine(_MyCoolDownCoroutine);
            _isCoolDownDone = true;
            CallCanvas(0);
        }
        public void ChangeCoolDown(int PerCent)
        {
            _CurrentCoolDown = _CoolDown - (_CoolDown * ((float)PerCent / 100));
        }
        protected void PerkMoney(bool a, int money)
        {
            perkmoneyactive = a;
            perkmoney= money;
        }
        private int GetIdx()
        {
            int idx = 10;
            int iteration = 0;
            foreach (EnumSkills ms in GetComponent<PlayerBehaviour>().MySkills)
            {
                if (ms == _MySkill)
                {
                    idx = iteration;
                    break;
                }
                iteration++;
            }
            return idx;
        }
        private void CallCanvas(float CurrentTime)
        {
            _OnUpdate?.Invoke(GetIdx(), _CoolDown, CurrentTime);
        }
        public void DrawSkill(int idx = -1, Sprite spr = null)
        {
            if (idx != -1) 
                _DrawSkill?.Invoke(idx, spr);
            else
                _DrawSkill?.Invoke(GetIdx(), SkillSO._SkillSprite);
        }
    }
}

