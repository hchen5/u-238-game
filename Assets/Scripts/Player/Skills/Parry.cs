using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SkillsNS.PlayerSkills;

namespace SkillsNS
{
    public class Parry : MBSkills
    {
        public delegate void ParryDelegate(bool Active);
        public event ParryDelegate SetParry;
        private Coroutine _MyCoroutine;
        [SerializeField]
        private float _ParryTime;
        protected override void Awake()
        {
            _MySkill = EnumSkills.Parry;
            base.Awake();
        }
        public override void Atack()
        {
            if (!isCoolDownDone) return;
            _MyCoroutine = StartCoroutine(ParryCoroutine());
            base.Atack();
        }
        private IEnumerator ParryCoroutine()
        {
            SetParry?.Invoke(true);
            yield return new WaitForSeconds(_ParryTime);
            SetParry?.Invoke(false);
        }
    }
}