using FSMState;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace SkillsNS
{
    public class PlayerSkills : MonoBehaviour
    {
        public enum EnumSkills { Dash, FireBall, Boomerang, Parry, None };
        MBSkills[] _Skills;
        private void Awake()
        {
            _Skills = GetComponents<MBSkills>();
        }
        public MBSkills GetSkill(EnumSkills es)
        {
            return (MBSkills) _Skills.First(Skill => Skill.MySkill == es);
        }
        public void DoSkill(EnumSkills es)
        {
            if (es != EnumSkills.None) 
            {
                MBSkills Skill = GetSkill(es);
                Skill.Atack();
            }
        }
    }
}
