using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillsNS
{
    public class BoomerangObject : MonoBehaviour
    {
        public delegate void BoomerangObjectDelegate();
        public event BoomerangObjectDelegate PlayerEnter;
        private void Start()
        {
            Physics2D.IgnoreLayerCollision(gameObject.layer, LayerMask.NameToLayer("BoomerangAtravesable"));
        }
        private void Update()
        {
            transform.Rotate(new Vector3(0, 0, 5));
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
            {
                if (collision.gameObject.tag == "Player")
                {
                    if (collision.gameObject.GetComponent<Boomerang>().ReturnBoomerang)
                        PlayerEnter?.Invoke();
                }
            }
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Enemy")))
            {
                if (LayerMask.LayerToName(collision.gameObject.layer) == "Enemy")
                {
                    GetComponent<Collider2D>().isTrigger = true;
                }
            }
        }
        private void OnDisable()
        {
            GetComponent<Collider2D>().isTrigger = true;
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
            {
                if (collision.gameObject.tag == "Player")
                {
                    GetComponent<Collider2D>().isTrigger = false;
                }
            }
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Enemy")))
            {
                if (LayerMask.LayerToName(collision.gameObject.layer) == "Enemy")
                {
                    GetComponent<Collider2D>().isTrigger = false;
                }
            }
        }
    }
}
