using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillsNS
{
    public class FireBallObject : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacles"))) 
            {
                gameObject.GetComponent<Pooleable>().ReturnToPool();
            }
        }
    }
}
