using FSMState;
using ScriptableObjectsNS;
using SkillsNS;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;
using UnityEngine.InputSystem;
using static PlayerNS.PlayerBehaviour;
using static SkillsNS.PlayerSkills;

namespace PlayerNS
{
    public class SM_PlayerOnGround : MBState
    {
        PlayerBehaviour _PlayerBehaviour;
        NewFiniteStateMachine _StateMachine;
        Rigidbody2D _RigidBody2D;
        private PlayerScriptableObject _PlayerSO;
        public delegate void GroundDelegate(bool Active);
        public event GroundDelegate onInventory;
        private bool _Save;
        private void Awake()
        {
            _PlayerBehaviour = GetComponent<PlayerBehaviour>();
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _RigidBody2D = GetComponent<Rigidbody2D>();
            _PlayerSO = _PlayerBehaviour.PlayerScriptableObject;
        }
        private void Start()
        {
            _Save = false; 
        }
        public override void Init()
        {
            //Debug.Log(_PlayerBehaviour);
            onInventory?.Invoke(false);
            _PlayerBehaviour.Inputs.FindAction("Movement").performed += Movement;
            _PlayerBehaviour.Inputs.FindAction("Movement").canceled += Movement;
            _PlayerBehaviour.Inputs.FindAction("Interact").performed += Interact;
            _PlayerBehaviour.Inputs.FindAction("Crunch").performed += Crunch;
            _PlayerBehaviour.Inputs.FindAction("Jump").performed += Jump;
            _PlayerBehaviour.Inputs.FindAction("Skills").performed += Skill;
            _PlayerBehaviour.Inputs.FindAction("Atack").performed += Atack;
            _PlayerBehaviour.Inputs.FindAction("Inventary").performed += Inventory;
            _PlayerBehaviour.MovePlayer(_PlayerBehaviour.Inputs.FindAction("Movement").ReadValue<Vector2>());
            if (_PlayerBehaviour.Inputs.FindAction("Crunch").ReadValue<float>() == 1)
                _StateMachine.ChangeState<SM_PlayerOnCrunch>();
        }
        private void Inventory(InputAction.CallbackContext context)
        {
            if (_Save)
            {
                onInventory?.Invoke(true);
                _StateMachine.ChangeState<SM_PlayerOnIddle>();
            }
        }
        private void Skill(InputAction.CallbackContext context)
        {
            int AtackSlot = context.action.GetBindingIndexForControl(context.control);
            if (AtackSlot > 3)
                AtackSlot -= 4;
            if (_PlayerBehaviour.MySkills.Count > AtackSlot)
                GetComponent<PlayerSkills>().DoSkill(_PlayerBehaviour.MySkills[AtackSlot]);
        }
        private void Jump(InputAction.CallbackContext context)
        {
            //Debug.Log("ASDasd");
            _RigidBody2D.AddForce(Vector2.up * _PlayerSO._JumpForce, ForceMode2D.Impulse);
            _StateMachine.ChangeState<SM_PlayerOnAir>();
        }
        private void Movement(InputAction.CallbackContext context)
        {
            Vector2 Direction = context.ReadValue<Vector2>();
            _PlayerBehaviour.MovePlayer(Direction);
        }
        private void Interact(InputAction.CallbackContext context)
        {
            bool ChangeState = _PlayerBehaviour.RaiseInteract(_Save);
            if (ChangeState)
                _StateMachine.ChangeState<SM_PlayerOnIddle>();
        }
        private void Crunch(InputAction.CallbackContext context)
        {
            _StateMachine.ChangeState<SM_PlayerOnCrunch>();
        }
        private void Atack(InputAction.CallbackContext context)
        {
            _PlayerBehaviour.Atacking();
            _StateMachine.ChangeState<SM_PlayerAtack>();
            //transform.GetChild(0).GetComponent<BasicAtack>().Atack();
        }
        public override void Exit()
        {
            if (_PlayerBehaviour != null)
            {
                _PlayerBehaviour.Inputs.FindAction("Movement").performed -= Movement;
                _PlayerBehaviour.Inputs.FindAction("Movement").canceled -= Movement;
                _PlayerBehaviour.Inputs.FindAction("Interact").performed -= Interact;
                _PlayerBehaviour.Inputs.FindAction("Crunch").performed -= Crunch;
                _PlayerBehaviour.Inputs.FindAction("Jump").performed -= Jump;
                _PlayerBehaviour.Inputs.FindAction("Skills").performed -= Skill;
                _PlayerBehaviour.Inputs.FindAction("Atack").performed -= Atack;
                _PlayerBehaviour.Inputs.FindAction("Inventary").performed -= Inventory;
            }
            StopAllCoroutines();
            base.Exit();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Pilar")
                _Save = true;
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == "Pilar")
                _Save = false;
        }
        private void OnCollisionExit2D(Collision2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacles")) && _StateMachine.GetState<SM_PlayerOnGround>().Equals(_StateMachine.CurrentState))
            {
                CapsuleCollider2D NormalCollider = (CapsuleCollider2D)_PlayerBehaviour.NormalCollider;
                //Vector2 BottomL = new Vector2(transform.position.x - NormalCollider.size.x / 2, transform.position.y - NormalCollider.size.y / 2);
                //Vector2 BottomR = new Vector2(transform.position.x + NormalCollider.size.x / 2, transform.position.y - NormalCollider.size.y / 2);
                Vector2 Bottom = new Vector2(transform.position.x, transform.position.y - NormalCollider.size.y / 2 + NormalCollider.offset.y);
                //RaycastHit2D rhitDL = Physics2D.Raycast(BottomL, -Vector2.up, 0.02f, LayerMask.GetMask("Obstacles"));
                //RaycastHit2D rhitDR = Physics2D.Raycast(BottomR, -Vector2.up, 0.02f, LayerMask.GetMask("Obstacles"));
                RaycastHit2D rhitD = Physics2D.Raycast(Bottom, -Vector2.up, 1f, LayerMask.GetMask("Obstacles"));
                if (rhitD.collider == null)
                {
                    _StateMachine.ChangeState<SM_PlayerOnAir>();
                }
            }
        }
    }
}
