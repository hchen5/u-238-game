using System;
using System.Collections;
using System.Collections.Generic;
using FSMState;
using NPCNS;
using UnityEngine;
using UnityEngine.InputSystem;


namespace PlayerNS
{
    public class SM_PlayerOnIddle : MBState
    {
        NewFiniteStateMachine _StateMachine;
        PlayerBehaviour _PlayerBehaviour;
        bool _Text;
        public delegate bool? TextDelegate();
        public event TextDelegate onText;
        public delegate void InventaryDelegate(bool Active);
        public event InventaryDelegate onInventory;
        public delegate bool InputPressed();
        public InputPressed WhenEscape;
        private IState _LastState;
        private void Awake()
        {
            _Text = false;
            _PlayerBehaviour = GetComponent<PlayerBehaviour>();
            _StateMachine = GetComponent<NewFiniteStateMachine>();
        }
        private void Start()
        {
            _PlayerBehaviour.OnIddle += LastState;
        }
        public override void Init()
        {
            _PlayerBehaviour.StopAudio();
            if (Time.timeScale == 1)
                GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Iddle);
            GetComponent<PlayerMovement>().enabled = false;
            _PlayerBehaviour.Inputs.FindAction("Escape").performed += Escape;
            _PlayerBehaviour.Inputs.FindAction("Inventary").performed += Inventary;
            _PlayerBehaviour.Inputs.FindAction("Interact").performed += Continue;
            base.Init();
        }
        private void Continue(InputAction.CallbackContext context)
        {
            if (_Text)
            {
                bool? Finish = onText?.Invoke();
                if (Finish != null)
                {
                    if ((bool)Finish)
                    {
                        _Text = false;
                        _StateMachine.ChangeState<SM_PlayerOnGround>();
                    }
                } else
                {
                    _Text = false;
                }
            }
        }
        private void LastState(IState state)
        {
            _LastState = state;
        }
        private void Escape(InputAction.CallbackContext context)
        {
            WhenEscape?.Invoke();
            if (!_Text) 
            {
                _Text = false;
                if (_LastState == null)
                    _StateMachine.ChangeState<SM_PlayerOnGround>();
                else
                {
                    if (_LastState.Equals(_StateMachine.GetState<SM_PlayerOnGround>()))
                        _StateMachine.ChangeState<SM_PlayerOnGround>();
                    else if (_LastState.Equals(_StateMachine.GetState<SM_PlayerOnAir>()))
                        _StateMachine.ChangeState<SM_PlayerOnAir>();
                    else if (_LastState.Equals(_StateMachine.GetState<SM_PlayerOnClimb>()))
                        _StateMachine.ChangeState<SM_PlayerOnClimb>();
                    else if (_LastState.Equals(_StateMachine.GetState<SM_PlayerOnCrunch>()))
                        _StateMachine.ChangeState<SM_PlayerOnCrunch>();
                }
            }
        }
        private void Inventary(InputAction.CallbackContext context)
        {
            if (Time.timeScale == 1 && !_PlayerBehaviour.BuyActivated)
                onInventory?.Invoke(true);
        }
        private void Conversation()
        {
            _Text = true;
        }
        /*private void EndConversation()
        {
            _Text = false;
            _StateMachine.ChangeState<SM_PlayerOnGround>();
        }*/
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "NPC")
            {
                collision.gameObject.GetComponent<NPCScript>().onNPCTalk += Conversation;
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == "NPC")
            {
                collision.gameObject.GetComponent<NPCScript>().onNPCTalk -= Conversation;
                if (_StateMachine.CurrentState.Equals(_StateMachine.GetState<SM_PlayerOnIddle>()))
                    _StateMachine.ChangeState<SM_PlayerOnGround>();
            }
        }
        public override void Exit()
        {
            _LastState = null;
            GetComponent<PlayerMovement>().enabled = true;
            if (_PlayerBehaviour != null)
            {
                _PlayerBehaviour.Inputs.FindAction("Inventary").performed -= Inventary;
                _PlayerBehaviour.Inputs.FindAction("Escape").performed -= Escape;
                _PlayerBehaviour.Inputs.FindAction("Jump").performed -= Continue;
            }
        }
        private void OnDestroy()
        {
            _PlayerBehaviour.OnIddle -= LastState; 
            if (_PlayerBehaviour != null)
            {
                _PlayerBehaviour.Inputs.FindAction("Inventary").performed -= Inventary;
                _PlayerBehaviour.Inputs.FindAction("Escape").performed -= Escape;
                _PlayerBehaviour.Inputs.FindAction("Jump").performed -= Continue;
            }
        }
    }
}

