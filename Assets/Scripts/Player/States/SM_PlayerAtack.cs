using FSMState;
using PlayerNS;
using ScriptableObjectsNS;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SM_PlayerAtack : MBState
{
    private GameObject _HitCollider;
    PlayerBehaviour _PlayerBehaviour;
    public delegate void AtackEvent();
    public event AtackEvent FinishAtack;
    private PlayerScriptableObject _SOPlayer;
    private bool _AtackDown;
    public bool AtackDown => _AtackDown;
    private void Awake()
    {
        _AtackDown = false;
        _PlayerBehaviour = GetComponent<PlayerBehaviour>();
        _SOPlayer = _PlayerBehaviour.PlayerScriptableObject;
        _HitCollider = transform.GetChild(0).gameObject;
    }
    private void OnDestroy()
    {
        _PlayerBehaviour.Inputs.FindAction("Movement").performed += Movement;
        _PlayerBehaviour.Inputs.FindAction("Movement").canceled += Movement;
    }
    public override void Init()
    {
        _PlayerBehaviour.Inputs.FindAction("Movement").performed += Movement;
        _PlayerBehaviour.Inputs.FindAction("Movement").canceled += Movement;
        _PlayerBehaviour.MovePlayer(_PlayerBehaviour.Inputs.FindAction("Movement").ReadValue<Vector2>());
        Vector2 Direction = GetComponent<PlayerBehaviour>().ShootDirection;
        if (Direction == Vector2.up)
        {
            GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Atack1);
            _HitCollider.transform.localPosition = new Vector2(0, _SOPlayer._DistanceTriggerBasicAtack);
        }
        else if (Direction == Vector2.down)
        {
            GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Atack3);
            _HitCollider.transform.localPosition = new Vector2(0, -_SOPlayer._DistanceTriggerBasicAtack);
            _AtackDown = true;
        }
        else
        {
            GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Atack2);
            if (Direction.y > 0)
                _HitCollider.transform.localPosition = new Vector2(_SOPlayer._DistanceTriggerBasicAtack / 2, _SOPlayer._DistanceTriggerBasicAtack / 2);
            else if (Direction.y < 0)
                _HitCollider.transform.localPosition = new Vector2(_SOPlayer._DistanceTriggerBasicAtack / 2, -_SOPlayer._DistanceTriggerBasicAtack / 2);
            else
                _HitCollider.transform.localPosition = new Vector2(_SOPlayer._DistanceTriggerBasicAtack, 0);
        }
        base.Init();
    }
    private void Movement(InputAction.CallbackContext context)
    {
        Vector2 Direction = context.ReadValue<Vector2>();
        _PlayerBehaviour.MovePlayer(Direction);
    }
    public override void Exit()
    {
        _AtackDown = false;
        if (_PlayerBehaviour != null)
        {
            _PlayerBehaviour.Inputs.FindAction("Movement").performed -= Movement;
            _PlayerBehaviour.Inputs.FindAction("Movement").canceled -= Movement;
        }
        FinishAtack?.Invoke();
        base.Exit();
    }
}
