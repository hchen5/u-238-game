using FSMState;
using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.HID;
using static UnityEngine.RuleTile.TilingRuleOutput;

namespace PlayerNS
{
    public class SM_PlayerOnClimb : MBState
    {
        PlayerBehaviour _PlayerBehaviour;
        NewFiniteStateMachine _StateMachine;
        Rigidbody2D _RigidBody2D;
        private PlayerScriptableObject _PlayerSO;
        float _NormalGravity;
        Coroutine _GravityCoroutine;

        private void Awake()
        {
            _PlayerBehaviour = GetComponent<PlayerBehaviour>();
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _RigidBody2D = GetComponent<Rigidbody2D>();
            _PlayerSO = _PlayerBehaviour.PlayerScriptableObject;
            _NormalGravity = _RigidBody2D.gravityScale;
        }
        public override void Init()
        {
            GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Climb);
            _GravityCoroutine = StartCoroutine(SetGravity());
            _PlayerBehaviour.Inputs.FindAction("Movement").performed += Movement;
            _PlayerBehaviour.Inputs.FindAction("Movement").canceled += Movement;
            _PlayerBehaviour.Inputs.FindAction("Jump").performed += Jump;
            base.Init();
        }
        private void Movement(InputAction.CallbackContext context)
        {
            Vector2 Direction = context.ReadValue<Vector2>();
            _PlayerBehaviour.MovePlayer(Direction);
        }
        private void Jump(InputAction.CallbackContext context)
        {
            RaycastHit2D rhitR = Physics2D.Raycast(transform.position, Vector2.right, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
            RaycastHit2D rhitL = Physics2D.Raycast(transform.position, -Vector2.right, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
            Vector2 Normal;
            float distanceR = Mathf.Infinity;
            float distanceL = Mathf.Infinity;
            if (rhitL.collider != null)
                distanceL = rhitL.distance;
            if (rhitR.collider != null)
                distanceR = rhitR.distance;
            if (distanceL < distanceR)
                Normal = rhitL.normal;
            else if (distanceR < distanceL)
                Normal = rhitR.normal;
            else
            {
                if (rhitL.collider == null && rhitR.collider == null)
                    _StateMachine.ChangeState<SM_PlayerOnGround>();
                else
                    Debug.LogError("Touching Two walls at the same time");
                return;
            }
            Vector2 JumpVector;
            if (_PlayerBehaviour.ForceDirection == -Normal)
            {
                if (Normal == Vector2.right)
                    JumpVector = Quaternion.AngleAxis(_PlayerSO._ClimbJumpDegreesToClimb, Vector3.forward) * Normal;
                else
                    JumpVector = Quaternion.AngleAxis(-_PlayerSO._ClimbJumpDegreesToClimb, Vector3.forward) * Normal;
            }
            else
            {
                if (Normal == Vector2.right)
                    JumpVector = Quaternion.AngleAxis(_PlayerSO._ClimbJumpDegreesToSeparate, Vector3.forward) * Normal;
                else
                    JumpVector = Quaternion.AngleAxis(-_PlayerSO._ClimbJumpDegreesToSeparate, Vector3.forward) * Normal;
            }
            JumpVector = new Vector2(JumpVector.x, Mathf.Abs(JumpVector.y));
            _RigidBody2D.AddForce(JumpVector * _PlayerSO._ClimbJumpForce, ForceMode2D.Impulse);
        }
        private IEnumerator SetGravity()
        {
            while (_RigidBody2D.velocity.y > 0)
            {
                yield return new WaitForSeconds(0.1f);
            }
            _RigidBody2D.gravityScale = _NormalGravity * _PlayerSO._GravityScaleReduction;
        }
        public override void Exit()
        {
            if (_GravityCoroutine != null)
                StopCoroutine(_GravityCoroutine);
            _PlayerBehaviour.Inputs.FindAction("Movement").performed -= Movement;
            _PlayerBehaviour.Inputs.FindAction("Movement").canceled -= Movement;
            _PlayerBehaviour.Inputs.FindAction("Jump").performed -= Jump;
            _RigidBody2D.gravityScale = _NormalGravity;
        }
        private void OnCollisionExit2D(Collision2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacles")) && _StateMachine.GetState<SM_PlayerOnClimb>().Equals(_StateMachine.CurrentState))
            {
                if (_GravityCoroutine != null)
                    StopCoroutine(_GravityCoroutine);
                _RigidBody2D.gravityScale = _NormalGravity;
                _StateMachine.ChangeState<SM_PlayerOnAir>();
            }
        }
        private void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacles")) && _StateMachine.GetState<SM_PlayerOnClimb>().Equals(_StateMachine.CurrentState))
            {
                CapsuleCollider2D NormalCollider = (CapsuleCollider2D)_PlayerBehaviour.NormalCollider;
                Vector2 Bottom = new Vector2(transform.position.x, transform.position.y - NormalCollider.size.y / 2 + NormalCollider.offset.y);
                RaycastHit2D rhitD = Physics2D.Raycast(Bottom, -Vector2.up, 0.1f, LayerMask.GetMask("Obstacles"));
                if (rhitD.collider != null)
                {
                    if (_GravityCoroutine != null)
                        StopCoroutine(_GravityCoroutine);
                    _RigidBody2D.gravityScale = _NormalGravity;
                    _StateMachine.ChangeState<SM_PlayerOnGround>();
                }
            }
        }
        private void OnDestroy()
        {
            _PlayerBehaviour.Inputs.FindAction("Movement").performed -= Movement;
            _PlayerBehaviour.Inputs.FindAction("Movement").canceled -= Movement;
            _PlayerBehaviour.Inputs.FindAction("Jump").performed -= Jump;
        }
        public void SetNormalGravity()
        {
            _RigidBody2D.gravityScale = _NormalGravity;
        }
    }
}
