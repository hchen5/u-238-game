using System;
using System.Collections;
using System.Collections.Generic;
using FSMState;
using ScriptableObjectsNS;
using SkillsNS;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using static SkillsNS.PlayerSkills;

namespace PlayerNS
{
    public class SM_PlayerOnAir : MBState
    {
        PlayerBehaviour _PlayerBehaviour;
        NewFiniteStateMachine _StateMachine;
        Rigidbody2D _RigidBody2D;
        private PlayerScriptableObject _PlayerSO;
        bool _isDobleJumpUsed;
        private bool _isAfterAtacking;
        private void Awake()
        {
            _isAfterAtacking = false;
            _PlayerBehaviour = GetComponent<PlayerBehaviour>();
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _RigidBody2D = GetComponent<Rigidbody2D>();
            _PlayerSO = _PlayerBehaviour.PlayerScriptableObject;
        }
        public override void Init()
        {
            GetComponent<SM_PlayerAtack>().FinishAtack += FinishAtack;
            GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Air);
            GetComponent<SM_PlayerOnClimb>().SetNormalGravity();
            if (!_isAfterAtacking)
                _isDobleJumpUsed = false;
            else
                _isAfterAtacking = false;
            _PlayerBehaviour.Inputs.FindAction("Movement").performed += Movement;
            _PlayerBehaviour.Inputs.FindAction("Movement").canceled += Movement;
            _PlayerBehaviour.Inputs.FindAction("Jump").performed += Jump;
            _PlayerBehaviour.Inputs.FindAction("Skills").performed += Skill;
            _PlayerBehaviour.Inputs.FindAction("Atack").performed += Atack;
            _PlayerBehaviour.MovePlayer(_PlayerBehaviour.Inputs.FindAction("Movement").ReadValue<Vector2>());
            base.Init();
        }
        private void Skill(InputAction.CallbackContext context)
        {
            int AtackSlot = context.action.GetBindingIndexForControl(context.control);
            if (AtackSlot > 3)
                AtackSlot -= 4;
            if (_PlayerBehaviour.MySkills.Count > AtackSlot)
                GetComponent<PlayerSkills>().DoSkill(_PlayerBehaviour.MySkills[AtackSlot]);
        }
        private void Atack(InputAction.CallbackContext context)
        {
            _PlayerBehaviour.Atacking();
            _isAfterAtacking = true;
            _StateMachine.ChangeState<SM_PlayerAtack>();
            //transform.GetChild(0).GetComponent<BasicAtack>().Atack();
        }
        private void FinishAtack()
        {
            if (!_StateMachine.CurrentState.Equals(_StateMachine.GetState<SM_PlayerOnAir>()) && !_StateMachine.CurrentState.Equals(_StateMachine.GetState<SM_PlayerAtack>()))
                _isAfterAtacking = false;
        }
        public override void Exit()
        {
            if (_PlayerBehaviour != null)
            {
                _PlayerBehaviour.Inputs.FindAction("Movement").performed -= Movement;
                _PlayerBehaviour.Inputs.FindAction("Movement").canceled -= Movement;
                _PlayerBehaviour.Inputs.FindAction("Jump").performed -= Jump;
                _PlayerBehaviour.Inputs.FindAction("Skills").performed -= Skill;
                _PlayerBehaviour.Inputs.FindAction("Atack").performed -= Atack;
            }
            //GetComponent<SM_PlayerAtack>().FinishAtack -= FinishAtack; //
            base.Exit();
        }
        private void OnDestroy()
        {
            if (_PlayerBehaviour != null)
            {
                _PlayerBehaviour.Inputs.FindAction("Movement").performed -= Movement;
                _PlayerBehaviour.Inputs.FindAction("Movement").canceled -= Movement;
                _PlayerBehaviour.Inputs.FindAction("Jump").performed -= Jump;
                _PlayerBehaviour.Inputs.FindAction("Skills").performed -= Skill;
                _PlayerBehaviour.Inputs.FindAction("Atack").performed -= Atack;
            }
            //GetComponent<SM_PlayerAtack>().FinishAtack -= FinishAtack; //
        }
        private void Movement(InputAction.CallbackContext context)
        {
            Vector2 Direction = context.ReadValue<Vector2>();
            _PlayerBehaviour.MovePlayer(Direction);
        }
        private void Jump(InputAction.CallbackContext context)
        {
            if (!_isDobleJumpUsed && _PlayerBehaviour.DoubleJump) 
            {
                _isDobleJumpUsed = true;
                _RigidBody2D.velocity = new Vector2(_RigidBody2D.velocity.x, 0);
                _RigidBody2D.AddForce(Vector2.up * _PlayerSO._DobleJumpForce, ForceMode2D.Impulse);
            }
        }
        private void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacles")) && _StateMachine.GetState<SM_PlayerOnAir>().Equals(_StateMachine.CurrentState))
            {
                CapsuleCollider2D NormalCollider = (CapsuleCollider2D) _PlayerBehaviour.NormalCollider;
                //Vector2 BottomL = new Vector2(transform.position.x - NormalCollider.size.x / 2, transform.position.y - NormalCollider.size.y / 2);
                //Vector2 BottomR = new Vector2(transform.position.x + NormalCollider.size.x / 2, transform.position.y - NormalCollider.size.y / 2);
                Vector2 Bottom = new Vector2(transform.position.x, transform.position.y - NormalCollider.size.y / 2 + NormalCollider.offset.y);
                Vector2 TOP = new Vector2(transform.position.x, transform.position.y + NormalCollider.size.y / 2 + NormalCollider.offset.y);
                Vector2 PosL = new Vector2(transform.position.x - NormalCollider.size.x / 2, transform.position.y);
                Vector2 PosR = new Vector2(transform.position.x + NormalCollider.size.x / 2, transform.position.y);
                RaycastHit2D rhitR = Physics2D.Raycast(PosR, Vector2.right, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
                RaycastHit2D rhitL = Physics2D.Raycast(PosL, -Vector2.right, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
                RaycastHit2D rhitU = Physics2D.Raycast(TOP, Vector2.up, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
                //RaycastHit2D rhitDL = Physics2D.Raycast(BottomL, -Vector2.up, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
                //RaycastHit2D rhitDR = Physics2D.Raycast(BottomR, -Vector2.up, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
                RaycastHit2D rhitD = Physics2D.Raycast(Bottom, -Vector2.up, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
                if (rhitD.collider != null || (rhitL.collider != null || rhitR.collider != null))
                {
                    float distance = Mathf.Infinity;
                    if (rhitR.collider != null && rhitL.collider != null)
                    {
                        if (rhitL.distance < rhitR.distance)
                            distance = rhitL.distance;
                        else
                            distance = rhitR.distance;
                    } else
                    {
                        if (rhitL.collider != null)
                            distance = rhitL.distance;
                        else
                            distance = rhitR.distance;
                    }
                    /*float DownDistance;
                    if (rhitDL.distance < rhitDR.distance && rhitDL.distance != 0)
                        DownDistance = rhitDL.distance;
                    else if (rhitDL.distance > rhitDR.distance && rhitDR.distance != 0)
                        DownDistance = rhitDR.distance;
                    else if (rhitDL.distance != 0)
                        DownDistance = rhitDL.distance;
                    else
                        DownDistance = rhitDR.distance;
                    Debug.Log(rhitDR.distance + " " + rhitDL.distance + " Final: " + DownDistance);*/
                    if (collision.gameObject.GetComponent<Effector2D>() != null)
                    {
                        RaycastHit2D rhitREffector = Physics2D.Raycast(PosR, -Vector2.right, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
                        RaycastHit2D rhitLEffector = Physics2D.Raycast(PosL, Vector2.right, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
                        RaycastHit2D rhitDEffector = Physics2D.Raycast(Bottom, Vector2.up, Mathf.Infinity, LayerMask.GetMask("Obstacles"));
                        if (rhitD.distance < 1f && (rhitDEffector.distance > 1f || rhitDEffector.distance == 0) && _RigidBody2D.velocity.y < 0.1f)
                            _StateMachine.ChangeState<SM_PlayerOnGround>();
                        else if (rhitR.distance > 1f && rhitL.distance < 1f && rhitLEffector.distance > 1 || rhitR.distance < 1f && rhitL.distance > 1f && rhitREffector.distance > 1 /*|| rhitR.distance == 0 && rhitL.distance == 0*/)
                            if (_PlayerBehaviour.ClimbUnloked)
                                _StateMachine.ChangeState<SM_PlayerOnClimb>();
                        return;
                    }
                    if (rhitU.collider != null)
                        if (rhitU.distance < 1f && rhitD.distance > 1f)
                            return;
                    if (rhitD.collider == null)
                        if (_PlayerBehaviour.ClimbUnloked)
                        {
                            _StateMachine.ChangeState<SM_PlayerOnClimb>();
                            return;
                        }
                    if (distance < rhitD.distance)
                    {
                        if (_PlayerBehaviour.ClimbUnloked)
                            _StateMachine.ChangeState<SM_PlayerOnClimb>();
                    }
                    else if (rhitD.distance < 1f)
                        _StateMachine.ChangeState<SM_PlayerOnGround>();
                }
            }
        }
    }
}

