using FSMState;
using ScriptableObjectsNS;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerNS
{
    public class SM_PlayerOnCrunch : MBState
    {
        PlayerBehaviour _PlayerBehaviour;
        NewFiniteStateMachine _StateMachine;
        Rigidbody2D _RigidBody2D;
        private PlayerScriptableObject _PlayerSO;
        private void Awake()
        {
            _PlayerBehaviour = GetComponent<PlayerBehaviour>();
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _RigidBody2D = GetComponent<Rigidbody2D>();
            _PlayerSO = _PlayerBehaviour.PlayerScriptableObject;
        }
        public override void Init()
        {
            _PlayerBehaviour.NormalCollider.enabled = false;
            _PlayerBehaviour.CrounchCollider.enabled = true;
            _PlayerBehaviour.Inputs.FindAction("Movement").performed += Movement;
            _PlayerBehaviour.Inputs.FindAction("Movement").canceled += Movement;
            _PlayerBehaviour.Inputs.FindAction("Crunch").canceled += Crunch;
            _PlayerBehaviour.Inputs.FindAction("Crunch").performed += Crunch;
            _PlayerBehaviour.Inputs.FindAction("Atack").performed += Atack;
            _PlayerBehaviour.MovePlayer(_PlayerBehaviour.Inputs.FindAction("Movement").ReadValue<Vector2>());
            if (_PlayerBehaviour.Inputs.FindAction("Crunch").ReadValue<float>() == 0)
            {
                if (CheckIfUnderPlatform())
                    StartCoroutine(CheckIfUnderPlatformAndStand());
                else
                {
                    _PlayerBehaviour.NormalCollider.enabled = true;
                    _PlayerBehaviour.CrounchCollider.enabled = false;
                    _StateMachine.ChangeState<SM_PlayerOnGround>();
                }
            }
            else
                StopAllCoroutines();
            base.Init();
        }
        private void Crunch(InputAction.CallbackContext context)
        {
            if (context.canceled) 
            {
                if (CheckIfUnderPlatform())
                    StartCoroutine(CheckIfUnderPlatformAndStand());
                else
                {
                    _PlayerBehaviour.NormalCollider.enabled = true;
                    _PlayerBehaviour.CrounchCollider.enabled = false;
                    _StateMachine.ChangeState<SM_PlayerOnGround>();
                }
            } else
                StopAllCoroutines();
        }
        private void Movement(InputAction.CallbackContext context)
        {
            Vector2 Direction = context.ReadValue<Vector2>();
            _PlayerBehaviour.MovePlayer(Direction);
        }
        private void Atack(InputAction.CallbackContext context)
        {
            _PlayerBehaviour.Atacking();
            _StateMachine.ChangeState<SM_PlayerAtack>();
            //transform.GetChild(0).GetComponent<BasicAtack>().Atack();
        }
        private bool CheckIfUnderPlatform()
        {
            bool hitRAndL = false;
            Vector2 OriginL = _PlayerBehaviour.NormalCollider.transform.position - new Vector3((_PlayerBehaviour.NormalCollider.transform.localScale.x / 2), 0, 0);
            Vector2 OriginR = _PlayerBehaviour.NormalCollider.transform.position + new Vector3((_PlayerBehaviour.NormalCollider.transform.localScale.x / 2), 0, 0);
            RaycastHit2D hitl = Physics2D.Raycast(OriginL, Vector2.up, _PlayerBehaviour.NormalCollider.transform.localScale.y + 0.1f, LayerMask.GetMask("Obstacles"));
            RaycastHit2D hitr = Physics2D.Raycast(OriginR, Vector2.up, _PlayerBehaviour.NormalCollider.transform.localScale.y + 0.1f, LayerMask.GetMask("Obstacles"));
            if (!hitl & !hitr)
                hitRAndL = false;
            else
                hitRAndL = true;
            return hitRAndL;
        }
        private IEnumerator CheckIfUnderPlatformAndStand()
        {
            while (CheckIfUnderPlatform())
            {
                yield return new WaitForSeconds(0.5f);
            }
            _PlayerBehaviour.NormalCollider.enabled = true;
            _PlayerBehaviour.CrounchCollider.enabled = false;
            _StateMachine.ChangeState<SM_PlayerOnGround>();
        }
        public override void Exit()
        {
            if (_PlayerBehaviour != null)
            {
                _PlayerBehaviour.Inputs.FindAction("Movement").performed -= Movement;
                _PlayerBehaviour.Inputs.FindAction("Movement").canceled -= Movement;
                _PlayerBehaviour.Inputs.FindAction("Crunch").canceled -= Crunch;
                _PlayerBehaviour.Inputs.FindAction("Crunch").performed -= Crunch;
                _PlayerBehaviour.Inputs.FindAction("Atack").performed -= Atack;
            }
            base.Exit();
        }
        private void OnDestroy()
        {
            if (_PlayerBehaviour != null)
            {
                _PlayerBehaviour.Inputs.FindAction("Movement").performed -= Movement;
                _PlayerBehaviour.Inputs.FindAction("Movement").canceled -= Movement;
                _PlayerBehaviour.Inputs.FindAction("Crunch").canceled -= Crunch;
                _PlayerBehaviour.Inputs.FindAction("Crunch").performed -= Crunch;
                _PlayerBehaviour.Inputs.FindAction("Atack").performed -= Atack;
            }
        }
        private void OnCollisionExit2D(Collision2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Obstacles")) && _StateMachine.GetState<SM_PlayerOnCrunch>().Equals(_StateMachine.CurrentState))
            {
                CapsuleCollider2D NormalCollider = (CapsuleCollider2D)_PlayerBehaviour.NormalCollider;
                Vector2 Bottom = new Vector2(transform.position.x, transform.position.y - NormalCollider.size.y / 2 + NormalCollider.offset.y);
                RaycastHit2D rhitD = Physics2D.Raycast(Bottom, -Vector2.up, 0.1f, LayerMask.GetMask("Obstacles"));
                if (rhitD.collider == null)
                {
                    _StateMachine.ChangeState<SM_PlayerOnAir>();
                }
            }
        }
    }
}
