using FSMState;
using PlayerNS;
using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D _Rigidbody;
    private PlayerScriptableObject _PlayerSO;
    PlayerBehaviour _PlayerBehaviour;
    NewFiniteStateMachine _StateMachine;
    float NormalGravity;
    private void Awake()
    {
        _StateMachine = GetComponent<NewFiniteStateMachine>();
        _Rigidbody = GetComponent<Rigidbody2D>();
        _PlayerBehaviour = GetComponent<PlayerBehaviour>();
        _PlayerSO = _PlayerBehaviour.PlayerScriptableObject;
    }
    private void Start()
    {
        NormalGravity = _Rigidbody.gravityScale;
    }
    private void FixedUpdate()
    {
        float SpeedForceLimiter = _PlayerSO._SpeedForceLimiter;
        float ForceLimiter = _PlayerSO._MovementForceLimiter;
        Vector2 ForceDirection = GetComponent<PlayerBehaviour>().ForceDirection;
        if (_Rigidbody.totalForce.x < SpeedForceLimiter)
            _Rigidbody.AddForce(ForceDirection * _PlayerBehaviour.SpeedF);
        
        float ForceX = _Rigidbody.totalForce.x;
        float ForceY = _Rigidbody.totalForce.y;
        if (_Rigidbody.totalForce.x > ForceLimiter)
            ForceX = ForceLimiter;
        if (_Rigidbody.totalForce.y > ForceLimiter)
            ForceY = ForceLimiter;
        if (_Rigidbody.totalForce.x < -ForceLimiter)
            ForceX = -ForceLimiter;
        if (_Rigidbody.totalForce.y < -ForceLimiter)
            ForceY = -ForceLimiter;

        _Rigidbody.totalForce = new Vector2(ForceX, ForceY);
        if (!_StateMachine.GetState<SM_PlayerOnClimb>().Equals(_StateMachine.CurrentState))
        {
            if (_Rigidbody.velocity.y < 0)
                _Rigidbody.gravityScale = NormalGravity * _PlayerSO._GravityScaleWhenFalling;
            else
                _Rigidbody.gravityScale = NormalGravity;
        }

        if (_Rigidbody.velocity.y > _PlayerSO._MovementSpeedLimiter)
            _Rigidbody.velocity = new Vector2(_Rigidbody.velocity.x, _PlayerSO._MovementSpeedLimiter);
        else if (_Rigidbody.velocity.y < -_PlayerSO._MovementSpeedLimiter)
            _Rigidbody.velocity = new Vector2(_Rigidbody.velocity.x,-_PlayerSO._MovementSpeedLimiter);

    }
}
