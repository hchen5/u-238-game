using ScriptableObjectsNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerNS
{
    public class PlayerAnimations : MonoBehaviour
    {
        [SerializeField]
        private CharacterAnimationsSO _SOAnimations;
        public enum _CharacterAnimations { Iddle, Air, Run, Climb, CrounchIddle, Crounch, Atack1, Atack2, Atack3 }
        private Animator _Animator;
        private void Awake()
        {
            _Animator = GetComponent<Animator>();
        }
        internal void SetAnimation(_CharacterAnimations anim)
        {
            switch (anim)
            {
                case _CharacterAnimations.Iddle:
                    _Animator.Play(_SOAnimations._Iddle.name);
                    break;
                case _CharacterAnimations.Air:
                    _Animator.Play(_SOAnimations._Air.name);
                    break;
                case _CharacterAnimations.Run:
                    _Animator.Play(_SOAnimations._Walk.name);
                    break;
                case _CharacterAnimations.Climb:
                    _Animator.Play(_SOAnimations._Climb.name);
                    break;
                case _CharacterAnimations.CrounchIddle:
                    _Animator.Play(_SOAnimations._CrounchIddle.name);
                    break;
                case _CharacterAnimations.Crounch:
                    _Animator.Play(_SOAnimations._Crounch.name);
                    break;
                case _CharacterAnimations.Atack1:
                    _Animator.Play(_SOAnimations._AtackUp.name);
                    break;
                case _CharacterAnimations.Atack2:
                    _Animator.Play(_SOAnimations._AtackForward.name);
                    break;
                case _CharacterAnimations.Atack3:
                    _Animator.Play(_SOAnimations._AtackDown.name);
                    break;
            }
        }
    }
}

