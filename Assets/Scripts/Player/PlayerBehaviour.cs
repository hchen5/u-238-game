using EnemiesNS;
using FSMState;
using ItemsNS;
using SaveDataNS;
using ScriptableObjectsNS;
using SkillsNS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.SceneManagement;
using static SkillsNS.PlayerSkills;
using IState = FSMState.IState;

namespace PlayerNS
{
    [RequireComponent(typeof(NewFiniteStateMachine))]
    [RequireComponent(typeof(CapsuleCollider2D))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(SM_PlayerOnAir))]
    [RequireComponent(typeof(SM_PlayerOnClimb))]
    [RequireComponent(typeof(SM_PlayerOnGround))]
    [RequireComponent(typeof(SM_PlayerOnCrunch))]
    [RequireComponent(typeof(SM_PlayerOnIddle))]
    [RequireComponent(typeof(PlayerMovement))]
    [RequireComponent(typeof(PlayerSkills))]
    [RequireComponent(typeof(Dash))]
    [RequireComponent(typeof(FireBall))]
    [RequireComponent(typeof(Boomerang))]
    [RequireComponent(typeof(Parry))]
    public class PlayerBehaviour : MonoBehaviour, ISaveablePlayer
    {
        private NewFiniteStateMachine _StateMachine;
        [Header("Inputs")]
        [SerializeField]
        private InputActionAsset _InputSys;
        private InputActionAsset _Inputs;
        [Header("ScriptableObjects")]
        [SerializeField]
        private PlayerScriptableObject _PlayerScriptableObject;
        Vector2 _Move;
        private bool _DoubleJump = false;
        private bool _ClimbUnlocked = false;
        private int _MaxHealth;
        private int _Health;
        //private bool _Lantern = false;
        private Dictionary<ItemsScriptableObjects, int> _ItemsInventory = new Dictionary<ItemsScriptableObjects, int>();
        private int _PlayerMoney;
        [Header("Colliders")]
        [SerializeField]
        private Collider2D _NormalCollider;
        [SerializeField]
        private Collider2D _CrounchCollider;
        [Header("GameEvents")]
        [SerializeField]
        private GameEventInt _GameEventMoney;
        [SerializeField]
        protected GameEventSimple _GameEventResetEnemy;
        [SerializeField]
        private GameEventInt _GameEventCooldownPerk;
        [SerializeField]
        private GameEventSimple _Revive;
        private Vector2 _ForceDirection;
        public Vector2 ForceDirection => _ForceDirection;
        private Vector2 _LookDirection;
        public Vector2 LookDirection => _LookDirection;
        private Vector2 _ShootDirection;
        public Vector2 ShootDirection => _ShootDirection;
        [Header("Skills")]
        [SerializeField]
        private List<EnumSkills> _MySkills = new List<EnumSkills>();
        [SerializeField]
        private List<EnumSkills> _UnlockedSkills = new List<EnumSkills>();
        private bool _StopWhenInteractable;
        public ReadOnlyCollection<EnumSkills> MySkills => _MySkills.AsReadOnly();
        private GameObject _SkillUnlockedObject = null;
        public delegate void DoorInteractable();
        public event DoorInteractable OnDoorInteractable;
        public delegate void InteractableDelegate();
        public event InteractableDelegate OnInteractable;
        public delegate void PlayerReceivedDMG();
        public event PlayerReceivedDMG OnPlayerReceivedDMG;
        public delegate void PlayerPerk();
        public event PlayerPerk OnPlayerPerk;
        public delegate void PlayerPerkDisable();
        public event PlayerPerkDisable OnPlayerPerkDisable;
        public delegate void Playerload();
        public event Playerload OnPlayerload;
        public delegate void PlayerMoneyPerk(bool a, int b);
        public event PlayerMoneyPerk OnPlayerMoneyPerk;
        public delegate bool InputPressed();
        public InputPressed WhenEscape;
        public delegate void ChangeState(IState State);
        public ChangeState OnIddle;
        private bool _isParry, _isInmune;
        private GameObject _PlayerHitCollider;
        private Vector2 _DefaultPlayerHitColliderLocalPosition;
        private Vector2 _DefaultPlayerHitColliderLocalScale;
        private int _DMG = 0;
        private float _SpeedF = 0f;
        private int _PerkPercent;
        List<TalismaScriptableObject> _EquipedTalismans = new List<TalismaScriptableObject>();
        [SerializeField]
        List<TalismaScriptableObject> _UnlockedTalismans = new List<TalismaScriptableObject>();
        [SerializeField]
        List<TalismaScriptableObject> _AllTalismans = new List<TalismaScriptableObject>();
        private Dictionary<string, Vector3> _UnlockedPilars = new Dictionary<string, Vector3>();
        [Header("Panels")]
        [SerializeField]
        private HudCanvas _HUDCanvas;
        //public bool BuyActivated => _HUDCanvas.BuyPanelActivated;
        public bool BuyActivated => false; //Delete
        [Header("DataBase SO")]
        [SerializeField]
        private DataBaseSO _DataBaseSO;
        [SerializeField]
        private GameObject _LanterGO;
        public int Health { get => _Health; }
        public Dictionary<ItemsScriptableObjects, int> ItemsInventory { get => _ItemsInventory; }
        public Vector2 Move { get => _Move; }
        public InputActionAsset Inputs { get => _Inputs; }
        public bool DoubleJump { get => _DoubleJump; }
        public bool ClimbUnloked { get => _ClimbUnlocked; }
        public PlayerScriptableObject PlayerScriptableObject { get => _PlayerScriptableObject; }
        public int PlayerMoney { get => _PlayerMoney; }
        public Collider2D NormalCollider { get => _NormalCollider; }
        public Collider2D CrounchCollider { get => _CrounchCollider; }
        public GameObject PlayerHitCollider { get => _PlayerHitCollider; }
        public int DMG { get => _DMG; }
        public float SpeedF { get => _SpeedF; }
        public List<TalismaScriptableObject> EquippedTalismans { get => _EquipedTalismans; }
        public int MaxHealth { get => _MaxHealth;}
        public Vector2 DefaultPlayerHitColliderLocalScale { get => _DefaultPlayerHitColliderLocalScale;}
        public List<TalismaScriptableObject> UnlockedTalismans { get => _UnlockedTalismans;}
        public List<EnumSkills> UnlockedSkills { get => _UnlockedSkills; }
        public Dictionary<string, Vector3> UnlockedPilars { get => _UnlockedPilars;}
        [SerializeField]
        GameObject ShieldSprite;
        [SerializeField]
        AudioSource _dmg;
        [SerializeField]
        AudioSource _Slash;
        [SerializeField]
        AudioSource _Walk;
        public void StopAudio()
        {
            _Walk.Stop();
            _dmg.Stop();
            _Slash.Stop();
        }
        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Inputs = Instantiate(_InputSys);
            _Inputs.FindActionMap("Player").Enable();
            _PlayerHitCollider = transform.GetChild(0).gameObject;
        }
        public void ListenGameEventEquippedTalismans(List<TalismaScriptableObject> ltso)
        {
            _EquipedTalismans = ltso;
            TalismansPerk();
            TalismansPerkDisable();
        }
        public void ListenGameEventEquippedSkills(List<SkillsSO> ltso)
        {
            _MySkills.Clear();
            foreach(SkillsSO skillsSO in ltso)
            {
                if (skillsSO != null)
                    _MySkills.Add(skillsSO._SkillEnum);
                else
                    _MySkills.Add(EnumSkills.None);
            }
            DrawSkillsHud();
        }
        private void Start()
        {
            Physics2D.IgnoreLayerCollision(gameObject.layer, LayerMask.NameToLayer("Enemy"), false);
            _Inputs.FindAction("Escape").performed += Escape;
            StartCoroutine(DrawSkillsHudCoroutine());
            if (SaveGameManager.Instance.Loaded)
                return;
            StartSettings();
        }
        private IEnumerator DrawSkillsHudCoroutine()
        {
            yield return new WaitForEndOfFrame();
            DrawSkillsHud();
        }
        private void DrawSkillsHud()
        {
            int iteration = 0;
            foreach (EnumSkills sk in _MySkills)
            {
                switch (sk)
                {
                    case EnumSkills.Dash:
                        GetComponent<Dash>().DrawSkill();
                        GetComponent<Dash>().ResetCoolDown();
                        break;
                    case EnumSkills.FireBall:
                        GetComponent<FireBall>().DrawSkill();
                        GetComponent<FireBall>().ResetCoolDown();
                        break;
                    case EnumSkills.Parry:
                        GetComponent<Parry>().DrawSkill();
                        GetComponent<Parry>().ResetCoolDown();
                        break;
                    case EnumSkills.Boomerang:
                        GetComponent<Boomerang>().DrawSkill();
                        GetComponent<Boomerang>().ResetCoolDown();
                        break;
                    default:
                        GetComponent<Dash>().DrawSkill(iteration, null);
                        break;
                }
                iteration++;
            }
        }
        private void TalismansPerk()
        {
            for (int i = 0; i < EquippedTalismans.Count; i++)
            {
                if (EquippedTalismans[i] != null)
                {
                    switch (EquippedTalismans[i]._PerkType)
                    {
                        case PerkType.Health:
                            _Health = (int)EquippedTalismans[i].AddPerkToPlayer(this)._PerkValue;
                            _MaxHealth = (int)EquippedTalismans[i].AddPerkToPlayer(this)._PerkValue;
                            break;
                        case PerkType.Speed:
                            _SpeedF = EquippedTalismans[i].AddPerkToPlayer(this)._PerkValue;
                            break;
                        case PerkType.Damage:
                            transform.GetChild(0).GetComponent<DamageToEnemy>().Damage = (int)EquippedTalismans[i].AddPerkToPlayer(this)._PerkValue;
                            break;
                        case PerkType.Range:
                            float rangex = EquippedTalismans[i].AddPerkToPlayer(this)._PerkValue;
                            float offsetx = (rangex * 0.5f) + 0.5f;
                            _PlayerHitCollider.transform.localPosition = new Vector3(offsetx, _PlayerHitCollider.transform.localPosition.y, 0);
                            _PlayerHitCollider.transform.localScale = new Vector2(rangex, _PlayerHitCollider.transform.localScale.y);
                            break;
                        case PerkType.Bleeding:
                            _PlayerHitCollider.GetComponent<DamageToEnemy>().BleedSetting(true, _EquipedTalismans[i].AddPerkToPlayer(this));
                            break;
                        case PerkType.Cooldown:
                            _PerkPercent = (int)_EquipedTalismans[i].AddPerkToPlayer(this)._PerkValue;
                            _GameEventCooldownPerk?.Raise(_PerkPercent);
                            break;
                        case PerkType.Money:
                            _PerkPercent = (int)_EquipedTalismans[i].AddPerkToPlayer(this)._PerkValue;
                            _PlayerHitCollider.GetComponent<DamageToEnemy>().MoneyPerkSetting(true, _PerkPercent);
                            OnPlayerMoneyPerk?.Invoke(true, _PerkPercent);
                            break;
                    }
                }
            }
            OnPlayerPerk?.Invoke();
        }
        private void TalismansPerkDisable()
        {
            List<TalismaScriptableObject> deseq = _AllTalismans.Except(_EquipedTalismans).ToList();
            for (int i = 0; i < deseq.Count; i++)
            {
                switch (deseq[i]._PerkType)
                {
                    case PerkType.Health:
                        _Health = _PlayerScriptableObject._Health;
                        _MaxHealth = _PlayerScriptableObject._Health;
                        OnPlayerPerkDisable?.Invoke();
                        break;
                    case PerkType.Speed:
                        _SpeedF = _PlayerScriptableObject._SpeedForce;
                        break;
                    case PerkType.Damage:
                        transform.GetChild(0).GetComponent<DamageToEnemy>().Damage = _DMG;
                        break;
                    case PerkType.Bleeding:
                        _PlayerHitCollider.GetComponent<DamageToEnemy>().BleedSetting(false, _AllTalismans[i].AddPerkToPlayer(this));
                        break;
                    case PerkType.Cooldown:
                        _PerkPercent = 0;
                        _GameEventCooldownPerk?.Raise(0);
                        break;
                    case PerkType.Range:
                        _PlayerHitCollider.transform.localPosition = _DefaultPlayerHitColliderLocalPosition;
                        _PlayerHitCollider.transform.localScale = _DefaultPlayerHitColliderLocalScale;
                        break;
                    case PerkType.Money:
                        _PerkPercent = 0;
                        _PlayerHitCollider.GetComponent<DamageToEnemy>().MoneyPerkSetting(false, _PerkPercent);
                        OnPlayerMoneyPerk?.Invoke(false, _PerkPercent);
                        break;
                }
            }
        }
        private void StartSettings() 
        {
            _ClimbUnlocked = _PlayerScriptableObject._ClimbUnlocked;
            _DoubleJump = _PlayerScriptableObject._DoubleJumpUnlocked;
            _DefaultPlayerHitColliderLocalPosition = _PlayerHitCollider.transform.localPosition;
            _DefaultPlayerHitColliderLocalScale = _PlayerHitCollider.transform.localScale;
            _MaxHealth = _PlayerScriptableObject._Health;
            _PlayerMoney = 0;
            _DMG = _PlayerHitCollider.GetComponent<DamageToEnemy>().Damage;
            _SpeedF = _PlayerScriptableObject._SpeedForce;
            _GameEventMoney?.Raise(_PlayerMoney);
            _StateMachine.ChangeState<SM_PlayerOnGround>();
            _Health = _PlayerScriptableObject._Health;
            _ItemsInventory = new Dictionary<ItemsScriptableObjects, int>();
            GetComponent<Parry>().SetParry += SetParry;
        }
        internal bool RaiseInteract(bool save = false)
        {
            if (_TouchingPilarName != null)
            {
                if (!_UnlockedPilars.ContainsKey(_TouchingPilarName))
                {
                    _UnlockedPilars.Add(_TouchingPilarName, _TouchingPilarPosition);
                }
            }
            if (_SkillUnlockedObject != null) 
            {
                EnumSkills sk = _SkillUnlockedObject.GetComponent<UnlockSkill>().Skills;
                bool Jump = _SkillUnlockedObject.GetComponent<UnlockSkill>().UnlockDobleJump;
                bool Climb = _SkillUnlockedObject.GetComponent<UnlockSkill>().UnlokedClimb;
                if (sk != EnumSkills.None)
                {
                    if (!_UnlockedSkills.Contains(sk))
                    {
                        _UnlockedSkills.Add(sk);
                        _HUDCanvas.SetFeedBackPanel(sk.ToString() + " Desbloqueada", 3f);
                    }
                }
                else if (Climb && !_ClimbUnlocked)
                {
                    _ClimbUnlocked = true;
                    _HUDCanvas.SetFeedBackPanel("Escalada Desbloqueada", 3f);
                }
                else if (Jump && !_DoubleJump)
                {
                    _DoubleJump = true;
                    _HUDCanvas.SetFeedBackPanel("Doble Salto Desbloqueado", 3f);
                }
                _SkillUnlockedObject.SetActive(false);
                _SkillUnlockedObject = null;
            }
            OnInteractable?.Invoke();
            OnDoorInteractable?.Invoke();
            if (save)
                CureHealth();
            return _StopWhenInteractable;
        }
        private void CureHealth()
        {
            _Health = _MaxHealth;
            OnPlayerReceivedDMG?.Invoke();
        }
        internal void MovePlayer(Vector2 v)
        {
            if (v.x < 0)
                _LookDirection = -Vector2.right;
            else if (v.x > 0)
                _LookDirection = Vector2.right;
            _ForceDirection = new Vector2(v.x, 0).normalized;
            if (v != Vector2.zero)
                _ShootDirection = v;
            else
                _ShootDirection = _LookDirection;
            if (!_StateMachine.CurrentState.Equals(_StateMachine.GetState<SM_PlayerAtack>()))
            {
                //Debug.Log(transform.name);
                if (_LookDirection == Vector2.right)
                    transform.localEulerAngles = new Vector2(0, 0);
                else
                    transform.localEulerAngles = new Vector2(0, 180);
            }

            if (_StateMachine.CurrentState.Equals(_StateMachine.GetState<SM_PlayerOnGround>()))
            {
                if (v.x == 0)
                    GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Iddle);
                else
                    GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Run);
            }
            if (_StateMachine.CurrentState.Equals(_StateMachine.GetState<SM_PlayerOnCrunch>()))
            {
                if (v.x == 0)
                    GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.CrounchIddle);
                else
                    GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Crounch);
            }
            GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("Effects");
            if ((v == Vector2.zero || v == Vector2.down) || (!_StateMachine.CurrentState.Equals(_StateMachine.GetState<SM_PlayerOnCrunch>())) && !_StateMachine.CurrentState.Equals(_StateMachine.GetState<SM_PlayerOnGround>()))
                _Walk.Stop();
            else
                _Walk.Play();
        }
        private void SetParry(bool Active)
        {
            _isParry = Active;
            ShieldSprite.SetActive(Active);
        }
        /// <summary>
        /// Resta Health al Player
        /// </summary>
        /// <param name="Damage">El valor que le quieres quitar de Health</param>
        private void RecibeDamage(int Damage) 
        {
            if (!_isParry && !_isInmune) 
            {
                DamgeParticle();
                if (Health - Damage > 0)
                    _Health -= Damage;
                else
                {
                    _Health = 0;
                    GameManager.Instance.ChangeDieScene();
                    //SaveGameManager.Instance.LoadData(SaveGameManager.Instance.saveFileNamePublic);
                }
                OnPlayerReceivedDMG?.Invoke();
            }
        }
        private void DamgeParticle()
        {
            GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("Effects");
            _dmg.Play();
            GameObject g = LevelManager.Instance.DamageParticles.GetElement();
            //Debug.Log(GetComponent<Collider2D>().bounds.max.x);
            g.transform.position = new Vector2(transform.position.x, transform.position.y + (GetComponent<Collider2D>().bounds.max.y - GetComponent<Collider2D>().bounds.center.y));
        }
        private IEnumerator ChangeColor()
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            yield return new WaitForSeconds(0.1f);
            GetComponent<SpriteRenderer>().color = Color.white;
        }
        /// <summary>
        /// Cambia de valor a la variable "_PlayerMoney" y invoca el evento "_GameEventMoney" para avisar al script "HudCanvas"
        /// </summary>
        /// <param name="Money"></param>
        private void AddSubstractMoney(int Money) 
        {
            _PlayerMoney += Money;
            _GameEventMoney?.Raise(_PlayerMoney);
        }
        /// <summary>
        /// Modifica el valor del "_PlayerMoney" dependiendo de lo que cueste ItemsScriptableObjects y lo a�ade al Diccionario _ItemsInventory
        /// _ItemInventory
        /// </summary>
        /// <param name="itemaddinventory"></param>
        public void SubstractMoneyAndAddItemToInventory(ItemsScriptableObjects itemaddinventory, TalismaScriptableObject talismaadd)
        {
            if (itemaddinventory != null)
            {
                AddSubstractMoney(-itemaddinventory._Cost);
                AddItemsintheInventory(itemaddinventory);
            }
            else if (talismaadd != null)
            {
                AddSubstractMoney(-talismaadd._Cost);
                UnlockTalismanTolist(talismaadd);
            }
        }
        private void UnlockTalismanTolist(TalismaScriptableObject t)
        {
            if(!_UnlockedTalismans.Contains(t))
                _UnlockedTalismans.Add(t);
        }
        public void SubstractItemFromInventory(ItemsScriptableObjects item)
        {
            if (_ItemsInventory.ContainsKey(item))
            {
                int a = _ItemsInventory[item] -1;
                if(a > 0)
                {
                    _ItemsInventory[item] -= 1;
                }else
                    _ItemsInventory.Remove(item);
            }
        }
        /// <summary>
        /// A�ade ItemsScriptableObjects al Dictionario _ItemsInventory si ya existe la key del ItemScriptableObjects le a�ade uno mas a su value
        /// </summary>
        /// <param name="Items"></param>
        private void AddItemsintheInventory(ItemsScriptableObjects Items) 
        {
            if (_ItemsInventory.ContainsKey(Items))
                _ItemsInventory[Items] += 1;
            else
                _ItemsInventory.Add(Items,1);
            if(Items._ItemName == "Linterna")
                ActiveLantern();
        }
        private void Escape(InputAction.CallbackContext context)
        {
            if (!_StateMachine.CurrentState.Equals(_StateMachine.GetState<SM_PlayerOnIddle>()))
            {
                WhenEscape?.Invoke();
                OnIddle?.Invoke(_StateMachine.CurrentState);
                _StateMachine.ChangeState<SM_PlayerOnIddle>();
            }
        }
        public void PlayerSeletedPilarFromHud(string name)
        {
            if (!_UnlockedPilars.ContainsKey(name))
                return;
            Vector3 PosPilar = new Vector3(UnlockedPilars[name].x, UnlockedPilars[name].y + 0.49f, UnlockedPilars[name].z);
            transform.position = PosPilar;
            Camera.main.transform.position = PosPilar;
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Item")
            {
                if (collision.GetComponent<Items>().ItemSO)
                {
                    if (collision.GetComponent<Items>().ItemSO._ItemType == ItemType.Economy)
                    {
                        AddSubstractMoney(collision.GetComponent<Items>().ItemSO._Cost);
                        if (collision.GetComponent<Pooleable>())
                        {
                            collision.GetComponent<Pooleable>().ReturnToPool();
                        }
                        else
                        {
                            collision.gameObject.SetActive(false);
                            Destroy(collision.gameObject);
                        }
                    }
                    else if (collision.GetComponent<Items>().ItemSO._ItemType != ItemType.Economy)
                    {
                        AddItemsintheInventory(collision.GetComponent<Items>().ItemSO);
                        if (collision.GetComponent<Pooleable>())
                        {
                            collision.GetComponent<Pooleable>().ReturnToPool();
                        }
                        else
                        {
                            collision.gameObject.SetActive(false);
                            Destroy(collision.gameObject);
                        }
                    }
                }
            }
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Enemy")))
            {
                if (!_isInmune && !_isParry)
                {
                    RecibeDamage(collision.transform.GetComponent<EnemyAttackDMG>().DMG);
                    StartCoroutine(IgnoreCollisionWithEnemy());
                }
                if (_isParry)
                {
                    _isParry = false;
                    ShieldSprite.SetActive(false);
                }
            }
            if (collision.gameObject.GetComponent<Interactable>() != null) 
            {
                _StopWhenInteractable = collision.gameObject.GetComponent<Interactable>().SetIddle;
            }
            if (collision.tag == "Pilar")
            {
                _TouchingPilarName = collision.GetComponent<Pilar>().Name;
                _TouchingPilarPosition = collision.transform.position;
                _StopWhenInteractable = true;
            }
            if (collision.tag == "SkillsUnlocked")
            {
                _SkillUnlockedObject = collision.gameObject;
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == "Pilar")
            {
                _TouchingPilarName = null;
                _TouchingPilarPosition = collision.transform.position;
                _StopWhenInteractable = false;
            }
            if (collision.tag == "SkillsUnlocked")
            {
                _SkillUnlockedObject = null;
            }
            if (collision.gameObject.GetComponent<Interactable>() != null)
            {
                _StopWhenInteractable = false;
            }
        }
        private string _TouchingPilarName;
        private Vector3 _TouchingPilarPosition;
        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.tag == "Item")
            {
                if (collision.GetComponent<Items>().ItemSO._ItemType == ItemType.Economy)
                {
                    AddSubstractMoney(collision.GetComponent<Items>().ItemSO._Cost);
                    if (collision.GetComponent<Pooleable>())
                    {
                        collision.GetComponent<Pooleable>().ReturnToPool();
                    }
                    else
                    {
                        collision.gameObject.SetActive(false);
                        Destroy(collision.gameObject);
                    }
                }
                else if (collision.GetComponent<Items>().ItemSO._ItemType != ItemType.Economy)
                {
                    AddItemsintheInventory(collision.GetComponent<Items>().ItemSO);
                    if (collision.GetComponent<Pooleable>())
                    {
                        collision.GetComponent<Pooleable>().ReturnToPool();
                    }
                    else
                    {
                        collision.gameObject.SetActive(false);
                        Destroy(collision.gameObject);
                    }
                }
            }
            if (collision.tag == "Pilar")
            {
                _TouchingPilarName = collision.GetComponent<Pilar>().Name;
                _TouchingPilarPosition = collision.transform.position;
            }
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Enemy")))
            {
                if (!_isInmune && !_isParry)
                {
                    RecibeDamage(collision.transform.GetComponent<EnemyAttackDMG>().DMG);
                    ImpulseWhenCollisionWithEnemy(collision.contacts[0].normal);
                    StartCoroutine(IgnoreCollisionWithEnemy());
                }
                if (_isParry)
                {
                    ImpulseWhenCollisionWithEnemy(collision.contacts[0].normal);
                    _isParry = false;
                    ShieldSprite.SetActive(false);
                }
            }
        }
        private void ImpulseWhenCollisionWithEnemy(Vector3 contactnormal) 
        {
            GetComponent<Rigidbody2D>().AddForce(contactnormal * _PlayerScriptableObject._OnEnemyCollisionForce, ForceMode2D.Impulse);
        }
        private IEnumerator IgnoreCollisionWithEnemy() 
        {
            _isInmune = true;
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"),LayerMask.NameToLayer("Enemy"), true);
            yield return new WaitForSeconds(_PlayerScriptableObject._TimeInmuneWhenCollisionEnemy);
            _isInmune = false;
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Enemy"), false);
        }
        private IState _StateAnterior;
        public void Atacking()
        {
            GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("Effects");
            _Slash.Play();
            _StateAnterior = _StateMachine.CurrentState;
        }
        public void FinishAtack()
        {
            if (_StateAnterior.Equals(_StateMachine.GetState<SM_PlayerOnAir>()))
            {
                GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Air);
                _StateMachine.ChangeState<SM_PlayerOnAir>();
            }
            else if (_StateAnterior.Equals(_StateMachine.GetState<SM_PlayerOnGround>()))
            {
                GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.Iddle);
                _StateMachine.ChangeState<SM_PlayerOnGround>();
            }
            else if (_StateAnterior.Equals(_StateMachine.GetState<SM_PlayerOnCrunch>()))
            {
                GetComponent<PlayerAnimations>().SetAnimation(PlayerAnimations._CharacterAnimations.CrounchIddle);
                _StateMachine.ChangeState<SM_PlayerOnCrunch>();
            }
        }
        public SaveGameData.PlayerData Save()
        {
            return new SaveGameData.PlayerData(transform.position,_Health,_PlayerMoney,_DoubleJump, _ClimbUnlocked,DictionaryItemsSOConvertTostringint(),
                DictionaryTalismanSoConvertTostringint(_UnlockedTalismans), DictionaryTalismanSoConvertTostringint(_EquipedTalismans), _UnlockedSkills, 
                _MySkills, _UnlockedPilars.Keys.ToList(), _UnlockedPilars.Values.ToList());
        }
        public void Load(SaveGameData.PlayerData _playerdata)
        {
            StartSettings();
            transform.position = _playerdata._Position;
            _Health = _PlayerScriptableObject._Health;
            _PlayerMoney = _playerdata._Money;
            _GameEventMoney?.Raise(_PlayerMoney);
            _DoubleJump = _playerdata._DoubleJumpUnlocked;
            _ClimbUnlocked = _playerdata._ClimbUnlocked;
            _UnlockedSkills = _playerdata._AttacksUnlocked;
            _MySkills = _playerdata._AttacksEquipped;
            _UnlockedTalismans.Clear();
            _ItemsInventory.Clear();
            foreach (PlayerInventory a in _playerdata._Inventory)
            {
                if(a._Name == "Linterna")
                {
                    ActiveLantern();
                }
                _ItemsInventory.Add(_DataBaseSO.GetItem(a._Name),a._Stacked);
            }
            for (int i=0; i< _playerdata._TalismanUnlocked.Count(); i++)
            {
                _UnlockedTalismans.Add(_DataBaseSO.GetTalismans(_playerdata._TalismanUnlocked[i]));
            }
            for (int i = 0; i < _playerdata._TalismanEquipped.Count(); i++)
            {
                _EquipedTalismans.Add(_DataBaseSO.GetTalismans(_playerdata._TalismanEquipped[i]));
            }
            for (int i= 0; i< _playerdata._PilarName.Count; i++)
            {
                _UnlockedPilars.Add(_playerdata._PilarName[i], _playerdata._PilarPosition[i]);
            }
            OnPlayerload?.Invoke();
        }
        private void ActiveLantern()
        {
            //_Lantern = true;
            _LanterGO.SetActive(true);
        }
        private List<PlayerInventory> DictionaryItemsSOConvertTostringint()
        {
            List<PlayerInventory> items = new List<PlayerInventory>();
            foreach (KeyValuePair<ItemsScriptableObjects,int> a in _ItemsInventory)
            {
                PlayerInventory a2;
                a2._Name = a.Key._ItemName;
                a2._Stacked = a.Value;
                items.Add(a2);
            }
            return items;
        }
        private List<string> DictionaryTalismanSoConvertTostringint(List<TalismaScriptableObject> tal)
        {
            List<string> talismans = new List<string>();
            for (int i= 0;i <tal.Count;i++)
            {
                if (tal[i] != null)
                    talismans.Add(tal[i]._PerkName);
                else
                    talismans.Add(null);
            }
            return talismans;
        }
        private void OnDisable()
        {
            _Inputs.FindAction("Escape").performed -= Escape;
            GetComponent<Parry>().SetParry -= SetParry;
            _Inputs.FindActionMap("Player").Disable();
        }
    }
}
