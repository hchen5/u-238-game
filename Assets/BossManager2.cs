using EnemiesNS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossManager2 : MonoBehaviour
{
    [SerializeField]
    private TriggerEnter _Triger;
    [SerializeField]
    private Enemy2Behaviour _Boss2;
    [SerializeField]
    private GameObject _Door2;

    private void Start()
    {
        if (_Boss2.GetComponent<Enemy2Behaviour>().Respawn)
            _Triger.onPlayer += CloseDoor;
    }
    private void CloseDoor()
    {
        _Triger.onPlayer -= CloseDoor;
        _Boss2.GetComponent<Enemy2Behaviour>().InitBoss();
        _Boss2.GetComponent<Enemy2Behaviour>().OnEvent += OpenDoor;
        _Door2.SetActive(true);

    }
    private void OpenDoor()
    {
        _Boss2.GetComponent<Enemy2Behaviour>().OnEvent -= OpenDoor;
        _Door2.SetActive(false);
    }
}
